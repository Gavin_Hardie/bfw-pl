#!/usr/bin/perl
use strict;
use File::Basename;
use K_Log;
use lib dirname(__FILE__);

use Bfw::Pipeline;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;

my $stage_sql = <<MULTILINE_SQL;
select
--DIMESIONS--
pallet_sequences.pallet_number as _pallet_number,
pallet_sequences.pallet_sequence_number as _pallet_sequence_number,
upper(fg_products.fg_product_code) as _fg_product_fg_product_code,
upper(pallet_sequences.country_code) as _target_market_country_code,
pallet_sequences.marketing_tm_group as _target_market_group_code,
upper(final_receiver_party_types.party_type_name) as _final_receiver_party_type_name,
upper(final_receiver_roles.role_name)as _final_receiver_role_name,
upper(final_receiver_parties.party_name) as _final_receiver_party_name,
upper(shipping_line_party_types.party_type_name) as _shipping_line_party_type_name,
upper(shipping_line_roles.role_name) as _shipping_line_role_name,
upper(shipping_line_parties.party_name) as _shipping_line_party_name,
upper(customer_party_types.party_type_name) as _customer_party_type_name,
upper(customer_roles.role_name) as _customer_role_name,
upper(customer_parties.party_name) as _customer_party_name,
upper(vessels.vessel_type_code) as _vessel_type_code,
upper(vessels.vessel_code) as _vessel_code,
upper(locations.location_type_code) as _location_type_code,
upper(locations.location_code) as _location_code,
upper(containers.container_code) as _container_code,
upper(load_instructions.depot_code) as _location_depot_code,
upper(ports_discharge.port_type) as _port_of_discharge_port_type,
upper(ports_discharge.port_code) as _port_of_discharge_port_code,
upper(ports_loading.port_type) as _port_of_loading_port_type,
upper(ports_loading.port_code) as _port_of_loading_port_code,
upper(fin_supplier_party_types.party_type_name) as _fin_supplier_party_type_code,
upper(fin_supplier_roles.role_name) as _fin_supplier_role_name,
upper(fin_supplier_parties.party_name) as _fin_supplier_party_name,
jmt_customer_invoice_data.deal_type_id as _deal_type_id,
pallet_sequences.customer_invoice_id as _customer_invoice_id,
invoices.invoice_ref_no as _invoice_ref_no,
preliminary_invoices.invoice_ref_no as _preliminary_invoice_ref_no,
seasons.season_type as _season_type,
seasons.season_code as _season_code,
--DATES--
jmt_pallet_sequence_quantity_data.voyage_year,
jmt_pallet_sequence_quantity_data.applic_invoice_departure_date,
jmt_pallet_sequence_quantity_data.actual_invoice_departure_date,
jmt_pallet_sequence_quantity_data.estimate_invoice_departure_date,
jmt_pallet_sequence_quantity_data.applic_invoice_arrival_date,
jmt_pallet_sequence_quantity_data.estimate_invoice_arrival_date,
jmt_pallet_sequence_quantity_data.actual_invoice_arrival_date,
jmt_pallet_sequence_quantity_data.account_sale_due_date,
--DEGENERATE--
pallet_sequences.customer_account_sale_id,
jmt_customer_invoice_data.customer_account_sale_approved,
jmt_customer_invoice_data.customer_account_sale_final_status,
jmt_customer_invoice_data.customer_account_sale_status_group as customer_account_sale_status_group,
jmt_customer_invoice_data.currency_code as customer_currency_code,
jmt_customer_invoice_data.invoiced_price_per_carton as invoiced_price_per_carton,
jmt_supplier_invoice_data.currency_code as supplier_currency_code,
jmt_supplier_invoice_data.pay_in_forex,
jmt_supplier_invoice_data.account_sale_finalised as supplier_account_sale_finalised,
--MEASURES--
customer_gl_codes.gl_code as gl_code,
estimated_exchange_rate.exchange_rate as roe_on_etd,
actual_exchange_rate.exchange_rate as roe_on_atd,
supplier_account_sales.exchange_rate as roe_on_acc_sale,
jmt_pallet_sequence_quantity_data.pallet_size,
jmt_pallet_sequence_quantity_data.std_ctn_qty,
jmt_pallet_sequence_quantity_data.act_ctn_qty,
jmt_pallet_sequence_quantity_data.nett_weight_per_carton,
jmt_pallet_sequence_financial_data.roe_local_usd,
jmt_pallet_sequence_financial_data.customer_invoiced_price_per_carton,
jmt_pallet_sequence_financial_data.customer_invoiced_price_zar_per_carton,
jmt_pallet_sequence_financial_data.credit_note_other_per_carton,
jmt_pallet_sequence_financial_data.credit_note_other_zar_per_carton,
jmt_pallet_sequence_financial_data.debit_note_other_per_carton,
jmt_pallet_sequence_financial_data.debit_note_other_zar_per_carton,
jmt_pallet_sequence_financial_data.credit_note_quality_per_carton,
jmt_pallet_sequence_financial_data.credit_note_quality_zar_per_carton,
jmt_pallet_sequence_financial_data.debit_note_quality_per_carton,
jmt_pallet_sequence_financial_data.debit_note_quality_zar_per_carton,
jmt_pallet_sequence_financial_data.CIF_FOB_per_carton,
jmt_pallet_sequence_financial_data.CIF_FOB_zar_per_carton,
jmt_pallet_sequence_financial_data.customer_amount_received_per_carton,
jmt_pallet_sequence_financial_data.customer_amount_received_zar_per_carton,
jmt_pallet_sequence_financial_data.freight_costs_per_carton,
jmt_pallet_sequence_financial_data.freight_costs_zar_per_carton,
jmt_pallet_sequence_financial_data.overseas_costs_per_carton,
jmt_pallet_sequence_financial_data.overseas_costs_zar_per_carton,
jmt_pallet_sequence_financial_data.pre_profit_fob_per_carton,
jmt_pallet_sequence_financial_data.pre_profit_fob_zar_per_carton,
jmt_pallet_sequence_financial_data.profit_per_carton,
jmt_pallet_sequence_financial_data.profit_zar_per_carton,
jmt_pallet_sequence_financial_data.post_profit_fob_per_carton,
jmt_pallet_sequence_financial_data.post_profit_fob_zar_per_carton,
jmt_pallet_sequence_financial_data.commission_per_carton,
jmt_pallet_sequence_financial_data.commission_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_fob_per_carton,
jmt_pallet_sequence_financial_data.supplier_fob_zar_per_carton,
jmt_pallet_sequence_financial_data.estimated_fob_cost_per_carton,
jmt_pallet_sequence_financial_data.estimated_fob_cost_zar_per_carton,
jmt_pallet_sequence_financial_data.actual_fob_costs_per_carton,
jmt_pallet_sequence_financial_data.actual_fob_costs_zar_per_carton,
jmt_pallet_sequence_financial_data.total_fob_cost_per_carton,
jmt_pallet_sequence_financial_data.total_fob_cost_zar_per_carton,
jmt_pallet_sequence_financial_data.ex_cold_store_per_carton,
jmt_pallet_sequence_financial_data.ex_cold_store_zar_per_carton,
jmt_pallet_sequence_financial_data.cold_storage_per_carton,
jmt_pallet_sequence_financial_data.cold_storage_zar_per_carton,
jmt_pallet_sequence_financial_data.dip_per_carton,
jmt_pallet_sequence_financial_data.dip_zar_per_carton,
jmt_pallet_sequence_financial_data.ppecb_levies_per_carton,
jmt_pallet_sequence_financial_data.ppecb_levies_zar_per_carton,
jmt_pallet_sequence_financial_data.cga_levies_per_carton,
jmt_pallet_sequence_financial_data.cga_levies_zar_per_carton,
jmt_pallet_sequence_financial_data.levies_per_carton,
jmt_pallet_sequence_financial_data.levies_zar_per_carton,
jmt_pallet_sequence_financial_data.packing_costs_per_carton,
jmt_pallet_sequence_financial_data.packing_costs_zar_per_carton,
jmt_pallet_sequence_financial_data.transport_to_port_per_carton,
jmt_pallet_sequence_financial_data.transport_to_port_zar_per_carton,
jmt_pallet_sequence_financial_data.commission_vat_per_carton,
jmt_pallet_sequence_financial_data.commission_vat_zar_per_carton,
jmt_pallet_sequence_financial_data.service_provider_vat_per_carton,
jmt_pallet_sequence_financial_data.service_provider_vat_zar_per_carton,
jmt_pallet_sequence_financial_data.vat_per_carton,
jmt_pallet_sequence_financial_data.vat_zar_per_carton,
jmt_pallet_sequence_financial_data.other_costs_per_carton,
jmt_pallet_sequence_financial_data.other_costs_zar_per_carton,
jmt_pallet_sequence_financial_data.due_to_supplier_per_carton,
jmt_pallet_sequence_financial_data.due_to_supplier_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_invoiced_price_per_carton,
jmt_pallet_sequence_financial_data.supplier_invoiced_price_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_credit_note_per_carton,
jmt_pallet_sequence_financial_data.supplier_credit_note_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_debit_note_per_carton,
jmt_pallet_sequence_financial_data.supplier_debit_note_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_net_per_carton,
jmt_pallet_sequence_financial_data.supplier_net_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_amount_paid_per_carton,
jmt_pallet_sequence_financial_data.supplier_amount_paid_zar_per_carton,
jmt_pallet_sequence_financial_data.supplier_balance_due_per_carton,
jmt_pallet_sequence_financial_data.supplier_balance_due_zar_per_carton,
pallet_sequences.invoice_price_per_unit as invoice_price_per_carton,
pallet_sequences.revenue_per_unit as revenue_per_carton,
pallet_sequences.foreign_cost_per_unit as foreign_cost_per_carton,
pallet_sequences.profit_loss_per_unit as profit_loss_per_carton,
COALESCE(market_finalisations.status, 'PENDING') as market_finalisation_status,
market_finalisations.provision_commercial_credit as provision_commercial_credit,
case
when (credit_note_other_zar_per_carton * jmt_pallet_sequence_quantity_data.act_ctn_qty) <> 0 then 'Commercial discount'
when upper(customer_account_sale_status) in ('POSSIBLE CLAIM') and credit_note_quality_zar_per_carton = 0 then 'Possible claim'
when credit_note_quality_zar_per_carton <> 0 then 'QC claim'
else 'Sound' end as qc_status_group,
--LOAD DATE--
now() as load_date
from
jmt.pallet_sequences pallet_sequences
join jmt.pallets pallets ON pallets.id = pallet_sequences.pallet_id or pallet_sequences.scrapped_pallet_number = pallets.pallet_number
--INVOICE--
join jmt.invoices invoices ON invoices.id = pallet_sequences.invoice_id
join jmt.invoice_items item ON item.id = pallet_sequences.invoice_item_id
left join jmt.invoices preliminary_invoices ON preliminary_invoices.id = pallet_sequences.preliminary_invoice_id
-- EXCHANGE RATES --
left join jmt.exchange_rates estimated_exchange_rate on estimated_exchange_rate.id = invoices.estimated_exchange_rate_id
left join jmt.exchange_rates actual_exchange_rate on actual_exchange_rate.id = invoices.exchange_rate_id
left join jmt.supplier_account_sales on supplier_account_sales.id = pallet_sequences.supplier_account_sale_id
--DIM: PRODUCT
left join jmt.fg_products fg_products ON fg_products.id = pallet_sequences.fg_product_id
join jmt.item_pack_products item_pack_products ON item_pack_products.id = fg_products.item_pack_product_id
join jmt.varieties varieties ON varieties.id = item_pack_products.variety_id
join jmt.commodities commodities ON commodities.id = varieties.commodity_id
join jmt.packs packs on packs.pack_code = fg_products.pack_code
join jmt.fg_product_weights fg_product_weights on fg_product_weights.pack_id = packs.id and fg_product_weights.commodity_id = varieties.commodity_id
left join jmt.suppliers suppliers ON suppliers.supplier_parties_role_id = pallet_sequences.fin_supplier_party_role_id
left join jmt.fg_product_weights_suppliers fg_product_weights_suppliers ON fg_product_weights_suppliers.supplier_id = suppliers.id AND fg_product_weights_suppliers.pack_id = packs.id AND fg_product_weights_suppliers.commodity_id = varieties.commodity_id
--DATE: ARRIVAL
left join jmt.load_instruction_details load_instruction_details ON pallets.load_instruction_detail_id = load_instruction_details.id
left join jmt.load_instruction_containers load_instruction_containers ON pallets.load_instruction_container_id = load_instruction_containers.id
left join jmt.consignment_order_load_instructions consignment_order_load_instructions ON COALESCE(load_instruction_containers.consignment_order_load_instruction_id, load_instruction_details.consignment_order_load_instruction_id) = consignment_order_load_instructions.id
left join jmt.consignment_orders consignment_orders ON consignment_order_load_instructions.consignment_order_id = consignment_orders.id
left join jmt.consignments consignments ON consignment_orders.consignment_id = consignments.id
left join jmt.voyage_ports voyage_ports ON consignments.pod_voyage_port_id = voyage_ports.id
left join jmt.voyages voyages ON voyages.id = voyage_ports.voyage_id
--DIM: SEAL POINT
left join jmt.load_instructions load_instructions on consignment_order_load_instructions.load_instruction_id = load_instructions.id
--DIM: PORTS
left join jmt.voyage_ports voyage_ports_discharge ON consignments.pod_voyage_port_id = voyage_ports_discharge.id
left join jmt.voyage_ports voyage_ports_loading ON load_instructions.pol_voyage_port_id = voyage_ports_loading.id
left join jmt.ports ports_discharge on ports_discharge.id = voyage_ports_discharge.port_id
left join jmt.ports ports_loading on ports_loading.id = voyage_ports_loading.port_id
--DIM: VESSELS
left join jmt.voyages vessels_voyages ON vessels_voyages.id = load_instructions.voyage_id
left join jmt.vessels vessels on vessels.id = vessels_voyages.vessel_id
--DATE: GATE_OUT_DATE
left join jmt.shipping_docs shipping_docs ON shipping_docs.consignment_id = consignments.id
--DIM: PARTY - FINAL_RECEIVER
left join jmt.parties_roles final_receiver_parties_roles ON final_receiver_parties_roles.id = consignments.final_receiver_party_role_id
left join jmt.roles final_receiver_roles on final_receiver_roles.id = final_receiver_parties_roles.role_id
left join jmt.parties final_receiver_parties on final_receiver_parties.id = final_receiver_parties_roles.party_id
left join jmt.party_types final_receiver_party_types on final_receiver_party_types.id = final_receiver_parties.party_type_id
--DIM: PARTY - CUSTOMER
left join jmt.parties_roles customer_parties_roles ON customer_parties_roles.id = consignments.customer_party_role_id
left join jmt.roles customer_roles on customer_roles.id = customer_parties_roles.role_id
left join jmt.parties customer_parties on customer_parties.id = customer_parties_roles.party_id
left join jmt.party_types customer_party_types on customer_party_types.id = customer_parties.party_type_id
--DIM: FINANCIAL_SUPPLIER
left join jmt.parties_roles fin_supplier_party_roles on fin_supplier_party_roles.id = pallet_sequences.fin_supplier_party_role_id --and fin_supplier_party_roles.role_name = 'FIN_SUPPLIER'
left join jmt.roles fin_supplier_roles on fin_supplier_roles.id = fin_supplier_party_roles.role_id
left join jmt.parties fin_supplier_parties on fin_supplier_parties.id = fin_supplier_party_roles.party_id
left join jmt.party_types fin_supplier_party_types on fin_supplier_party_types.id = fin_supplier_parties.party_type_id
--DIM: LOCATION
left join jmt.locations locations on locations.location_code = pallets.location_code
--DIM: CONTAINER_CODE
left join jmt.containers containers on containers.id = load_instruction_containers.container_id AND consignments.id = containers.consignment_id
--DIM: PARTY - SHIPPING_LINE
left join jmt.vessel_bookings vessel_bookings on containers.vessel_booking_id = vessel_bookings.id
left join jmt.parties_roles shipping_line_parties_roles ON shipping_line_parties_roles.id = vessel_bookings.shipping_line_party_role_id
left join jmt.roles shipping_line_roles on shipping_line_roles.id = shipping_line_parties_roles.role_id
left join jmt.parties shipping_line_parties on shipping_line_parties.id = shipping_line_parties_roles.party_id
left join jmt.party_types shipping_line_party_types on shipping_line_party_types.id = shipping_line_parties.party_type_id
--DIM: SEASON
left join jmt.seasons seasons on seasons.season_code = pallet_sequences.season_code
--MARKET_FINALISATIONS--
left join jmt.market_finalisations on market_finalisations.id = pallet_sequences.market_finalisation_id
--CUSTOMER_ACCOUNTS--
left join stg.jmt_customer_invoice_data on jmt_customer_invoice_data.id = pallet_sequences.id
--SUPPLIER_ACCOUNTS--
left join stg.jmt_supplier_invoice_data on jmt_supplier_invoice_data.id = pallet_sequences.id
--MEASURES: gl_codes
left join jmt.customer_gl_codes on customer_gl_codes.currency_id = invoices.currency_id
    and customer_gl_codes.customer_id = invoices.customer_id
--MEASURES: FINANCIAL
join stg.jmt_pallet_sequence_financial_data on jmt_pallet_sequence_financial_data.id = pallet_sequences.id
--MEASURES: DATE & QUANTITY
join stg.jmt_pallet_sequence_quantity_data on jmt_pallet_sequence_quantity_data.id = pallet_sequences.id
    and jmt_pallet_sequence_quantity_data.voyage_year > 2012
where
pallet_sequences.pallet_number IS NOT NULL AND
pallet_sequences.carton_quantity > 0
MULTILINE_SQL


my $publish_sql = <<MULTILINE_SQL;
SELECT coalesce(public.d_pallet_sequence.pallet_sequence_sk, -1) as pallet_sequence_sk,
       coalesce(public.d_fg_product.fg_product_sk, -1) as fg_product_sk,
       coalesce(public.d_marketing_target_market.marketing_target_market_sk, -1) as marketing_target_market_sk,
       coalesce(public.d_shipping_line_party.shipping_line_party_sk, -1) as shipping_line_party_sk,
       coalesce(public.d_customer.customer_sk, -1) as customer_sk,
       coalesce(public.d_vessel.vessel_sk,-1) as vessel_sk,
       coalesce(public.d_location.location_sk, -1) as location_sk,
       coalesce(public.d_container.container_sk, -1) as container_sk,
       coalesce(public.d_seal_point_location.seal_point_location_sk, -1) as seal_point_location_sk,
       coalesce(public.d_port_of_discharge.port_of_discharge_sk, -1) as port_of_discharge_sk,
       coalesce(public.d_port_of_loading.port_of_loading_sk, -1) as port_of_loading_sk,
       coalesce(public.d_fin_supplier.fin_supplier_sk, -1) as fin_supplier_sk,
       coalesce(public.d_deal_type.deal_type_sk, -1) as deal_type_sk,
       coalesce(public.d_season.season_sk, -1) as season_sk,
       coalesce(public.d_customer_invoice.customer_invoice_sk, -1) as customer_invoice_sk,
       coalesce(public.d_invoice.invoice_sk, -1) as invoice_sk,
       coalesce(public.d_preliminary_invoice.preliminary_invoice_sk, -1) as preliminary_invoice_sk,
       voyage_year,
       mk_date_sk(applic_invoice_departure_date::date) as applic_invoice_departure_date_sk,
       mk_date_sk(actual_invoice_departure_date::date) as actual_invoice_departure_date_sk,
       mk_date_sk(estimate_invoice_departure_date::date) as estimate_invoice_departure_date_sk,
       mk_date_sk(applic_invoice_arrival_date::date) as applic_invoice_arrival_date_sk,
       mk_date_sk(estimate_invoice_arrival_date::date) as estimate_invoice_arrival_date_sk,
       mk_date_sk(actual_invoice_arrival_date::date) as actual_invoice_arrival_date_sk,
       mk_date_sk(account_sale_due_date::date) as account_sale_due_date_sk,
       stg.f_pallet_sequence_sales_as.gl_code,
       stg.f_pallet_sequence_sales_as.customer_account_sale_id,
       stg.f_pallet_sequence_sales_as.customer_account_sale_final_status,
       stg.f_pallet_sequence_sales_as.customer_account_sale_approved,
       stg.f_pallet_sequence_sales_as.customer_account_sale_status_group,
       stg.f_pallet_sequence_sales_as.customer_currency_code,
       stg.f_pallet_sequence_sales_as.supplier_currency_code,
       stg.f_pallet_sequence_sales_as.pay_in_forex,
       stg.f_pallet_sequence_sales_as.supplier_account_sale_finalised,
       stg.f_pallet_sequence_sales_as.pallet_size,
       stg.f_pallet_sequence_sales_as.std_ctn_qty,
       stg.f_pallet_sequence_sales_as.act_ctn_qty,
       stg.f_pallet_sequence_sales_as.nett_weight_per_carton,
       stg.f_pallet_sequence_sales_as.roe_local_usd,
       stg.f_pallet_sequence_sales_as.roe_on_etd,
       stg.f_pallet_sequence_sales_as.roe_on_atd,
       stg.f_pallet_sequence_sales_as.roe_on_acc_sale,
       stg.f_pallet_sequence_sales_as.invoiced_price_per_carton,
       stg.f_pallet_sequence_sales_as.customer_invoiced_price_per_carton,
       stg.f_pallet_sequence_sales_as.customer_invoiced_price_zar_per_carton,
       stg.f_pallet_sequence_sales_as.credit_note_other_per_carton,
       stg.f_pallet_sequence_sales_as.credit_note_other_zar_per_carton,
       stg.f_pallet_sequence_sales_as.debit_note_other_per_carton,
       stg.f_pallet_sequence_sales_as.debit_note_other_zar_per_carton,
       stg.f_pallet_sequence_sales_as.credit_note_quality_per_carton,
       stg.f_pallet_sequence_sales_as.credit_note_quality_zar_per_carton,
       stg.f_pallet_sequence_sales_as.debit_note_quality_per_carton,
       stg.f_pallet_sequence_sales_as.debit_note_quality_zar_per_carton,
       stg.f_pallet_sequence_sales_as.CIF_FOB_per_carton,
       stg.f_pallet_sequence_sales_as.CIF_FOB_zar_per_carton,
       stg.f_pallet_sequence_sales_as.customer_amount_received_per_carton,
       stg.f_pallet_sequence_sales_as.customer_amount_received_zar_per_carton,
       stg.f_pallet_sequence_sales_as.freight_costs_per_carton,
       stg.f_pallet_sequence_sales_as.freight_costs_zar_per_carton,
       stg.f_pallet_sequence_sales_as.overseas_costs_per_carton,
       stg.f_pallet_sequence_sales_as.overseas_costs_zar_per_carton,
       stg.f_pallet_sequence_sales_as.pre_profit_fob_per_carton,
       stg.f_pallet_sequence_sales_as.pre_profit_fob_zar_per_carton,
       stg.f_pallet_sequence_sales_as.profit_per_carton,
       stg.f_pallet_sequence_sales_as.profit_zar_per_carton,
       stg.f_pallet_sequence_sales_as.post_profit_fob_per_carton,
       stg.f_pallet_sequence_sales_as.post_profit_fob_zar_per_carton,
       stg.f_pallet_sequence_sales_as.commission_per_carton,
       stg.f_pallet_sequence_sales_as.commission_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_fob_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_fob_zar_per_carton,
       stg.f_pallet_sequence_sales_as.estimated_fob_cost_per_carton,
       stg.f_pallet_sequence_sales_as.estimated_fob_cost_zar_per_carton,
       stg.f_pallet_sequence_sales_as.actual_fob_costs_per_carton,
       stg.f_pallet_sequence_sales_as.actual_fob_costs_zar_per_carton,
       stg.f_pallet_sequence_sales_as.total_fob_cost_per_carton,
       stg.f_pallet_sequence_sales_as.total_fob_cost_zar_per_carton,
       stg.f_pallet_sequence_sales_as.ex_cold_store_per_carton,
       stg.f_pallet_sequence_sales_as.ex_cold_store_zar_per_carton,
       stg.f_pallet_sequence_sales_as.cold_storage_per_carton,
       stg.f_pallet_sequence_sales_as.cold_storage_zar_per_carton,
       stg.f_pallet_sequence_sales_as.dip_per_carton,
       stg.f_pallet_sequence_sales_as.dip_zar_per_carton,
       stg.f_pallet_sequence_sales_as.ppecb_levies_per_carton,
       stg.f_pallet_sequence_sales_as.ppecb_levies_zar_per_carton,
       stg.f_pallet_sequence_sales_as.cga_levies_per_carton,
       stg.f_pallet_sequence_sales_as.cga_levies_zar_per_carton,
       stg.f_pallet_sequence_sales_as.levies_per_carton,
       stg.f_pallet_sequence_sales_as.levies_zar_per_carton,
       stg.f_pallet_sequence_sales_as.packing_costs_per_carton,
       stg.f_pallet_sequence_sales_as.packing_costs_zar_per_carton,
       stg.f_pallet_sequence_sales_as.transport_to_port_per_carton,
       stg.f_pallet_sequence_sales_as.transport_to_port_zar_per_carton,
       stg.f_pallet_sequence_sales_as.commission_vat_per_carton,
       stg.f_pallet_sequence_sales_as.commission_vat_zar_per_carton,
       stg.f_pallet_sequence_sales_as.service_provider_vat_per_carton,
       stg.f_pallet_sequence_sales_as.service_provider_vat_zar_per_carton,
       stg.f_pallet_sequence_sales_as.vat_per_carton,
       stg.f_pallet_sequence_sales_as.vat_zar_per_carton,
       stg.f_pallet_sequence_sales_as.other_costs_per_carton,
       stg.f_pallet_sequence_sales_as.other_costs_zar_per_carton,
       stg.f_pallet_sequence_sales_as.due_to_supplier_per_carton,
       stg.f_pallet_sequence_sales_as.due_to_supplier_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_invoiced_price_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_invoiced_price_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_credit_note_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_credit_note_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_debit_note_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_debit_note_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_net_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_net_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_amount_paid_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_amount_paid_zar_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_balance_due_per_carton,
       stg.f_pallet_sequence_sales_as.supplier_balance_due_zar_per_carton,
       stg.f_pallet_sequence_sales_as.invoice_price_per_carton,
       stg.f_pallet_sequence_sales_as.revenue_per_carton,
       stg.f_pallet_sequence_sales_as.foreign_cost_per_carton,
       stg.f_pallet_sequence_sales_as.profit_loss_per_carton,
       stg.f_pallet_sequence_sales_as.provision_commercial_credit,
       stg.f_pallet_sequence_sales_as.market_finalisation_status,
       stg.f_pallet_sequence_sales_as.qc_status_group,
       now() as load_date
from stg.f_pallet_sequence_sales_as
join public.d_pallet_sequence on stg.f_pallet_sequence_sales_as._pallet_number = d_pallet_sequence.pallet_number
    and stg.f_pallet_sequence_sales_as._pallet_sequence_number = d_pallet_sequence.pallet_sequence_number
left join public.d_fg_product on stg.f_pallet_sequence_sales_as._fg_product_fg_product_code = d_fg_product.fg_product_code
left join public.d_marketing_target_market on stg.f_pallet_sequence_sales_as._target_market_country_code = d_marketing_target_market.country_code
    and stg.f_pallet_sequence_sales_as._target_market_group_code = d_marketing_target_market.marketing_target_market_group_code
left join public.d_final_receiver_party on stg.f_pallet_sequence_sales_as._final_receiver_party_type_name = d_final_receiver_party.final_receiver_party_type_nm
    and stg.f_pallet_sequence_sales_as._final_receiver_party_name = d_final_receiver_party.final_receiver_party_nm
left join public.d_shipping_line_party on stg.f_pallet_sequence_sales_as._shipping_line_party_type_name = public.d_shipping_line_party.shipping_line_party_type_nm
    and stg.f_pallet_sequence_sales_as._shipping_line_party_name = public.d_shipping_line_party.shipping_line_party_nm
left join public.d_customer on stg.f_pallet_sequence_sales_as._customer_party_type_name = public.d_customer.party_type_name
    and stg.f_pallet_sequence_sales_as._customer_party_name = public.d_customer.customer_name
left join public.d_vessel on stg.f_pallet_sequence_sales_as._vessel_type_code = public.d_vessel.vessel_type_code
    and stg.f_pallet_sequence_sales_as._vessel_code = public.d_vessel.vessel_code
left join public.d_location on stg.f_pallet_sequence_sales_as._location_type_code = public.d_location.location_type_code
    and stg.f_pallet_sequence_sales_as._location_code = public.d_location.location_code
left join public.d_container on stg.f_pallet_sequence_sales_as._container_code = public.d_container.container_code
left join public.d_seal_point_location on stg.f_pallet_sequence_sales_as._location_depot_code = public.d_seal_point_location.seal_point_location_code
left join public.d_port_of_discharge on stg.f_pallet_sequence_sales_as._port_of_discharge_port_type = public.d_port_of_discharge.port_of_discharge_type
    and stg.f_pallet_sequence_sales_as._port_of_discharge_port_code = public.d_port_of_discharge.port_of_discharge_code
left join public.d_port_of_loading on stg.f_pallet_sequence_sales_as._port_of_loading_port_type = public.d_port_of_loading.port_of_loading_type
    and stg.f_pallet_sequence_sales_as._port_of_loading_port_code = public.d_port_of_loading.port_of_loading_code
left join public.d_fin_supplier on stg.f_pallet_sequence_sales_as._fin_supplier_party_type_code = public.d_fin_supplier.party_type_name
    and stg.f_pallet_sequence_sales_as._fin_supplier_party_name = public.d_fin_supplier.fin_supplier_name
left join public.d_deal_type on stg.f_pallet_sequence_sales_as._deal_type_id = public.d_deal_type.deal_type_id
left join public.d_season on stg.f_pallet_sequence_sales_as._season_type = public.d_season.season_type
    and stg.f_pallet_sequence_sales_as._season_code = public.d_season.season_code
left join public.d_customer_invoice on stg.f_pallet_sequence_sales_as._customer_invoice_id = public.d_customer_invoice.customer_invoice_id
left join public.d_invoice on stg.f_pallet_sequence_sales_as._invoice_ref_no = d_invoice.invoice_ref_no
left join public.d_preliminary_invoice on stg.f_pallet_sequence_sales_as._preliminary_invoice_ref_no = d_preliminary_invoice.preliminary_invoice_ref_no
MULTILINE_SQL

# Run $stage_sql into stg.f_pallet_sequence_sales_as
K_Log::log("[bfw0006] Building stg.f_pallet_sequence_sales_as");
Pipeline->new(
    {
        dataset => 'f_pallet_sequence_sales_as',
        action  => 'Stage',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $stage_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'stg',
                tablename => 'f_pallet_sequence_sales_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();

# Truncate and copy stg.f_container_shipped_as into qry.f_pallet_sequence_sales_as
K_Log::log("[bfw0006] Building qry.f_pallet_sequence_sales_as");
Pipeline->new(
    {
        dataset => 'f_pallet_sequence_sales_as',
        action  => 'Move',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $publish_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'qry',
                tablename => 'f_pallet_sequence_sales_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();

K_Log::log("[bfw0006] Done");

1;
