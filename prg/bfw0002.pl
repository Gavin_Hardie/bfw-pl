#!/usr/bin/perl

use strict;
use File::Basename;
use lib dirname(__FILE__);
use Data::Dumper;
use YAML::Tiny;
use K_Log;
use v5.010;

use Bfw::Pipeline;
use Bfw::PipeManager;
use Bfw::Source::TableSource;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;

my $yaml    = YAML::Tiny->read("$ENV{MIS}/prg/bfw0002.yml");
my $config  = $yaml->[0];
my $manager = PipeManager->new( { alias => 'UNI' } );

foreach my $key ( sort keys %{$config} ) {

    my $sql = $config->{$key};
    my ($table) = $key=~m/^\d+_(.*)/;

    K_Log::log("[bfw0002] Building stg.$table");

    Pipeline->new(
        {
            dataset => $table,
            action  => 'Stage',
            source  => SelectSource->new(
                {
                    alias  => 'UNI',
                    select => $sql
                }
            ),
            target => TableWriter->new(
                {
                    alias     => 'UNI',
                    schema    => 'stg',
                    tablename => $table,
                    truncate  => 1
                }
            ),
            datatypeCheck => 0,
        }
    )->run();

}

K_Log::log("[bfw0002] Done");

1;
