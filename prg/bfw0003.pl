#!/usr/bin/perl

use strict;
use File::Basename;
use lib dirname(__FILE__);
use Data::Dumper;
use K_Log;
use YAML::Tiny;

use Bfw::PipeManager;
use Bfw::Pipeline;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;
use Bfw::Source::TableSource;
use Bfw::Target::T1DimensionWriter;

my $yaml   = YAML::Tiny->read("$ENV{MIS}/prg/bfw0003.yml");
my $config = $yaml->[0];

my $manager = PipeManager->new( { alias => 'UNI' } );

foreach my $key ( sort keys %{$config} ) {

    my ($table) = $key=~m/^\d+_(.*)/;


    my $sql         = $config->{$key}->{sql};
    my $where_clause = $config->{$key}->{where};
    my $sk_col      = $config->{$key}->{sk_col};
    my @unique_cols = $config->{$key}->{unique_cols};
    my $pipe_code   = $config->{$key}->{pipe_code};
    my @pipes       = ();

    my $source_sql;
    if (defined $where_clause) {
        $source_sql = "select * from ( $sql ) a where $where_clause";
    } else {
        $source_sql = $sql;
    }

    # Build the dimension into stg.$table
    K_Log::log("[bfw0003] Building stg.$table");
    Pipeline->new(
    {
        dataset => $table,
        action  => 'Build',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $source_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'stg',
                tablename => $table,
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    })->run();

    # Merge stg.$table into qry.$table
    K_Log::log("[bfw0003] Updating qry.$table");
    Pipeline->new(
    {
        dataset => $table,
        action  => 'Merge',
        code    => $pipe_code . '_m',
        source  => TableSource->new(
            {
                alias     => 'UNI',
                schema    => 'stg',
                tablename => $table
            }
        ),
        target => T1DimensionWriter->new(
            {
                alias       => 'UNI',
                schema      => 'qry',
                tablename   => $table,
                sk_col      => $sk_col,
                unique_cols => @unique_cols
            }
        ),
        datatypeCheck => 0,
    })->run();

}


my $dbh = Db::connect('UNI');

# Foreach $key check if $table contains the -1 record.
foreach my $key ( sort keys %{$config} ) {
    my ($table) = $key=~m/^\d+_(.*)/;
    my $sk_col = $config->{$key}->{sk_col};

    my $count_sql = "select count(*) from qry.$table where $sk_col = -1";
    my ($count) = $dbh->selectrow_array($count_sql);
    if ($count == 0) {
        my $insert_sql = $config->{$key}->{nullrec};
        print "  $insert_sql\n";
        $dbh->do($insert_sql);
    }
}

K_Log::log("[bfw0003] Done");

1;
