#!/usr/bin/perl
use strict;
use File::Basename;
use K_Log;
use lib dirname(__FILE__);

use Bfw::Pipeline;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;

my $stage_sql = <<MULTILINE_SQL;
select
-- UNIQUE BUSINESS KEYS --
pallet_sequences.pallet_number as _pallet_number,
pallet_sequences.pallet_sequence_number as _pallet_sequence_number,
upper(fg_products.fg_product_code) as _fg_product_code,
upper(customer_party_types.party_type_name) as _customer_party_type_name,
upper(customer_roles.role_name) as _customer_role_name,
upper(customer_parties.party_name) as _customer_party_name,
upper(containers.container_code) as _container_code,
upper(ports_discharge.port_type) as _discharge_port_type,
upper(ports_discharge.port_code) as _discharge_port_code, --Extract uses invoices.port_of_discharge
upper(ports_loading.port_type) as _loading_port_type,
upper(ports_loading.port_code) as _loading_port_code, --Extract uses invoices.port_of_loading,
upper(fin_supplier_party_types.party_type_name) as _fin_supplier_party_type_name,
upper(fin_supplier_roles.role_name) as _fin_supplier_role_name,
upper(fin_supplier_parties.party_name) as _fin_supplier_party_name,
upper(deal_types.deal_type) as _deal_type_desc,
upper(inco_terms.inco_term) as _inco_term,
from_currency.currency_sk as _from_currency_sk,     -- b_key for d_exchange_rate
to_currency.currency_sk as _to_currency_sk,         --
exchange_rates.roe_date as _roe_date,               --
--YEAR--
jmt_pallet_sequence_quantity_data.financial_year,
--DATES--
jmt_pallet_sequence_quantity_data.actual_invoice_departure_date,
jmt_pallet_sequence_quantity_data.estimate_invoice_departure_date,
jmt_pallet_sequence_quantity_data.applic_invoice_departure_date,
jmt_pallet_sequence_quantity_data.actual_invoice_arrival_date,
jmt_pallet_sequence_quantity_data.estimate_invoice_arrival_date,
jmt_pallet_sequence_quantity_data.applic_invoice_arrival_date,
jmt_pallet_sequence_quantity_data.account_sale_due_date,
--AGE--
--jmt_pallet_sequence_quantity_data.liquid_age, --Should be in the report so can drill down and up with age recalculating
--DEGENERATE--
voyages.voyage_code,
jmt_customer_invoice_data.customer_account_sale_status,
jmt_customer_invoice_data.customer_account_sale_qc_status,
jmt_customer_invoice_data.customer_account_sale_notes,
jmt_customer_invoice_data.customer_account_sale_finalised,
jmt_customer_invoice_data.invoice_ref_no,
jmt_customer_invoice_data.invoice_amount,
jmt_customer_invoice_data.debit_refs,
jmt_customer_invoice_data.credit_refs,
jmt_customer_invoice_data.debit_amount,
jmt_customer_invoice_data.credit_amount,
jmt_customer_invoice_data.total_invoice_receipt_amount,
jmt_customer_invoice_data.balance_outstanding,
--MEASURES--
jmt_pallet_sequence_quantity_data.act_ctn_qty as no_cartons,
--LOAD DATE--
now() as load_date
from
jmt.pallet_sequences pallet_sequences
join jmt.pallets pallets on pallets.id = pallet_sequences.pallet_id or pallets.pallet_number = pallet_sequences.scrapped_pallet_number
--INVOICE--
join jmt.invoices invoices ON invoices.id = pallet_sequences.invoice_id
--EXCHANGE RATE---
join jmt.exchange_rates ON invoices.exchange_rate_id = exchange_rates.id    -- actual
join jmt.currencies from_currencies ON exchange_rates.from_currency_id = from_currencies.id
join qry.d_currency from_currency ON from_currencies.currency_code = from_currency.currency_code
join jmt.currencies to_currencies ON exchange_rates.to_currency_id = to_currencies.id
join qry.d_currency to_currency ON to_currencies.currency_code = to_currency.currency_code
--INVOICE ITEMS--
join jmt.invoice_items item ON item.id = pallet_sequences.invoice_item_id
join stg.jmt_customer_invoice_data on jmt_customer_invoice_data.id = pallet_sequences.id
--DIM: PRODUCT
join jmt.fg_products fg_products on fg_products.id = pallet_sequences.fg_product_id
left join jmt.item_pack_products item_pack_products on item_pack_products.id = fg_products.item_pack_product_id
left join jmt.varieties varieties on varieties.id = item_pack_products.variety_id
--DIM: DATA and MEASURES
join stg.jmt_pallet_sequence_quantity_data on jmt_pallet_sequence_quantity_data.id = pallet_sequences.id
    and jmt_pallet_sequence_quantity_data.voyage_year > 2012
--DATE: ARRIVAL
left join jmt.load_instruction_details load_instruction_details on pallets.load_instruction_detail_id = load_instruction_details.id
left join jmt.load_instruction_containers load_instruction_containers on pallets.load_instruction_container_id = load_instruction_containers.id
left join jmt.consignment_order_load_instructions consignment_order_load_instructions on COALESCE(load_instruction_containers.consignment_order_load_instruction_id, load_instruction_details.consignment_order_load_instruction_id) = consignment_order_load_instructions.id
left join jmt.consignment_orders consignment_orders on consignment_order_load_instructions.consignment_order_id = consignment_orders.id
left join jmt.consignments consignments on consignment_orders.consignment_id = consignments.id
left join jmt.voyage_ports voyage_ports on consignments.pod_voyage_port_id = voyage_ports.id
left join jmt.voyages voyages on voyages.id = voyage_ports.voyage_id
--DIM: SEAL POINT
left join jmt.load_instructions load_instructions on consignment_order_load_instructions.load_instruction_id = load_instructions.id
--PORTS
left join jmt.voyage_ports voyage_ports_discharge on consignments.pod_voyage_port_id = voyage_ports_discharge.id
left join jmt.voyage_ports voyage_ports_loading on load_instructions.pol_voyage_port_id = voyage_ports_loading.id
left join jmt.ports ports_discharge on ports_discharge.id = voyage_ports_discharge.port_id
left join jmt.ports ports_loading on ports_loading.id = voyage_ports_loading.port_id
--DIM: PARTY - CUSTOMER
left join jmt.parties_roles customer_parties_roles on customer_parties_roles.id = consignments.customer_party_role_id
left join jmt.roles customer_roles on customer_roles.id = customer_parties_roles.role_id
left join jmt.parties customer_parties on customer_parties.id = customer_parties_roles.party_id
left join jmt.party_types customer_party_types on customer_party_types.id = customer_parties.party_type_id
--DIM: FINANCIAL_SUPPLIER
left join jmt.parties_roles fin_supplier_party_roles on fin_supplier_party_roles.id = pallet_sequences.fin_supplier_party_role_id --and fin_supplier_party_roles.role_name = 'FIN_SUPPLIER'
left join jmt.roles fin_supplier_roles on fin_supplier_roles.id = fin_supplier_party_roles.role_id
left join jmt.parties fin_supplier_parties on fin_supplier_parties.id = fin_supplier_party_roles.party_id
left join jmt.party_types fin_supplier_party_types on fin_supplier_party_types.id = fin_supplier_parties.party_type_id
--DIM: LOCATION
left join jmt.locations locations on locations.location_code = pallets.location_code
--DIM: CONTAINER_CODE
left join jmt.containers containers on containers.id = load_instruction_containers.container_id AND consignments.id = containers.consignment_id
--DIM: SEASON
left join jmt.seasons seasons on seasons.season_code = pallet_sequences.season_code
--DIM: DEAL_TYPES & INCO_TERM
left join jmt.deal_types deal_types ON deal_types.id = invoices.deal_type_id
left join jmt.inco_terms inco_terms ON inco_terms.id = invoices.inco_term_id
where pallet_sequences.pallet_number IS NOT NULL
and pallet_sequences.carton_quantity > 0
and invoices.approved = 't'
and not invoices.cancelled
-- and pallet_sequences.customer_account_sale_id is null        -- THIS IS DONE IN THE NEXT SQL
MULTILINE_SQL


my $publish_sql = <<MULTILINE_SQL;
SELECT coalesce(public.d_pallet_sequence.pallet_sequence_sk, -1) as pallet_sequence_sk,
       coalesce(public.d_fg_product.fg_product_sk, -1) as fg_product_sk,
       coalesce(public.d_customer.customer_sk, -1) as customer_sk,
       coalesce(public.d_container.container_sk, -1) as container_sk,
       coalesce(public.d_port_of_discharge.port_of_discharge_sk, -1) as port_of_discharge_sk,
       coalesce(public.d_port_of_loading.port_of_loading_sk, -1) as port_of_loading_sk,
       coalesce(public.d_fin_supplier.fin_supplier_sk, -1) as fin_supplier_sk,
       coalesce(public.d_inco_term.inco_term_sk, -1) as inco_term_sk,
       coalesce(public.d_deal_type.deal_type_sk, -1) as deal_type_sk,
       coalesce(d_exchange_rate.exchange_rate_sk, -1) as exchange_rate_sk,
       stg.f_outstanding_account_sales_as.financial_year,
	--DATES--
    mk_date_sk(stg.f_outstanding_account_sales_as.actual_invoice_departure_date) as actual_invoice_departure_date_sk,
	mk_date_sk(stg.f_outstanding_account_sales_as.estimate_invoice_departure_date) as estimate_invoice_departure_date_sk,
	mk_date_sk(stg.f_outstanding_account_sales_as.applic_invoice_departure_date) as applic_invoice_departure_date_sk,
    mk_date_sk(stg.f_outstanding_account_sales_as.actual_invoice_arrival_date) as actual_invoice_arrival_date_sk,
	mk_date_sk(stg.f_outstanding_account_sales_as.estimate_invoice_arrival_date) as estimate_invoice_arrival_date_sk,
	mk_date_sk(stg.f_outstanding_account_sales_as.applic_invoice_arrival_date) as applic_invoice_arrival_date_sk,
	mk_date_sk(stg.f_outstanding_account_sales_as.account_sale_due_date) as account_sale_due_date_sk,
	--AGE--
	--jmt_pallet_sequence_quantity_data.liquid_age, --Should be in the report so can drill down and up with age recalculating
	--DEGENERATE--
	stg.f_outstanding_account_sales_as.voyage_code,
    stg.f_outstanding_account_sales_as.customer_account_sale_status,
    stg.f_outstanding_account_sales_as.customer_account_sale_qc_status,
    stg.f_outstanding_account_sales_as.customer_account_sale_notes,
    stg.f_outstanding_account_sales_as.customer_account_sale_finalised,
	stg.f_outstanding_account_sales_as.invoice_ref_no,
	--MEASURES--
	stg.f_outstanding_account_sales_as.no_cartons,
    stg.f_outstanding_account_sales_as.invoice_amount,
    stg.f_outstanding_account_sales_as.debit_refs,
    stg.f_outstanding_account_sales_as.credit_refs,
    stg.f_outstanding_account_sales_as.debit_amount,
    stg.f_outstanding_account_sales_as.credit_amount,
    stg.f_outstanding_account_sales_as.total_invoice_receipt_amount,
    stg.f_outstanding_account_sales_as.balance_outstanding,
    --LOAD DATE--
    now() as load_date
from stg.f_outstanding_account_sales_as
        -- THIS JOIN IS TO ALLOW ME TO REMOVE TH CONTRAINT IN PREV SQL
join public.d_pallet_sequence on stg.f_outstanding_account_sales_as._pallet_number = d_pallet_sequence.pallet_number
    and stg.f_outstanding_account_sales_as._pallet_sequence_number = d_pallet_sequence.pallet_sequence_number
join jmt.pallet_sequences on d_pallet_sequence.pallet_number = pallet_sequences.pallet_number
    and d_pallet_sequence.pallet_sequence_number = pallet_sequences.pallet_sequence_number
    and pallet_sequences.customer_account_sale_id is null
left join public.d_fg_product on stg.f_outstanding_account_sales_as._fg_product_code = d_fg_product.fg_product_code
left join public.d_customer on stg.f_outstanding_account_sales_as._customer_party_type_name = public.d_customer.party_type_name
    and stg.f_outstanding_account_sales_as._customer_party_name = public.d_customer.customer_name
left join public.d_container on stg.f_outstanding_account_sales_as._container_code = public.d_container.container_code
left join public.d_port_of_discharge on stg.f_outstanding_account_sales_as._discharge_port_type = public.d_port_of_discharge.port_of_discharge_type
    and stg.f_outstanding_account_sales_as._discharge_port_code = public.d_port_of_discharge.port_of_discharge_code
left join public.d_port_of_loading on stg.f_outstanding_account_sales_as._loading_port_type = public.d_port_of_loading.port_of_loading_type
    and stg.f_outstanding_account_sales_as._loading_port_code = public.d_port_of_loading.port_of_loading_code
left join public.d_fin_supplier on stg.f_outstanding_account_sales_as._fin_supplier_party_type_name = public.d_fin_supplier.party_type_name
    and stg.f_outstanding_account_sales_as._fin_supplier_party_name = public.d_fin_supplier.fin_supplier_name
left join public.d_inco_term on stg.f_outstanding_account_sales_as._inco_term = public.d_inco_term.inco_term
left join public.d_deal_type on stg.f_outstanding_account_sales_as._deal_type_desc = public.d_deal_type.deal_type_desc
left join public.d_exchange_rate on stg.f_outstanding_account_sales_as._from_currency_sk = public.d_exchange_rate.from_currency_sk
    and stg.f_outstanding_account_sales_as._to_currency_sk = public.d_exchange_rate.to_currency_sk
    and mk_date_sk(stg.f_outstanding_account_sales_as._roe_date) = public.d_exchange_rate.roe_date_sk
MULTILINE_SQL

# Run $stage_sql into stg.f_pallet_sequence_sales_as
K_Log::log("[bfw0007] Building stg.f_outstanding_account_sales_as");
Pipeline->new(
    {
        dataset => 'f_outstanding_account_sales_as',
        action  => 'Stage',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $stage_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'stg',
                tablename => 'f_outstanding_account_sales_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();


# Truncate and copy stg.f_container_shipped_as into qry.f_pallet_sequence_sales_as
K_Log::log("[bfw0007] Building qry.f_outstanding_account_sales_as");
Pipeline->new(
    {
        dataset => 'f_outstanding_account_sales_as',
        action  => 'Move',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $publish_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'qry',
                tablename => 'f_outstanding_account_sales_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();

K_Log::log("[bfw0007] Done");

1;
