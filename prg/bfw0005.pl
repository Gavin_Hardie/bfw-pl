#!/usr/bin/perl
use strict;
use File::Basename;
use K_Log;
use lib dirname(__FILE__);

use Bfw::Pipeline;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;

my $stage_sql = <<MULTILINE_SQL;
select
--DIMENSIONS--
a.id,
a._container_container_code,
a._fg_product_product_code,
a._shipping_country_code,
a._current_location_location_type_code,
a._current_location_location_code,
a._ports_discharge_port_type,
a._ports_discharge_port_code,
a._customer_party_type_name,
a._customer_role_name,
a._customer_customer_name,
a._carrier_party_types_party_type_name,
a._carrier_roles_role_name,
a._carrier_parties_party_name,
a._forwarding_agent_party_types_party_type_name,
a._forwarding_agent_roles_role_name,
a._forwarding_agent_parties_party_name,
a._shipping_line_party_types_party_type_name,
a._shipping_line_roles_role_name,
a._shipping_line_party_name,
a._season_season_type,
a._season_season_code,
a._voyage_id,
--DATE--
a.voyage_year,
a.applic_voyage_departure_date,
--MEASURES--
sum(a.pallet_size) as pallet_size,
sum(a.pallet_size/b.pallet_size) as equivalent_container_qty,
--LOAD DATE--
now() as load_date
from (
        select
        --JOINING ATTRIBUTE--
        upper(containers.container_code) as container_code,
        --DIMESIONS--
        upper(containers.container_code) as _container_container_code,
        upper(fg_products.fg_product_code) as _fg_product_product_code,
        pallet_sequences.country_code as _shipping_country_code,
        upper(locations.location_type_code) as _current_location_location_type_code,
        upper(locations.location_code) as _current_location_location_code,
        upper(ports_discharge.port_type) as _ports_discharge_port_type,
        upper(ports_discharge.port_code) as _ports_discharge_port_code,
        upper(customer_party_types.party_type_name) as _customer_party_type_name,
        upper(customer_roles.role_name) as _customer_role_name,
        upper(customer_parties.party_name) as _customer_customer_name,
        upper(carrier_party_types.party_type_name) as _carrier_party_types_party_type_name,
        upper(carrier_roles.role_name) as _carrier_roles_role_name,
        upper(carrier_parties.party_name) as _carrier_parties_party_name,
        upper(forwarding_agent_party_types.party_type_name) as _forwarding_agent_party_types_party_type_name,
        upper(forwarding_agent_roles.role_name) as _forwarding_agent_roles_role_name,
        upper(forwarding_agent_parties.party_name) as _forwarding_agent_parties_party_name,
        upper(shipping_line_party_types.party_type_name) as _shipping_line_party_types_party_type_name,
        upper(shipping_line_roles.role_name) as _shipping_line_roles_role_name,
        upper(shipping_line_parties.party_name) as _shipping_line_party_name,
        upper(seasons.season_type) as _season_season_type,
        upper(seasons.season_code) as _season_season_code,
        voyages.id as _voyage_id,
        --DATES--
        jmt_pallet_sequence_quantity_data.voyage_year,
        jmt_pallet_sequence_quantity_data.applic_voyage_departure_date as applic_voyage_departure_date,
        --MEASURES--
        pallet_sequences.id,
        sum(jmt_pallet_sequence_quantity_data.pallet_size) as pallet_size
        from
        jmt.pallet_sequences pallet_sequences
        JOIN jmt.pallets pallets ON pallets.id = pallet_sequences.pallet_id
        --DATE & PALLET_SIZE
        join stg.jmt_pallet_sequence_quantity_data on jmt_pallet_sequence_quantity_data.id = pallet_sequences.id
        --DIM: PRODUCT
        left join jmt.fg_products fg_products ON fg_products.id = pallet_sequences.fg_product_id
        left join jmt.item_pack_products item_pack_products ON item_pack_products.id = fg_products.item_pack_product_id
        left join jmt.varieties varieties ON varieties.id = item_pack_products.variety_id
        left join jmt.suppliers suppliers ON suppliers.supplier_parties_role_id = pallet_sequences.fin_supplier_party_role_id
        --DATE: ARRIVAL
        left join jmt.load_instruction_details load_instruction_details ON pallets.load_instruction_detail_id = load_instruction_details.id
        left join jmt.load_instruction_containers load_instruction_containers ON pallets.load_instruction_container_id = load_instruction_containers.id
        left join jmt.consignment_order_load_instructions consignment_order_load_instructions ON COALESCE(load_instruction_containers.consignment_order_load_instruction_id, load_instruction_details.consignment_order_load_instruction_id) = consignment_order_load_instructions.id
        left join jmt.consignment_orders consignment_orders ON consignment_order_load_instructions.consignment_order_id = consignment_orders.id
        left join jmt.consignments consignments ON consignment_orders.consignment_id = consignments.id
        left join jmt.voyage_ports voyage_ports ON consignments.pod_voyage_port_id = voyage_ports.id
        left join jmt.voyages voyages ON voyages.id = voyage_ports.voyage_id
        left join jmt.vessels vessels on vessels.vessel_code = voyages.vessel_code
        --PORTS
        left join jmt.voyage_ports voyage_ports_discharge ON consignments.pod_voyage_port_id = voyage_ports_discharge.id
        left join jmt.load_instructions load_instructions on load_instructions.id = consignment_order_load_instructions.load_instruction_id
        left join jmt.voyage_ports voyage_ports_loading ON load_instructions.pol_voyage_port_id = voyage_ports_loading.id
        left join jmt.ports ports_discharge on ports_discharge.id = voyage_ports_discharge.port_id
        left join jmt.ports ports_loading on ports_loading.id = voyage_ports_loading.port_id
        --DIM: CONTAINER_CODE
        left join jmt.containers containers on containers.id = load_instruction_containers.container_id AND consignments.id = containers.consignment_id
        --LINK: DIM: SHIPPING_LINE, FORWARDING AGENT
        left join jmt.vessel_bookings vessel_bookings on containers.vessel_booking_id = vessel_bookings.id
        --DIM: PARTY - CUSTOMER
        left join jmt.parties_roles customer_parties_roles ON customer_parties_roles.id = consignments.customer_party_role_id
        left join jmt.roles customer_roles on customer_roles.id = customer_parties_roles.role_id
        left join jmt.parties customer_parties on customer_parties.id = customer_parties_roles.party_id
        left join jmt.party_types customer_party_types on customer_party_types.id = customer_parties.party_type_id
        --DIM: PARTY - CARRIER
        left join jmt.parties_roles carrier_parties_roles ON carrier_parties_roles.id = containers.carrier_party_role_id
        left join jmt.roles carrier_roles on carrier_roles.id = carrier_parties_roles.role_id
        left join jmt.parties carrier_parties on carrier_parties.id = carrier_parties_roles.party_id
        left join jmt.party_types carrier_party_types on carrier_party_types.id = carrier_parties.party_type_id
        --DIM: PARTY - FORWARDING AGENT
        left join jmt.parties_roles forwarding_agent_parties_roles ON forwarding_agent_parties_roles.id = vessel_bookings.forw_agent_party_role_id
        left join jmt.roles forwarding_agent_roles on forwarding_agent_roles.id = forwarding_agent_parties_roles.role_id
        left join jmt.parties forwarding_agent_parties on forwarding_agent_parties.id = forwarding_agent_parties_roles.party_id
        left join jmt.party_types forwarding_agent_party_types on forwarding_agent_party_types.id = forwarding_agent_parties.party_type_id
        --DIM: FINANCIAL_SUPPLIER
        left join jmt.parties_roles fin_supplier_party_roles on fin_supplier_party_roles.id = pallet_sequences.fin_supplier_party_role_id --and fin_supplier_party_roles.role_name = 'FIN_SUPPLIER'
        left join jmt.roles fin_supplier_roles on fin_supplier_roles.id = fin_supplier_party_roles.role_id
        left join jmt.parties fin_supplier_parties on fin_supplier_parties.id = fin_supplier_party_roles.party_id
        left join jmt.party_types fin_supplier_party_types on fin_supplier_party_types.id = fin_supplier_parties.party_type_id
        --left join jmt.suppliers suppliers on suppliers.supplier_parties_role_id = fin_supplier_party_roles.id
        left join jmt.suppliers_supplier_groups suppliers_supplier_groups on suppliers_supplier_groups.supplier_id = suppliers.id
            and suppliers_supplier_groups.rank = 1
        left join jmt.supplier_groups supplier_groups on supplier_groups.id = suppliers_supplier_groups.supplier_group_id
        left join jmt.supplier_super_groups supplier_super_groups on supplier_super_groups.id = supplier_groups.supplier_super_group_id
        --DIM: PARTY - SHIPPING_LINE
        left join jmt.parties_roles shipping_line_parties_roles ON shipping_line_parties_roles.id = vessel_bookings.shipping_line_party_role_id
        left join jmt.roles shipping_line_roles on shipping_line_roles.id = shipping_line_parties_roles.role_id
        left join jmt.parties shipping_line_parties on shipping_line_parties.id = shipping_line_parties_roles.party_id
        left join jmt.party_types shipping_line_party_types on shipping_line_party_types.id = shipping_line_parties.party_type_id
        --DIM: LOCATION
        left join jmt.locations locations on locations.location_code = pallets.location_code
        --DIM: SEASON
        left join jmt.seasons seasons on seasons.season_code = pallet_sequences.season_code
        where
        pallet_sequences.pallet_number IS NOT NULL AND
        pallet_sequences.carton_quantity > 0 and
        jmt_pallet_sequence_quantity_data.valid = TRUE and
        jmt_pallet_sequence_quantity_data.voyage_year > 2012 and
        supplier_super_groups.supplier_super_group_code <> 'FRA-SW' and
        jmt_pallet_sequence_quantity_data.applic_voyage_departure_date is not NULL and
        shipping_line_parties.party_name <> ''
        group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26) as a
left join ( select
            --JOINING ATTRIBUTE--
            upper(containers.container_code) as container_code,
            sum(jmt_pallet_sequence_quantity_data.pallet_size) as pallet_size
            from
            jmt.pallet_sequences pallet_sequences
            JOIN jmt.pallets pallets ON pallets.id = pallet_sequences.pallet_id
            --DATE & PALLET_SIZE
            join stg.jmt_pallet_sequence_quantity_data on jmt_pallet_sequence_quantity_data.id = pallet_sequences.id
            left join jmt.load_instruction_details load_instruction_details ON pallets.load_instruction_detail_id = load_instruction_details.id
            left join jmt.load_instruction_containers load_instruction_containers ON pallets.load_instruction_container_id = load_instruction_containers.id
            left join jmt.consignment_order_load_instructions consignment_order_load_instructions ON COALESCE(load_instruction_containers.consignment_order_load_instruction_id, load_instruction_details.consignment_order_load_instruction_id) = consignment_order_load_instructions.id
            left join jmt.consignment_orders consignment_orders ON consignment_order_load_instructions.consignment_order_id = consignment_orders.id
            left join jmt.consignments consignments ON consignment_orders.consignment_id = consignments.id
            left join jmt.voyage_ports voyage_ports ON consignments.pod_voyage_port_id = voyage_ports.id
            left join jmt.voyages voyages ON voyages.id = voyage_ports.voyage_id
            left join jmt.vessels vessels on vessels.vessel_code = voyages.vessel_code
            left join jmt.containers containers on containers.id = load_instruction_containers.container_id AND consignments.id = containers.consignment_id
            --LINK: DIM: SHIPPING_LINE, FORWARDING AGENT
            left join jmt.vessel_bookings vessel_bookings on containers.vessel_booking_id = vessel_bookings.id
            --DIM: FINANCIAL_SUPPLIER
            left join jmt.parties_roles fin_supplier_party_roles on fin_supplier_party_roles.id = pallet_sequences.fin_supplier_party_role_id --and fin_supplier_party_roles.role_name = 'FIN_SUPPLIER'
            left join jmt.roles fin_supplier_roles on fin_supplier_roles.id = fin_supplier_party_roles.role_id
            left join jmt.parties fin_supplier_parties on fin_supplier_parties.id = fin_supplier_party_roles.party_id
            left join jmt.party_types fin_supplier_party_types on fin_supplier_party_types.id = fin_supplier_parties.party_type_id
            left join jmt.suppliers suppliers on suppliers.supplier_parties_role_id = fin_supplier_party_roles.id
            left join jmt.suppliers_supplier_groups suppliers_supplier_groups on suppliers_supplier_groups.supplier_id = suppliers.id
                    and suppliers_supplier_groups.rank = 1
            left join jmt.supplier_groups supplier_groups on supplier_groups.id = suppliers_supplier_groups.supplier_group_id
            left join jmt.supplier_super_groups supplier_super_groups on supplier_super_groups.id = supplier_groups.supplier_super_group_id
            --DIM: PARTY - SHIPPING_LINE
            left join jmt.parties_roles shipping_line_parties_roles ON shipping_line_parties_roles.id = vessel_bookings.shipping_line_party_role_id
            left join jmt.roles shipping_line_roles on shipping_line_roles.id = shipping_line_parties_roles.role_id
            left join jmt.parties shipping_line_parties on shipping_line_parties.id = shipping_line_parties_roles.party_id
            left join jmt.party_types shipping_line_party_types on shipping_line_party_types.id = shipping_line_parties.party_type_id
            where
            pallet_sequences.pallet_number IS NOT NULL AND
            pallet_sequences.carton_quantity > 0 and
            jmt_pallet_sequence_quantity_data.valid = TRUE and
            jmt_pallet_sequence_quantity_data.voyage_year > 2012 and
            supplier_super_groups.supplier_super_group_code <> 'FRA-SW' and
            jmt_pallet_sequence_quantity_data.applic_voyage_departure_date is not NULL and
            shipping_line_parties.party_name <> ''
            group by 1) as b on a.container_code = b.container_code
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25
MULTILINE_SQL


my $publish_sql = <<MULTILINE_SQL;
select  f_container_shipped_as.id,
        coalesce(qry.d_container.container_sk, -1) as container_sk,
        coalesce(qry.d_fg_product.fg_product_sk, -1) as fg_product_sk,
        coalesce(shipping.shipping_target_market_sk, -1) as shipping_target_market_sk,
        coalesce(qry.d_location.location_sk, -1) as location_sk,
        coalesce(qry.d_port.port_sk, -1) as port_of_discharge_sk,
        coalesce(customer.customer_sk, -1) as customer_sk,
        coalesce(carrier_party.carrier_party_sk, -1) as carrier_party_sk,
        coalesce(forwarding_agent.forwarding_agent_party_sk, -1) as forwarding_agent_party_sk,
        coalesce(shipping_line.shipping_line_party_sk, -1) as shipping_line_party_sk,
        coalesce(qry.d_season.season_sk, -1) as season_sk,
        coalesce(qry.d_voyage.voyage_sk, -1) as voyage_sk,
        stg.f_container_shipped_as.voyage_year as voyage_year,
        mk_date_sk(stg.f_container_shipped_as.applic_voyage_departure_date) as applic_voyage_departure_date_sk,
        stg.f_container_shipped_as.pallet_size as pallet_size,
        stg.f_container_shipped_as.equivalent_container_qty as equivalent_container_qty,
        now() as load_date
from stg.f_container_shipped_as
left join qry.d_container on stg.f_container_shipped_as._container_container_code = d_container.container_code
left join qry.d_fg_product on stg.f_container_shipped_as._fg_product_product_code = d_fg_product.fg_product_code
left join public.d_shipping_target_market as shipping on stg.f_container_shipped_as._shipping_country_code = shipping.country_code
left join qry.d_location on stg.f_container_shipped_as._current_location_location_type_code = qry.d_location.location_type_code
    and stg.f_container_shipped_as._current_location_location_code = qry.d_location.location_code
left join qry.d_port on stg.f_container_shipped_as._ports_discharge_port_type = qry.d_port.port_type
    and stg.f_container_shipped_as._ports_discharge_port_code = qry.d_port.port_code
left join public.d_customer as customer on stg.f_container_shipped_as._customer_party_type_name = customer.party_type_name
    and stg.f_container_shipped_as._customer_customer_name = customer.customer_name
left join public.d_carrier_party as carrier_party on stg.f_container_shipped_as._carrier_party_types_party_type_name = carrier_party.carrier_party_type_nm
    and stg.f_container_shipped_as._carrier_parties_party_name = carrier_party.carrier_party_nm
left join public.d_forwarding_agent_party as forwarding_agent on stg.f_container_shipped_as._forwarding_agent_party_types_party_type_name = forwarding_agent.forwarding_agent_party_type_nm
    and stg.f_container_shipped_as._forwarding_agent_parties_party_name = forwarding_agent.forwarding_agent_party_nm
left join public.d_shipping_line_party as shipping_line on stg.f_container_shipped_as._shipping_line_party_types_party_type_name = shipping_line.shipping_line_party_type_nm
    and stg.f_container_shipped_as._shipping_line_party_name = shipping_line.shipping_line_party_nm
left join qry.d_season on stg.f_container_shipped_as._season_season_type = qry.d_season.season_type
    and stg.f_container_shipped_as._season_season_code = qry.d_season.season_code
left join qry.d_voyage on stg.f_container_shipped_as._voyage_id = qry.d_voyage.id
MULTILINE_SQL

# Run $stage_sql into stg.f_container_shipped_as
K_Log::log("[bfw0005] Building stg.f_container_shipped_as");
Pipeline->new(
    {
        dataset => 'f_container_shipped_as',
        action  => 'Stage',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $stage_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'stg',
                tablename => 'f_container_shipped_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();


# Truncate and copy stg.f_container_shipped_as into qry.f_container_shipped_as
K_Log::log("[bfw0005] Building qry.f_container_shipped_as");
Pipeline->new(
    {
        dataset => 'f_container_shipped_as',
        action  => 'Stage',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $publish_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'qry',
                tablename => 'f_container_shipped_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();

K_Log::log("[bfw0005] Done");

1;
