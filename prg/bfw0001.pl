#!/usr/bin/perl

use strict;
use File::Basename;
use lib dirname(__FILE__);
use Data::Dumper;
use YAML::Tiny;
use K_Log;

use Bfw::Pipeline;
use Bfw::PipeManager;
use Bfw::Source::TableSource;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;

my $yaml    = YAML::Tiny->read("$ENV{MIS}/prg/bfw0001.yml");
my $config  = $yaml->[0];
my $manager = PipeManager->new( { alias => 'UNI' } );

foreach my $table ( sort keys %{$config} ) {

    K_Log::log("[bfw0001] Loading jmt.$table");

    my $columns = $config->{$table}->{unique_cols};
    my $select_clause;
    if ( defined($columns) ) {
        my $column_list = join(
            ', ',
            map {
                if   ( $_ =~ /_id$/ ) { $_ }
                else                  { "upper($_)" }
            } @{$columns}
        );
        $select_clause =
            "*, rank() over (partition by $column_list order by "
          . "coalesce(active, false) desc, id desc) as rank";
    }
    else {
        $select_clause = "*, 1 as rank";
    }

    my @pipes = ();

    push @pipes,
      Pipeline->new(
        {
            dataset => $table,
            action  => 'Land',
            source  => SelectSource->new(
                {
                    alias  => 'JMT',
                    select => "select $select_clause from public.$table"
                }
            ),
            target => TableWriter->new(
                {
                    alias            => 'UNI',
                    schema           => 'jmt',
                    tablename        => $table,
                    truncate         => 1,
                    #force_row_by_row => 1,  This is too slow at the moment
                }
            ),
            datatypeCheck => 0,
        }
      );
    $manager->run_pipes(@pipes);

}

K_Log::log("[bfw0001] Done");

1;
