#!/usr/bin/bash

touch /tmp/ubi.lock 2>/dev/null

(
  # Wait for lock on /var/lock/ubi/exclusivelock (fd 200) for 10 seconds
  flock -x -w 10 200 || exit 1

  $MIS/prg/bfw0001.pl >> $MIS/log/bfw0001.log 2>&1
  $MIS/prg/bfw0002.pl >> $MIS/log/bfw0002.log 2>&1
  $MIS/prg/bfw0003.pl >> $MIS/log/bfw0003.log 2>&1

  parallel $MIS/prg/bfw{}.pl >> $MIS/log/bfw{}.log 2>&1 ::: 0004 0005 0006 0007

) 200</tmp/ubi.lock
