#!/usr/bin/perl
use strict;
use File::Basename;
use K_Log;
use lib dirname(__FILE__);

use Bfw::Pipeline;
use Bfw::Source::SelectSource;
use Bfw::Target::TableWriter;

my $stage_sql = <<MULTILINE_SQL;
select
-- PRIMARY KEY --
pallet_sequences.id,
-- UNIQUE BUSINESS KEYS --
pallets.pallet_number as _pallet_number,
pallet_sequences.pallet_sequence_number as _pallet_sequence_number,
upper(pallet_sequences.farm_code) as _farm_code,
upper(fg_products.fg_product_code) as _fg_product_code,
upper(pallet_sequences.country_code) as _marketing_country_code,
upper(pallet_sequences.marketing_tm_group) as _marketing_target_market_group,
upper(pallet_sequences.country_code) as _shipping_country_code,
upper(pallet_sequences.packed_tm_group) as _packed_country_code,
upper(final_receiver_party_types.party_type_name) as _final_receiver_party_type_name,
upper(final_receiver_roles.role_name) as _final_receiver_role_name,
upper(final_receiver_parties.party_name) as _final_receiver_party_name,
upper(exporter_party_types.party_type_name) as _exporter_party_type_name,
upper(exporter_roles.role_name) as _exporter_role_name,
upper(pallet_sequences.organization_code) as _organization_name,
upper(exporter_parties.party_name) as _exporter_party_name,
upper(forward_agent_party_types.party_type_name) as _forwarding_agent_party_type_name,
upper(forward_agent_roles.role_name) as _forwarding_agent_role_name,
upper(forward_agent_parties.party_name) as _forwarding_agent_party_name,
upper(shipping_line_party_types.party_type_name) as _shipping_line_party_type_name,
upper(shipping_line_roles.role_name) as _shipping_line_role_name,
upper(shipping_line_parties.party_name) as _shipping_line_party_name,
upper(customer_party_types.party_type_name) as _customer_party_type_name,
upper(customer_roles.role_name) as _customer_role_name,
upper(customer_parties.party_name) as _customer_party_name,
upper(vessels.vessel_type_code) as _vessel_type_code,
upper(vessels.vessel_code) as _vessel_code,
upper(locations.location_type_code) as _location_type_code,
upper(locations.location_code) as _location_code,
upper(containers.container_code) as _container_code,
upper(load_instructions.depot_code) as _load_instructions_depot_code,
upper(ports_discharge.port_type) as _discharge_port_type,
upper(ports_discharge.port_code) as _discharge_port_code,
upper(ports_loading.port_type) as _loading_port_type,
upper(ports_loading.port_code) as _loading_port_code,
upper(fin_supplier_party_types.party_type_name) as _fin_supplier_party_type_name,
upper(fin_supplier_roles.role_name) as _fin_supplier_role_name,
upper(fin_supplier_parties.party_name) as _fin_supplier_party_name,
upper(seasons.season_type) as _season_type,
upper(seasons.season_code) as _season_code,
upper(cities.city_code) as _city_code,
pallets.pallet_number as _stock_item_inventory_reference,
voyages.id as _voyage_id,
shipping_docs.id as _shipping_doc_id,
--DATES--
jmt_pallet_sequence_quantity_data.packed_date,
jmt_pallet_sequence_quantity_data.inspection_date,
jmt_pallet_sequence_quantity_data.intake_date,
jmt_pallet_sequence_quantity_data.cold_date,
jmt_pallet_sequence_quantity_data.exit_date,
jmt_pallet_sequence_quantity_data.reinspection_date,
jmt_pallet_sequence_quantity_data.load_out_date,
jmt_pallet_sequence_quantity_data.applic_shipment_date,
jmt_pallet_sequence_quantity_data.voyage_year,
jmt_pallet_sequence_quantity_data.actual_voyage_departure_date,
jmt_pallet_sequence_quantity_data.estimate_voyage_departure_date,
jmt_pallet_sequence_quantity_data.applic_voyage_departure_date,
jmt_pallet_sequence_quantity_data.actual_voyage_arrival_date,
jmt_pallet_sequence_quantity_data.estimate_voyage_arrival_date,
jmt_pallet_sequence_quantity_data.applic_voyage_arrival_date,
jmt_pallet_sequence_quantity_data.gate_out_date,
pallets.original_intake_date,
pallets.re_inspected_for_ambient_date as reinspected_for_ambient_date,
pallet_sequences.original_inspection_date,
--AGE--
jmt_pallet_sequence_quantity_data.stock_age,
jmt_pallet_sequence_quantity_data.max_age_on_arrival,
jmt_pallet_sequence_quantity_data.original_ambient_age,
jmt_pallet_sequence_quantity_data.original_inspection_age,
jmt_pallet_sequence_quantity_data.actual_voyage_arrival_age,
jmt_pallet_sequence_quantity_data.liquid_age,
jmt_pallet_sequence_quantity_data.shipped_age,
jmt_pallet_sequence_quantity_data.storage_age,
jmt_pallet_sequence_quantity_data.pod_age,
jmt_pallet_sequence_quantity_data.intake_age,
jmt_pallet_sequence_quantity_data.original_intake_age,
--DEGENERATE--
jmt_pallet_sequence_quantity_data.valid,
jmt_pallet_sequence_quantity_data.archived,
voyages.voyage_code,
vessels_voyages.voyage_number as ship_number,
jmt_pallet_sequence_quantity_data.supply_chain_status,
--MEASURES--
jmt_pallet_sequence_quantity_data.storage_amt_excl,
jmt_pallet_sequence_quantity_data.storage_vat,
jmt_pallet_sequence_quantity_data.storage_amt_incl,
jmt_pallet_sequence_quantity_data.storage_rate_type,
jmt_pallet_sequence_quantity_data.pallet_size,
jmt_pallet_sequence_quantity_data.orig_std_ctn_qty,
jmt_pallet_sequence_quantity_data.std_ctn_qty,
jmt_pallet_sequence_quantity_data.act_ctn_qty,
jmt_pallet_sequence_quantity_data.nett_weight_per_carton,
jmt_pallet_sequence_quantity_data.gross_weight_per_carton,
pallets.gross_mass,
--LOAD DATE--
now() as load_date
from
jmt.pallet_sequences
join jmt.pallets pallets on pallets.id = pallet_sequences.pallet_id or pallet_sequences.scrapped_pallet_number = pallets.pallet_number
--DIM: PRODUCT
left join jmt.fg_products fg_products on fg_products.id = pallet_sequences.fg_product_id
left join jmt.item_pack_products item_pack_products on item_pack_products.id = fg_products.item_pack_product_id
left join jmt.varieties varieties on varieties.id = item_pack_products.variety_id
--DIM: DATA and MEASURES
left join stg.jmt_pallet_sequence_quantity_data on jmt_pallet_sequence_quantity_data.id = pallet_sequences.id and jmt_pallet_sequence_quantity_data.voyage_year > 2012
--DATE: ARRIVAL
left join jmt.load_instruction_details load_instruction_details on pallets.load_instruction_detail_id = load_instruction_details.id
left join jmt.load_instruction_containers load_instruction_containers on pallets.load_instruction_container_id = load_instruction_containers.id
left join jmt.consignment_order_load_instructions consignment_order_load_instructions on COALESCE(load_instruction_containers.consignment_order_load_instruction_id, load_instruction_details.consignment_order_load_instruction_id) = consignment_order_load_instructions.id
left join jmt.consignment_orders consignment_orders on consignment_order_load_instructions.consignment_order_id = consignment_orders.id
left join jmt.consignments consignments on consignment_orders.consignment_id = consignments.id
left join jmt.voyage_ports voyage_ports on consignments.pod_voyage_port_id = voyage_ports.id
left join jmt.voyages voyages on voyages.id = voyage_ports.voyage_id
left join jmt.shipping_docs shipping_docs on shipping_docs.consignment_id=consignments.id
--DIM: SEAL POINT
left join jmt.load_instructions load_instructions on consignment_order_load_instructions.load_instruction_id = load_instructions.id
--PORTS
left join jmt.voyage_ports voyage_ports_discharge on consignments.pod_voyage_port_id = voyage_ports_discharge.id
left join jmt.voyage_ports voyage_ports_loading on load_instructions.pol_voyage_port_id = voyage_ports_loading.id
left join jmt.ports ports_discharge on ports_discharge.id = voyage_ports_discharge.port_id
left join jmt.ports ports_loading on ports_loading.id = voyage_ports_loading.port_id
--DIM: VESSELS
left join jmt.voyages vessels_voyages on vessels_voyages.id = load_instructions.voyage_id
left join jmt.vessels vessels on vessels.id = vessels_voyages.vessel_id
--DIM: PARTY - FINAL_RECEIVER
left join jmt.parties_roles final_receiver_parties_roles on final_receiver_parties_roles.id = consignments.final_receiver_party_role_id
left join jmt.roles final_receiver_roles on final_receiver_roles.id = final_receiver_parties_roles.role_id
left join jmt.parties final_receiver_parties on final_receiver_parties.id = final_receiver_parties_roles.party_id
left join jmt.party_types final_receiver_party_types on final_receiver_party_types.id = final_receiver_parties.party_type_id
--DIM: PARTY - CUSTOMER
left join jmt.parties_roles customer_parties_roles on customer_parties_roles.id = consignments.customer_party_role_id
left join jmt.roles customer_roles on customer_roles.id = customer_parties_roles.role_id
left join jmt.parties customer_parties on customer_parties.id = customer_parties_roles.party_id
left join jmt.party_types customer_party_types on customer_party_types.id = customer_parties.party_type_id
--DIM: FINANCIAL_SUPPLIER
left join jmt.parties_roles fin_supplier_party_roles on fin_supplier_party_roles.id = pallet_sequences.fin_supplier_party_role_id --and fin_supplier_party_roles.role_name = 'FIN_SUPPLIER'
left join jmt.roles fin_supplier_roles on fin_supplier_roles.id = fin_supplier_party_roles.role_id
left join jmt.parties fin_supplier_parties on fin_supplier_parties.id = fin_supplier_party_roles.party_id
left join jmt.party_types fin_supplier_party_types on fin_supplier_party_types.id = fin_supplier_parties.party_type_id
--DIM: LOCATION
left join jmt.locations locations on locations.location_code = pallets.location_code
--DIM: CONTAINER_CODE
left join jmt.containers containers on containers.id = load_instruction_containers.container_id AND consignments.id = containers.consignment_id
--DIM: PARTY - SHIPPING_LINE
left join jmt.vessel_bookings vessel_bookings on containers.vessel_booking_id = vessel_bookings.id
left join jmt.parties_roles shipping_line_parties_roles on shipping_line_parties_roles.id = vessel_bookings.shipping_line_party_role_id
left join jmt.roles shipping_line_roles on shipping_line_roles.id = shipping_line_parties_roles.role_id
left join jmt.parties shipping_line_parties on shipping_line_parties.id = shipping_line_parties_roles.party_id
left join jmt.party_types shipping_line_party_types on shipping_line_party_types.id = shipping_line_parties.party_type_id
--DIM: EXPORTER
LEFT JOIN jmt.vessel_bookings pol_vessel_bookings ON pol_vessel_bookings.id = (( SELECT max(vessel_bookings_1.id) AS max FROM jmt.vessel_bookings vessel_bookings_1 WHERE vessel_bookings_1.pol_voyage_port_id = load_instructions.pol_voyage_port_id ))
left join jmt.parties_roles exporter_party_roles ON exporter_party_roles.id = COALESCE(vessel_bookings.exporter_party_role_id, pol_vessel_bookings.exporter_party_role_id)
left join jmt.roles exporter_roles on exporter_roles.id = exporter_party_roles.role_id
left join jmt.organizations ON organizations.party_id = exporter_party_roles.party_id
left join jmt.parties exporter_parties on exporter_parties.id = exporter_party_roles.party_id
left join jmt.party_types exporter_party_types on exporter_party_types.id = exporter_parties.party_type_id
--DIM: FORWARDING_AGENT
left join jmt.parties_roles forward_agent_party_roles ON forward_agent_party_roles.id = COALESCE(vessel_bookings.forw_agent_party_role_id, pol_vessel_bookings.forw_agent_party_role_id)
left join jmt.roles forward_agent_roles on forward_agent_roles.id = forward_agent_party_roles.role_id
left join jmt.parties forward_agent_parties on forward_agent_parties.id = forward_agent_party_roles.party_id
left join jmt.party_types forward_agent_party_types on forward_agent_party_types.id = forward_agent_parties.party_type_id
--DIM: SEASON
left join jmt.seasons seasons on seasons.season_code = pallet_sequences.season_code
--DIM: CITIES
left join jmt.cities ON cities.city_code::text = pallets.final_destination::text
where
pallet_sequences.carton_quantity > 0
MULTILINE_SQL

my $publish_sql = <<MULTILINE_SQL;
SELECT
    f_pallet_sequence_movement_as.id,
    coalesce(qry.d_pallet_sequence.pallet_sequence_sk, -1) as pallet_sequence_sk,
    coalesce(qry.d_farm.farm_sk, -1) as farm_sk,
    coalesce(qry.d_fg_product.fg_product_sk, -1) as fg_product_sk,
    coalesce(marketing.marketing_target_market_sk, -1) as marketing_target_market_sk,
    coalesce(shipping.shipping_target_market_sk, -1) as shipping_target_market_sk,
    coalesce(packed.packed_target_market_sk, -1) as packed_target_market_sk,
    coalesce(exporter.exporter_party_sk, -1) as exporter_party_sk,
    coalesce(organization.organization_sk, -1) as organization_sk,
    coalesce(forwarding_agent.forwarding_agent_party_sk, -1) as forwarding_agent_party_sk,
    coalesce(final_receiver.final_receiver_party_sk, -1) as final_receiver_party_sk,
    coalesce(shipping_line.shipping_line_party_sk, -1) as shipping_line_party_sk,
    coalesce(customer.customer_sk, -1) as customer_sk,
    coalesce(vessel.vessel_sk, -1) as vessel_sk,
    coalesce(location.location_sk, -1) as location_sk,
    coalesce(container.container_sk, -1) as container_sk,
    coalesce(discharge.port_sk, -1) as port_of_discharge_sk,
    coalesce(loading.port_sk, -1) as port_of_loading_sk,
    coalesce(fin_supplier.fin_supplier_sk, -1) as fin_supplier_sk,
    coalesce(season.season_sk, -1) as season_sk,
    coalesce(city.city_sk, -1) as city_sk,
    coalesce(seal_point_location.location_sk, -1) as seal_point_location_sk,
    coalesce(voyage.voyage_sk, -1) as voyage_sk,
    coalesce(shipping_doc.shipping_doc_sk, -1) as shipping_doc_sk,
    coalesce(stock_item.stock_item_sk, -1) as stock_item_sk,
    mk_date_sk(packed_date::date) as packed_date_sk,
    mk_date_sk(inspection_date::date) as inspection_date_sk,
    mk_date_sk(intake_date::date) as intake_date_sk,
    mk_date_sk(cold_date::date) as cold_date_sk,
    mk_date_sk(exit_date::date) as exit_date_sk,
    mk_date_sk(reinspection_date::date) as reinspection_date_sk,
    mk_date_sk(load_out_date::date) as load_out_date_sk,
    mk_date_sk(applic_shipment_date::date) as applic_shipment_date_sk,
    mk_date_sk(actual_voyage_departure_date::date) as actual_voyage_departure_date_sk,
    mk_date_sk(estimate_voyage_departure_date::date) as estimate_voyage_departure_date_sk,
    mk_date_sk(applic_voyage_departure_date::date) as applic_voyage_departure_date_sk,
    mk_date_sk(actual_voyage_arrival_date::date) as actual_voyage_arrival_date_sk,
    mk_date_sk(estimate_voyage_arrival_date::date) as estimate_voyage_arrival_date_sk,
    mk_date_sk(applic_voyage_arrival_date::date) as applic_voyage_arrival_date_sk,
    mk_date_sk(f_pallet_sequence_movement_as.gate_out_date::date) as gate_out_date_sk,
    mk_date_sk(f_pallet_sequence_movement_as.original_intake_date::date) as original_intake_date_sk,
    mk_date_sk(f_pallet_sequence_movement_as.original_inspection_date::date) as original_inspection_date_sk,
    mk_date_sk(f_pallet_sequence_movement_as.reinspected_for_ambient_date::date) as reinspected_for_ambient_date_sk,
    f_pallet_sequence_movement_as.voyage_year,
    f_pallet_sequence_movement_as.stock_age,
    f_pallet_sequence_movement_as.max_age_on_arrival,
    f_pallet_sequence_movement_as.original_ambient_age,
    f_pallet_sequence_movement_as.original_inspection_age,
    f_pallet_sequence_movement_as.actual_voyage_arrival_age,
    f_pallet_sequence_movement_as.liquid_age,
    f_pallet_sequence_movement_as.pod_age,
    f_pallet_sequence_movement_as.voyage_code,
    f_pallet_sequence_movement_as.pallet_size,
    f_pallet_sequence_movement_as.nett_weight_per_carton,
    f_pallet_sequence_movement_as.gross_weight_per_carton,
    f_pallet_sequence_movement_as.gross_mass,
    f_pallet_sequence_movement_as.orig_std_ctn_qty,
    f_pallet_sequence_movement_as.std_ctn_qty,
    f_pallet_sequence_movement_as.act_ctn_qty,
    f_pallet_sequence_movement_as.supply_chain_status,
    f_pallet_sequence_movement_as.shipped_age,
    f_pallet_sequence_movement_as.storage_age,
    f_pallet_sequence_movement_as.intake_age,
    f_pallet_sequence_movement_as.original_intake_age,
    f_pallet_sequence_movement_as.storage_rate_type,
    f_pallet_sequence_movement_as.storage_amt_excl,
    f_pallet_sequence_movement_as.storage_vat,
    f_pallet_sequence_movement_as.storage_amt_incl,
    f_pallet_sequence_movement_as.valid,
    f_pallet_sequence_movement_as.archived,
    f_pallet_sequence_movement_as.ship_number,
    now() as load_date
from stg.f_pallet_sequence_movement_as
left join qry.d_pallet_sequence on _pallet_number = d_pallet_sequence.pallet_number
    and _pallet_sequence_number = d_pallet_sequence.pallet_sequence_number
left join qry.d_farm on _farm_code = qry.d_farm.farm_code
left join qry.d_fg_product on _fg_product_code = qry.d_fg_product.fg_product_code
left join public.d_marketing_target_market marketing on _marketing_country_code = marketing.country_code
left join public.d_shipping_target_market shipping on _shipping_country_code = shipping.country_code
left join public.d_packed_target_market packed on _packed_country_code = packed.country_code
left join public.d_exporter_party exporter on _exporter_party_type_name = exporter.exporter_party_type_nm
    and _exporter_party_name = exporter.exporter_party_nm
left join public.d_organization organization on _organization_name = organization.organization_name
left join public.d_forwarding_agent_party forwarding_agent on _forwarding_agent_party_type_name = forwarding_agent.forwarding_agent_party_type_nm
    and _forwarding_agent_party_name = forwarding_agent.forwarding_agent_party_nm
left join public.d_final_receiver_party final_receiver on _final_receiver_party_type_name = final_receiver.final_receiver_party_type_nm
    and _final_receiver_party_name = final_receiver.final_receiver_party_nm
left join public.d_shipping_line_party shipping_line on _shipping_line_party_type_name = shipping_line.shipping_line_party_type_nm
    and _shipping_line_party_name = shipping_line.shipping_line_party_nm
left join public.d_customer customer on _customer_party_type_name = customer.party_type_name
    and _customer_party_name = customer.customer_name
left join qry.d_vessel vessel on _vessel_type_code = vessel.vessel_type_code
    and _vessel_code = vessel.vessel_code
left join qry.d_location location on _location_type_code = location.location_type_code
    and _location_code = location.location_code
left join qry.d_container container on _container_code = container.container_code
left join qry.d_port as discharge on _discharge_port_type = discharge.port_type
    and _discharge_port_code = discharge.port_code
left join qry.d_port loading on _loading_port_type = loading.port_type
    and _loading_port_code = loading.port_code
left join qry.d_fin_supplier fin_supplier on _fin_supplier_party_type_name = fin_supplier.party_type_name
    and _fin_supplier_role_name = fin_supplier.role_name
    and _fin_supplier_party_name = fin_supplier.fin_supplier_name
left join qry.d_season season on _season_type = season.season_type
    and _season_code = season.season_code
left join qry.d_voyage voyage on _voyage_id = voyage.id
left join qry.d_shipping_doc shipping_doc on _shipping_doc_id = shipping_doc.id
left join qry.d_city city on _city_code = city.city_code
left join qry.d_location seal_point_location on _load_instructions_depot_code = seal_point_location.location_code
left join qry.d_stock_item stock_item on _stock_item_inventory_reference = stock_item.inventory_reference
MULTILINE_SQL

K_Log::log("[bfw0004] Building stg.f_pallet_sequence_movement_as");
# Run $stage_sql into stg.f_pallet_sequence_as
Pipeline->new(
    {
        dataset => 'f_pallet_sequence_movement_as',
        action  => 'Stage',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $stage_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'stg',
                tablename => 'f_pallet_sequence_movement_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();

K_Log::log("[bfw0004] Building qry.f_pallet_sequence_as");
# Truncate and copy stg.f_pallet_sequence_as into qry.f_pallet_sequence_as
Pipeline->new(
    {
        dataset => 'f_pallet_sequence_movement_as',
        action  => 'Stage',
        source  => SelectSource->new(
            {
                alias  => 'UNI',
                select => $publish_sql
            }
        ),
        target => TableWriter->new(
            {
                alias     => 'UNI',
                schema    => 'qry',
                tablename => 'f_pallet_sequence_movement_as',
                truncate  => 1
            }
        ),
        datatypeCheck => 0,
    }
)->run();

K_Log::log("[bfw0004] Done");

1;
