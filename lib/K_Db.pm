package K_Db;
use strict;
use Carp;
use DBI qw(:sql_types);
use K_Log;
use FindBin qw($Bin);
use Fatal qw(open close);
use Hash::Util qw(lock_keys unlock_keys);
use K_Cf;

#$SIG{__WARN__} = sub { die $_[0] };

sub connect
{
    my ( $dbn, $dt_fmt ) = @_;
    if ( !exists $K_Cf::CFG{DB}{$dbn}{DSN} )
    {
        croak "Cannot find entry for $dbn in config file";
    }
    my $dbh = DBI->connect(
        $K_Cf::CFG{DB}{$dbn}{DSN},
        $K_Cf::CFG{DB}{$dbn}{USER},
        $K_Cf::CFG{DB}{$dbn}{PWD},
        {
            PrintError       => 1,
            AutoCommit       => 1,
            ChopBlanks       => 1,
            #RaiseError       => 1,
        }
        )
        or croak "$DBI::errstr\n";

    # $dbh->{HandleError} = sub { $_[0] = Carp::longmess( $_[0] ); 0; };
    print "Connected to $dbn...\n" if $K_Cf::CFG{GEN}{DEBUG};
    return $dbh;
}

sub copy {
    my ($dbh,$tab)=@_;
    $dbh->do( qq{copy $tab from '} . $ENV{MIS}
            . qq{/stg/} . $tab . qq{.txt' CSV delimiter E'\t' quote '\x7f'} );
    my $db_cnt=$dbh->selectrow_array(qq{select count(*) from }
        . $tab);
    my $cmd=qq{wc -l } . $ENV{MIS} . qq{/stg/}
        . $tab . q{.txt | awk '{print $1}'};
    my $fl_cnt=qx{$cmd};
    die "Error:$tab rows=$db_cnt - $ENV{MIS}/stg/$tab.txt=$fl_cnt"
        if $db_cnt!=$fl_cnt;
}

sub run
{
    my ( $dbh, $href, $sql ) = @_;

    my $sth = $dbh->prepare(qq{ $sql });

    unlock_keys( %{$href} );
    $sth->execute;
    %{$href} = ();
    $sth->bind_columns( \( @{$href}{ @{ $sth->{NAME} } } ) );
    lock_keys( %{$href} );
    return $sth;
}

sub write_load
{
    my ($args) = @_;

    #--------------------------------------------------
    # Default load file path

    die "Must supply a valid K_Rec object to load"
        unless ref( $args->{rec} ) eq 'K_Rec';
    my $ldpath  = "$Bin/../stg/" . $args->{table};
    my $logpath =  $args->{logpath} || "$Bin/../log/"  . $args->{table};
    my $sqlfile = $args->{script} || $ldpath . ".sql";
    my ( $outstr, $from_file );

    if ( $args->{truncate} )
    {
        $outstr .= qq{TRUNCATE TABLE } . $args->{table} . ";\nCOMMIT;\n";
    }

    #-----------------------------------------------
    # If there are several load files, eg when
    # loading after threaded job, produce a list
    # with all the file names

    if ( $args->{ld_num} )
    {
        for my $i ( 1 .. $args->{ld_num} )
        {
            $from_file .= $ldpath . "${i}.txt";
            if ( $i < $args->{ld_num} )
            {
                $from_file .= "','";
            }
        }
    }
    else
    {
        $from_file = $args->{ldfile} || $ldpath . ".txt";
    }

    $outstr .= $args->{table} . "(\n";

    #--------------------------------------------------
    # Column specs from the rec object passed as argument

    $outstr .= $args->{rec}{ldsql} . "FILLER(1))\n";

    $outstr .= qq{ FROM '} . $from_file . qq{'\n QUOTES OFF ESCAPES OFF  \n}
        . (
        ( $args->{rec}{var} && !$args->{rec}{missing_eol_delimiter} )
        ? " DELIMITED BY '"
            . $args->{rec}{sep} . "'"
            . " ROW DELIMITED BY '\\x0a' \n"
        : " "
        )
        . ( $args->{rec}{header} ? "\nSKIP 1\n" : "" )
        . (
        $args->{ignore}
        ? "IGNORE CONSTRAINT NULL 10, DATA VALUE 10, UNIQUE 10\n"
        : ""
        )
        . qq{MESSAGE LOG '}
        . $logpath
        . ".mess.log'\n "
        . qq{ ROW LOG '}
        . $logpath
        . ".row.log' ONLY LOG ALL ";
    $outstr .= ";\ncommit;\n";

    open my $sql_fh, ">", $sqlfile or croak "Cannot open $sqlfile:$!";
    print $sql_fh $outstr or croak $!;
    close $sql_fh;

    #------------------------------------------------------------
    # If the table to be loaded was specified
    # Write out a load file for this table

    if (( $K_Cf::CFG{GEN}{ENV} eq 'dev' && $args->{run} )
    || $args->{force})
    {
        dbexec( { file => $sqlfile } );
    }
    else
    {
        print
            qq{Not running the load because no 'run' parameter specified or \
			config 'ENV' not set to 'dev'\n} if $K_Cf::CFG{GEN}{DEBUG};
    }
    return;
}

sub dbexec
{
    my $args = shift;
    my ( $sql, $fh, $sqlfile, $tmp );

    my $dbh = K_Db::connect('EDW');
    if ( $args->{file} )
    {

        open $fh, "<", $args->{file} or die "Cannot open $args->{file}:$!";
        $sql = do { local $/; <$fh> };
        close $fh;

   #        system(qq{$K_Cf::CFG{DBISQL}{EXEC}$args->{file} }) and croak "$!";
    }
    else
    {
        croak "Must provide sql or a filename" unless $args->{sql};

        #        $sqlfile = qq{$Bin/../stg/dbexec$$.sql};
        #        open my $tmp_fh, ">", $sqlfile or croak $!;
        #        print $tmp_fh $args->{sql} or croak $!;
        #        close $tmp_fh;
        #        system(qq{$K_Cf::CFG{DBISQL}{EXEC}$sqlfile }) and croak "$!";
        #        unlink($sqlfile);

        $sql = $args->{sql};
    }

    #----------------------------------------------------------------------
    # Manipulate the batch SQL received - possibly containing ; and commit
    # statements -
    # to run it one statement at a time
    #
    $sql =~ s/commit;//imsg;    # Get rid of commits
    my @sqls = $sql =~ m{([^;]+);*}msg;    # Separate into statements
    for my $sql_st (@sqls)
    {                                      # For each statement
        next if $sql_st =~ m/\A\s*\z/ms;    # Next if blank
        $sql_st =~ s/\A\s*//gms;            # Get rid if leading whitespace,
                                            # possibly containing \n
        my ($tmp_str) = $sql_st =~ m{\A(.*?)$}ms;    # For display
        K_Log::msg("dbexec:$tmp_str ...");
        $dbh->do($sql_st);
    }
    $dbh->disconnect;
}
1;
__END__

=head1 NAME

K_Db -- subroutines for database access

=head1 SYNOPSIS

	use K_Db;

	my $dbh  = K_Db::connect("EDW");
	my %row;
    my $sth = K_Db::run( $dbh, \%row, qq{SELECT getdate() get_dt } );

Fetch the results. Columns of each row in the result set are in C<%row> hash.

	while ( $sth->fetch() ) {
		my $dt=$row{get_dt};
		# Do something with the data
	}

	$sth->finish;
	$dbh->disconnect;

=head1 Subroutines

=head2 connect

Creates a database handle that can be used to run SQL
against a database and return results.

	my $dbh = K_Db::connect( "DB", "YMD3_YYYY" );

"DB" must be the block heading of one of the databases in C<$EDW/edw.ini>.

Use the optional C<YMD3_YYYY> argument if dates are required by default in
the "CCYYMMDD" format.

=head2 run

Uses a hash reference in the calling script to 'bind' columns in the
result set.

=head2 write_load

	use K_Db;

	K_Db::write_load(
		{
			table    => "SOME_TABLE",
			ldfile  => "/path/to/some/file",
			truncate => 1,
            run      => 1,
			rec    => $rec    # object from K_Rec
		}
	);

Parameters:

=over

=item table

Table that will be loaded.

=item ldfile

If this parameter is not specified, default load file is determined by:

=over

=item * Load file location: in ../stg directory adjacent to the script directory

=item * Load file name: same as table name with .txt extension

=back

The load script will have the same name and location but C<.sql> extension.

Example: script in $EDW/subsys/bin, table to be loaded: SOME.TABLE, then the default load file
will be $EDW/subsys/stg/SOME.TABLE.txt, and load script $EDW/subsys/stg/SOME.TABLE.sql

=item truncate

1 specifies that the table is to be truncated  before loading. If the argument
is not specified the load will append.

=item run

1 specifies that the LOAD TABLE .sql file is to be run ie
the table actually is loaded. Otherwise the load is left for another job
to run (mostly in production). This is also affected by the ENV= setting
in C<edw.conf>. Only if that is also set to 'dev' will the load be run.

=item rec

An object in the C<K_Rec> class. This allows scripts to manipulate
the load script before calling C<write_load>.

=back

=head2 dbexec

Executes sql as a script, for LOAD TABLE, batch extracts

=cut
