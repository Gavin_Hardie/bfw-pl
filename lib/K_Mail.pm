package K_Mail;
use strict;
use warnings;
use K_Cf;
use Carp;
use Mail::Sender;

sub send
{
    my $args = shift;
    my $sender = new Mail::Sender {
        smtp => $K_Cf::CFG{MAIL}{GEN}{SMTP},
        from => $args->{from},
        on_errors=>'die'
    };
    die "Could not create a mail sender"
        unless $sender;
    print "Sending email to $args->{to}\n";
    if ( $args->{file} )
    {
        $sender->MailFile(
                          {
                to      => $args->{to},
                cc      => $args->{cc} || '',
                subject => $args->{subject},
                msg     => $args->{msg},
                file    => $args->{file}
            }
        );
    }
    else
    {
        $sender->MailMsg(
                         {
                to  => $args->{to},
                cc  => $args->{cc} || '',
                subject => $args->{subject},
                msg => $args->{msg}
            },
        );

    }

}

1;
__END__

=head1 NAME

D_Mail - Module to do mail stuff

=head1 SYNOPSIS

use D_Mail;

...
D_Mail::send({from=>...,
            to=>...,
            subject=>...,});

=cut
