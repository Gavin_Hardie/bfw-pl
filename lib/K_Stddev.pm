package K_Stddev;
use strict;
use warnings;
use FindBin qw($Bin);
use 5.010;
use Pod::Usage;
use Data::Dumper;
use Fcntl;
use POSIX qw/strftime/;

my $rows;
sub average{
    my($data) = @_;
    if (not @$data) {
            die("Empty arrayn");
    }
    my $total = 0;my $i=0;
    foreach (@$data) {
        if (defined $_)
        {
            $total += $_;
        } else
        {
            splice @{$data},$i,1;
        }
        $i++;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev{
    my($data) = @_;
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
        $sqtotal += ($average-$_) ** 2;
    }
    my $std; #say scalar @$data; say $sqtotal;
    if(scalar @$data == 1){
        $std = ($sqtotal / (@$data)) ** 0.5;
    } else {
        $std = ($sqtotal / (@$data-1)) ** 0.5;
    }
    return $std;
}

sub proc_hist {
    my ($hist, $desc, $met_desc) = @_;
    my $avg=sprintf("%.2f",average($hist));
    my $std=sprintf("%.2f",stdev($hist));
    my $upper=sprintf("%.2f",$avg+3*$std);
    my $lower=sprintf("%.2f",$avg-3*$std);
    # say "$desc:mean $avg,mean+3σ:$upper,"
     #  . "mean-3σ:$lower";
    return ($avg,$std,$upper,$lower);
}

1;

__END__

