package K_Googledocs;

# Upload documents to Google Documents.
# 
# Copyright 2010 Alessandro Ghedini <al3xbio@gmail.com>
# --------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# Alessandro Ghedini wrote this file. As long as you retain this 
# notice you can do whatever you want with this stuff. If we 
# meet some day, and you think this stuff is worth it, you can 
# buy me a beer in return.
# --------------------------------------------------------------

use K_Log;
use K_Cf;
use HTTP::Request::Common;
use LWP::UserAgent;
use JSON -support_by_pp;
use Media::Type::Simple;
use File::Basename;
use Data::Dumper;

use strict;
use warnings;

sub upload {
    my ($folder,$file,$gdocs_email,$gdocs_psw)=@_;

    my $upl_fn= (fileparse($file))[0];
    my $ua = LWP::UserAgent -> new;
    my $url = 'https://www.google.com/accounts/ClientLogin';
    my %request;

    #email and password from rpt file overrides config file (mis.conf)
    if (defined($gdocs_email) && defined($gdocs_psw))
	{
	    %request = ('accountType', 'HOSTED_OR_GOOGLE',
	       'Email', $gdocs_email,
	       'Passwd', $gdocs_psw,
	       'service', 'writely',
	       'source', 'GoogleDocsUploader-GoogleDocsUploader-00',
	      );
	} else #use config login details
	{
	    %request = ('accountType', 'HOSTED_OR_GOOGLE',
	       'Email', $K_Cf::CFG{GOOGLEDOCS}{EMAIL},
	       'Passwd', $K_Cf::CFG{GOOGLEDOCS}{PWD},
	       'service', 'writely',
	       'source', 'GoogleDocsUploader-GoogleDocsUploader-00',
	      );
	}

    my $response = $ua -> request(POST $url, [%request]);
    die $response->content if $response->content =~/Error/;
    $response = $response-> as_string;
    my $auth = (split /=/, (split /\n/, (split /\n\n/, $response)[1])[2])[1];

    my $status = (split / /,(split /\n/, $response)[0])[1];
    die("ERROR: Unauthorized.\n") if $status == 403;

    if ($folder) {
        $url = 'https://docs.google.com/feeds/folders/private/full/folder:'
            . $folder . '?alt=json';
    } else {
        $url = "https://docs.google.com/feeds/documents/private/full?alt=json";
    }

    $ua -> default_header('Authorization' => "GoogleLogin auth=$auth");


    if (!open(FILE, $file)) {
        print "ERROR: Unable to open '$file' file.\n";
        next;
    }

    my $data = join("", <FILE>);
    close FILE;

    my $mime = type_from_ext(($file =~ m/([^.]+)$/)[0]);

    $ua -> default_header('Slug' => $upl_fn . (($upl_fn=~/(\.\w+)$/)[0]));

    my $request = HTTP::Request -> new(POST => $url);
    #$request->uri ($request->uri . '?convert=false');
    $request -> content_type($mime);
    $request -> content($data);
    my $response2 = $ua -> request($request) -> as_string;

    $status = (split / /,(split /\n/, $response2)[0])[1];
    my $body = (split /\n\n/, $response2)[1];

    if ($status != 201) {
        print "ERROR: $body";
        next;
    }

    my $json = new JSON;

    my $json_text = $json -> decode($body);

    my $title=$$json_text{entry}{title}{'$t'};
    my $link = $$json_text{entry}{link}[0]{href};

    K_Log::msg( "Document successfully created with title '$title'.\nLink:\n$link\n");

}

1;

__END__

=head1 NAME

GoogleDocsUploader.pl - Uploads documents to Google Documents.

=head1 USAGE

GoogleDocsUploader [OPTIONS]

=head1 OPTIONS

=over
		
=item -e	Specifies the login email (e.g. example@gmail.com).

=item -f	Specifies the file to upload (can be more than one).

=back

=head1 MULTIPLE FILES UPLOAD

You can upload multiple files by setting multiple '-f' options.

=head1 FILE TYPE

Allowed file types (checked with MIME) are:

	CSV	text/csv
	TSV	text/tab-separated-values
	TAB	text/tab-separated-values
	HTML	text/html
	HTM	text/html
	DOC	application/msword
	DOCX	application/vnd.openxmlformats-officedocument.
					wordprocessingml.document
	ODS	application/x-vnd.oasis.opendocument.spreadsheet
	ODT	application/vnd.oasis.opendocument.text
	RTF	application/rtf
	SXW	application/vnd.sun.xml.writer
	TXT	text/plain
	XLS	application/vnd.ms-excel
	XLSX	application/vnd.openxmlformats-officedocument.
						spreadsheetml.sheet
	PDF	application/pdf
	PPT	application/vnd.ms-powerpoint
	PPS	application/vnd.ms-powerpoint

=cut

