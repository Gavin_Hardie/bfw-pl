package K_Rcon;
use strict;
use K_Db;
use K_Cf;
use K_Log;
use DBI;
use Carp;
use Data::Dumper;

our $col_data;

sub cmp_src_tgt
{
    my $args = shift;
    my $t_dbh  = K_Db::connect('UNI');
    my $s_dbh  = K_Db::connect('UNI');
    my %row;
    K_Log::msg("Checking $args->{desc} ...");
    my $src_sth = $s_dbh->prepare( $args->{src_sql} );

    my %d_row;
    my $tgt_sth = $t_dbh->prepare( $args->{tgt_sql} );

    local $" = ";";
    $src_sth->execute;
    $tgt_sth->execute;
    $col_data={names=>$src_sth->{NAME},
        types => $src_sth->{pg_type}};
    my $num_rows    = 0;
    my $prev_row;

    no warnings "uninitialized";
WHILE: while (1)
    {
        my @src = $src_sth->fetchrow_array();
        my @tgt = $tgt_sth->fetchrow_array();
        if ( scalar @src && !scalar @tgt )
        {
            print_err({prev=>$prev_row, src=>\@src,tgt=>\@tgt});
        }
        if ( scalar @tgt && !scalar @src )
        {
            print_err({prev=>$prev_row, src=>\@src,tgt=>\@tgt});
        }
        last WHILE unless @src;
        $num_rows++;
        for my $i ( 0 .. scalar @{$col_data->{names}} - 1 )
        {
            if ( $col_data->{types}->[$i] eq 'varchar'  
            ||  $col_data->{types}->[$i] eq 'date' 
            ||  $col_data->{types}->[$i] eq 'datetime' 
            ||  $col_data->{types}->[$i] eq 'timestamp' )
            {
                if ( $src[$i] ne $tgt[$i] )
                {
                    print_err({prev=>$prev_row, src=>\@src,tgt=>\@tgt});
                }
            }
            elsif ( $src[$i] != $tgt[$i] )
            {
                print_err({prev=>$prev_row, src=>\@src,tgt=>\@tgt});
            }
        }
        $prev_row="";
        $prev_row .=sprintf(
            "%-30.30s%-30.30s%-30.30s\n",
            $col_data->{names}->[$_],
            $src[$_] || " ", $tgt[$_] || " ")
            for 0 .. scalar @{$col_data->{names}} - 1;
    }
    print "$num_rows result rows OK\n";
    return;
}

sub print_err {
    my $args=shift;
    printf( "%-30.30s%-30.30s%-30.30s\n", "COLUMN", "SRC", "TGT");
    printf( "%-30.30s%-30.30s%-30.30s\n", "-" x 30, "-" x 30, "-" x 30);
    print "Previous row:\n";
    print $args->{prev} || "\n\n";
    print "Error row:\n";
    print "---------:\n";
    no warnings "uninitialized";
    for my $i (0 .. scalar @{$col_data->{names}} - 1)
    {
        printf( "%-30.30s%-30.30s%-30.30s\n",
            $col_data->{names}->[$i],  $args->{src}->[$i] , 
             $args->{tgt}->[$i] ) 
    }
    croak;
}


1;
__END__

=head1 DESCRIPTION

=cut
