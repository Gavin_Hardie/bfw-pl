package K_Html;
use Scalar::Util;
use K_Cf;
use File::Basename;
use K_Db;
use Config::General;
use URI::Escape;
use strict;
use warnings;
our ($usr,$pwd);

sub tabout {
	my $args=shift;

    my ($dbh,$DB);

    #--------------------------------------------------
    #   If DB entry in edw.conf was supplied, use that
    #   Otherwise default to using 'HOS' entry in edw.conf
    #   and the username/password used to login to the site
    #
    $dbh = K_Db->connect( 'MIS');
	my $sth=$dbh->prepare($sql) or die "Cannot prepare $sql";
	$sth->execute() or die "Cannot execute $sql:" . $dbh->errstr;

	print q{
	<style>
     .th { font-family:"ARIAL"; font-size: 8pt; font-style:"BOLD";   mso-number-format: "\@" }
	  .tdtext1 { font-size: 8pt;   mso-number-format: "\@" }
	  .tdint1 { font-size: 8pt;   mso-number-format: \#\#\#0 }
	  .tdflt1 { font-size: 8pt;   mso-number-format: \#\,\#\#0\.00 }
	  .tdtext2 { font-size: 8pt;   mso-number-format: "\@" }
	  .tdint2 { font-size: 8pt;   mso-number-format: \#\#\#0 }
	  .tdflt2 { font-size: 8pt;   mso-number-format: \#\,\#\#0\.00 }
	  .xl25 {  WHITE-SPACE: normal; mso-number-format: "mmm\ d,\ yyyy" }
	</style>
	<table class="Grid">
	<tr>
	};
	my ($fh,$CSVFILE,$htmlink);
	for my $i (0..($sth->{NUM_OF_FIELDS}-1)) { 
    	if ($sth->{TYPE}[$i]==1 || $sth->{TYPE}[$i]==9){ 
    	    print qq{<th class="th" FILTER=ALL align=left>} .   $sth->{NAME}->[$i] . qq{</th>};
       	} else {
	    	print qq{<th class="th" FILTER=ALL align=right>} .   $sth->{NAME}->[$i] . qq{</th>};
		}
	}
	print "</tr>";
	while (my @row = $sth->fetchrow_array) {
		print "<tr>";
		if ($fh) { 
		   s/\s+$//g for @row;
		   print $fh join(",",@row),"\n";
		}
        my %col_val;
		for my $k (0..($sth->{NUM_OF_FIELDS}-1)) { 
            $col_val{$sth->{NAME}->[$k]}=$row[$k];
			if ($sth->{TYPE}->[$k]==$D_Cf::CFG{DB}{$DB}{CHAR_TYPE}){ 
				print qq{<td class="tdtext2">} . $row[$k] . qq{</td>};
				next;
			} 
			if ($sth->{TYPE}->[$k]==$D_Cf::CFG{DB}{$DB}{DATE_TYPE}) { 
					$row[$k] =~ s|(.{4})-(.{2})-(.{2}).*|$1/$2/$3|;
					print qq{<td class="tdtext2">} . $row[$k] . qq{</td>};
					next;
			}
		   	if (defined $sth->{SCALE}->[$k]) { 
                print $sth->{SCALE}->[$k]>0 ? qq{<td align=right class="tdflt2">} .  $row[$k] . qq{</td>}
                : qq{<td align=right class="tdint2">} .  $row[$k] . qq{</td>};
                next;
            }
		   	if ($sth->{TYPE}->[$k]==$D_Cf::CFG{DB}{$DB}{INT_TYPE} 
		   	|| $sth->{TYPE}->[$k]==$D_Cf::CFG{DB}{$DB}{SINT_TYPE}) { 
                print qq{<td align=right class="tdint2">} .  $row[$k] . qq{</td>};
                next;
            }
            print qq{<td align=right class="tdint2">} .  $sth->{TYPE}->[$k] . qq{</td>};
            
        }
        if ($args->{link}){ 
            (my $url=$args->{link})=~s/\s*\n\s*//g;
            $url=~s/\[< (.*?) >\]/$col_val{qq{$1}}/gmse;
            $url=~s/ /%%32%30/g; # force uri encoding
            print qq{<td><a href="$url">$args->{link_name}</a></td>};
        }
    }
	print qq{</tr></table>};
    print "<BR>No data found<BR>" unless $sth->rows;
	$sth->finish();
	$dbh->disconnect();
}


    
sub proc_parms {
	my ($insql)=@_;
	my $outsql="";
	#print "buildsql:" . $insql . "\n";
	my @linesql=split(/\n/,$insql);
	LINES:
	for my $line (@linesql) {
		do {$outsql .= $line . "\n";next LINES; } if (! ($line =~ m{^\s*(s\||\|)(.*?)\|(.*?)\|}));
		my ($str,$col,$srch) = ($1,$2,$3); 
		next LINES if (! $srch );
		$outsql .= " ";
		if ($srch =~ m/_Use_All_Values_/) {
			next LINES;
		}
		if ($srch =~ m/^\*/) {
			$srch =~ s/\*/%/g;
			$outsql .= qq{$col like '$srch' } . "\n";
			next LINES;
		}
		if ($srch =~ m/^([<>])(.*)/) {
			my ($sign,$rest)=($1,$2);
			$outsql .= qq{$col } . $sign . " " . (($str =~ m/^s/) ? qq{'$rest'} : $rest) . "\n";
			next LINES;
		}
		if ($srch =~ m/,/) {
			my @tmpvals = split(/,/,$srch);
			my $firstval = shift @tmpvals;
			$outsql .= "$col in (" . (($str =~ m/^s/) ? qq{'$firstval'} : $firstval );
			for my $tmpval (@tmpvals) { 
				$outsql .= "," . (($str =~ m/^s/) ? qq{'$tmpval'} : $tmpval );
			}
			$outsql .= ")\n";
			next LINES;
		}
#		if ($srch =~ m/between/i) {
#			my ($firstv,$secv) = split(/-/,$srch);
#			$outsql .= "$col between " 
#                . (($str =~ m/^s/) ? qq{'$firstv'} : $firstv ) . " and " 
#                . (($str =~ m/^s/) ? qq{'$secv'} : $secv );
#			$outsql .= "\n";
#			next LINES;
#		}
		$outsql .= $col . "= " . (($str =~ m/^s/) ? qq{'$srch'} : $srch ) . "\n";
	}

	return $outsql;
}
1;
