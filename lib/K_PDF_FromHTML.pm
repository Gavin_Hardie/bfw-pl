package K_PDF_FromHTML;
use strict;
use Carp;
use HTML::HTMLDoc;

sub Convert 
{
    my ( $htmlFileName, $pdfFilename ) = @_;
    my ($htmldoc, $pdf);
    
    $htmldoc = HTML::HTMLDoc->new();
    
    $htmldoc->set_page_size('A4');
    $htmldoc->portrait();
    $htmldoc->set_right_margin( 1, 'cm' );
    $htmldoc->set_left_margin( 1, 'cm' );
    $htmldoc->set_top_margin( 1, 'cm' );
    $htmldoc->set_bottom_margin( 1, 'cm' );
    $htmldoc->set_fontsize(10);
    $htmldoc->set_compression(6); # fast/less 1 - 9 slow/more
    
    $htmldoc->set_input_file($htmlFileName);
    
    $pdf = $htmldoc->generate_pdf();
    $pdf->to_file($pdfFilename);
    
    return 1;
}
1;

