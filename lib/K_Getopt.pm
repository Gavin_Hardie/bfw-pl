package K_Getopt;
use warnings;
use strict;
use Carp;
use FindBin qw($RealScript);
use Getopt::Std;

$Getopt::Std::STANDARD_HELP_VERSION=1;
$main::VERSION=" ";
sub main::HELP_MESSAGE {
    print qx/perldoc $RealScript/;
    exit 1;
}
sub get_opts {
    &Getopt::Std::getopts; 
    #Pass on @_ passed to this sub, see perlsub
}
1;

=head1 SYNOPSIS

=head1 DESCRIPTION

Sets up help message and calls Getopt:Std::getops

=cut

