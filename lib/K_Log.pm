package K_Log;
use strict;
use Carp;

# Msg messages always go to stdout
sub msg {
#  my ($msg)=@_;
  print qx/date "+%F %k:%M:%S"/ =~/(.*)\n/, " ", @_,"\n" or croak "$!";
}


# Log messages always go to log file.
# If ENV{DEBUG}, they also go to stdout
sub log {

  my $dir = "$ENV{MIS}/log";

  if (-e $dir and -d $dir) {
      open(my $fh, ">> $dir/logfile.txt");
      print $fh  qx/date "+%F %k:%M:%S"/ =~/(.*)\n/, " ", @_,"\n" or croak "$!";
      close ($fh);
  } else {
      die "Unable to access directory $dir";
  }

  # ENV{DEBUG} causes log messages to go to stdout as well
  if (defined($ENV{DEBUG}) && ($ENV{DEBUG} == 1)) {
    print qx/date "+%F %k:%M:%S"/ =~/(.*)\n/, " ", @_,"\n" or croak "$!";
  }
}

# If ENV{DEBUG}, debug messages go to stdout
sub debug {
  if (defined($ENV{DEBUG}) && ($ENV{DEBUG} == 1)) {
    print qx/date "+%F %k:%M:%S"/ =~/(.*)\n/, " ", @_,"\n" or croak "$!";
  }
}

1;
