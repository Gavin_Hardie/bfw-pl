use utf8;

package Schema::Result::Person;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::Result::Entity

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<entity>

=cut

__PACKAGE__->table("person");

__PACKAGE__->add_columns(
  "per_id",
  {data_type => "integer", is_auto_increment => 1, is_nullable => 0},
  "per_ref",
  {data_type => "text", is_nullable => 1},
  "per_hash",
  {data_type => "text", is_nullable => 1},
  "per_salt",
  {data_type => "text", is_nullable => 1},
  "per_usrtp",
  {data_type => "text", is_nullable => 1},
  "per_frstnm",
  {data_type => "text", is_nullable => 1},
  "per_title",
  {data_type => "text", is_nullable => 1},
  "per_surnm",
  {data_type => "text", is_nullable => 1},
  "per_logins",
  {data_type => "text", is_nullable => 1},
);

__PACKAGE__->set_primary_key("per_id");


1;
__END__
