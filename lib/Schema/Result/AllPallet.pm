use utf8;
package Schema::Result::AllPallet;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Schema::Result::AllPallet

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<all_pallets>

=cut

__PACKAGE__->table("all_pallets");

=head1 ACCESSORS

=head2 original_location

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 original_consignment_note_number

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 original_intake_date

  data_type: 'date'
  is_nullable: 1

=head2 original_inspection_date

  data_type: 'date'
  is_nullable: 1

=head2 original_inspection_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 original_ambient_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 intake_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 original_intake_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 weighed_at

  data_type: 'date'
  is_nullable: 1

=head2 weighing_location

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 pallet_gross_mass

  data_type: 'numeric'
  is_nullable: 1
  size: [11,3]

=head2 fin_season_code

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 fin_season

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 fin_season_group_code

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 packhse_num

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 transfer_po

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 dispatched_at

  data_type: 'date'
  is_nullable: 1

=head2 pallet_number

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 shipped_from_ps

  data_type: 'boolean'
  is_nullable: 1

=head2 container_short_code

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 location_code

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 internal_location_code

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 internal_location_type

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 orgzn

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 commodity

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 variety

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 pckd_count

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 grade_code

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 pack

  data_type: 'char'
  is_nullable: 1
  size: 4

=head2 packed_puc

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 orchard

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 farm_code

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 cold_date

  data_type: 'date'
  is_nullable: 1

=head2 cold_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 max_age_on_arrival

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 pod_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 age_on_arrival_rule

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 inspection_date

  data_type: 'date'
  is_nullable: 1

=head2 inspection_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 ambient_age

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 ambient_age_rule

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 inspection_age_rule

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 day28_rule

  data_type: 'varchar'
  is_nullable: 1
  size: 6

=head2 re_inspection_date_time

  data_type: 'timestamp'
  is_nullable: 1

=head2 re_inspected_for_ambient_date

  data_type: 'date'
  is_nullable: 1

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 dhl_waybill

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 variety_description

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 mark_code

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 status

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 allocated

  data_type: 'boolean'
  is_nullable: 1

=head2 season_code

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 inv_code

  data_type: 'char'
  is_nullable: 1
  size: 5

=head2 seq_ctn_qty

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 exit_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 pod

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 packed_tm_group

  data_type: 'char'
  is_nullable: 1
  size: 5

=head2 ship_number

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 seal_code

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 seal_point

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 container_code

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 temp_tail

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 customer

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 consignee

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 pol

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 cargo_due

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 carrier

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 consignment_code

  data_type: 'varchar'
  is_nullable: 1
  size: 60

=head2 container_number

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 date_mates_sent

  data_type: 'date'
  is_nullable: 1

=head2 destroyed

  data_type: 'boolean'
  is_nullable: 1

=head2 eta

  data_type: 'date'
  is_nullable: 1

=head2 etd

  data_type: 'date'
  is_nullable: 1

=head2 arrival_date

  data_type: 'date'
  is_nullable: 1

=head2 departure_date

  data_type: 'date'
  is_nullable: 1

=head2 fin_tm_group

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 final_destination

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 final_receiver

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 final_set_received_date

  data_type: 'date'
  is_nullable: 1

=head2 forw_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 gross_total

  data_type: 'numeric'
  is_nullable: 1
  size: [17,12]

=head2 gross_weight

  data_type: 'numeric'
  is_nullable: 1
  size: [17,12]

=head2 inspection_point

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 intake_number

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 invoice_target_market

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 load_instruction_code

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 load_out_date

  data_type: 'date'
  is_nullable: 1

=head2 missing_stock_data

  data_type: 'boolean'
  is_nullable: 1

=head2 nett_total

  data_type: 'numeric'
  is_nullable: 1
  size: [17,12]

=head2 nett_weight

  data_type: 'numeric'
  is_nullable: 1
  size: [17,12]

=head2 notify_party

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 order_number

  data_type: 'integer'
  is_nullable: 1

=head2 original_mv_booked

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 packed_farm

  data_type: 'varchar'
  is_nullable: 1
  size: 80

=head2 phc

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 picking_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 position

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 prod_country

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 prod_province

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 prod_region

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 qc_failed_tm

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 rec_country

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 ref_no

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 season_group_code

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 season_year

  data_type: 'char'
  is_nullable: 1
  size: 4

=head2 sequence_nr

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 shipment_type

  data_type: 'char'
  is_nullable: 1
  size: 4

=head2 shipped

  data_type: 'boolean'
  is_nullable: 1

=head2 shipping_line_invoice_captured_date

  data_type: 'date'
  is_nullable: 1

=head2 shipping_line_invoice_no

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 shipping_tm_group

  data_type: 'varchar'
  is_nullable: 1
  size: 5

=head2 supplier

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 supplier_group

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 supplier_super_group

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 supplier_type

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 trans_shipment_vessel

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 transit

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 ucr_number

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 voyage_code

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 voyage_id

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 intake_date

  data_type: 'date'
  is_nullable: 1

=head2 shipline

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 arrival_week

  data_type: 'numeric'
  is_nullable: 1
  size: [2,0]

=head2 shipping_week

  data_type: 'numeric'
  is_nullable: 1
  size: [2,0]

=head2 created_by

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 affected_by_program

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 updated_by

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 updated_at

  data_type: 'timestamp'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp'
  is_nullable: 1

=head2 preliminary_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 invoice_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 customer_account_sale_id

  data_type: 'varchar'
  is_nullable: 1
  size: 10

=head2 credit_note_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 debit_note_ref

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 inco_term

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 deal_type

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 currency_code

  data_type: 'char'
  is_nullable: 1
  size: 3

=head2 estimated_exchange_rate

  data_type: 'numeric'
  is_nullable: 1
  size: [6,4]

=head2 exchange_rate

  data_type: 'numeric'
  is_nullable: 1
  size: [6,4]

=head2 invoice_price_per_unit

  data_type: 'numeric'
  is_nullable: 1
  size: [12,7]

=head2 revenue_per_unit

  data_type: 'numeric'
  is_nullable: 1
  size: [12,7]

=head2 foreign_cost_per_unit

  data_type: 'numeric'
  is_nullable: 1
  size: [12,7]

=head2 profit_loss_per_unit

  data_type: 'numeric'
  is_nullable: 1
  size: [12,7]

=head2 preliminary_approved

  data_type: 'boolean'
  is_nullable: 1

=head2 invoice_approved

  data_type: 'boolean'
  is_nullable: 1

=head2 customer_account_sale_approved

  data_type: 'boolean'
  is_nullable: 1

=head2 customer_account_sale_final_status

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 pool_name

  data_type: 'varchar'
  is_nullable: 1
  size: 25

=head2 packing_week

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 packed_date_time

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 qc_status

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 vessel_code

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 variety_group

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 marketing_tm_group

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=head2 pallet_size

  data_type: 'numeric'
  is_nullable: 1
  size: [25,20]

=head2 fruit_group_code

  data_type: 'varchar'
  is_nullable: 1
  size: 30

=head2 standard_cartons

  data_type: 'numeric'
  is_nullable: 1
  size: [5,0]

=head2 average_nett_weight

  data_type: 'numeric'
  is_nullable: 1
  size: [5,2]

=head2 supply_chain_status

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 commodity_group_code

  data_type: 'varchar'
  is_nullable: 1
  size: 15

=head2 exit_date

  data_type: 'timestamp'
  is_nullable: 1

=head2 forward_agent_name

  data_type: 'varchar'
  is_nullable: 1
  size: 35

=head2 gate_out_date

  data_type: 'date'
  is_nullable: 1

=head2 refresh_date

  data_type: 'timestamp'
  is_nullable: 1
  size: 0

=head2 stock_age

  data_type: 'interval'
  is_nullable: 1

=head2 shipped_age

  data_type: 'interval'
  is_nullable: 1

=head2 container_qty

  data_type: 'numeric'
  is_nullable: 1
  size: [11,0]

=head2 arrival_age

  data_type: 'interval'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "original_location",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "original_consignment_note_number",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "original_intake_date",
  { data_type => "date", is_nullable => 1 },
  "original_inspection_date",
  { data_type => "date", is_nullable => 1 },
  "original_inspection_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "original_ambient_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "intake_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "original_intake_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "weighed_at",
  { data_type => "date", is_nullable => 1 },
  "weighing_location",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "pallet_gross_mass",
  { data_type => "numeric", is_nullable => 1, size => [11, 3] },
  "fin_season_code",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "fin_season",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "fin_season_group_code",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "packhse_num",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "transfer_po",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "dispatched_at",
  { data_type => "date", is_nullable => 1 },
  "pallet_number",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "shipped_from_ps",
  { data_type => "boolean", is_nullable => 1 },
  "container_short_code",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "location_code",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "internal_location_code",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "internal_location_type",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "orgzn",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "commodity",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "variety",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "pckd_count",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "grade_code",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "pack",
  { data_type => "char", is_nullable => 1, size => 4 },
  "packed_puc",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "orchard",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "farm_code",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "cold_date",
  { data_type => "date", is_nullable => 1 },
  "cold_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "max_age_on_arrival",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "pod_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "age_on_arrival_rule",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "inspection_date",
  { data_type => "date", is_nullable => 1 },
  "inspection_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "ambient_age",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "ambient_age_rule",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "inspection_age_rule",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "day28_rule",
  { data_type => "varchar", is_nullable => 1, size => 6 },
  "re_inspection_date_time",
  { data_type => "timestamp", is_nullable => 1 },
  "re_inspected_for_ambient_date",
  { data_type => "date", is_nullable => 1 },
  "id",
  { data_type => "integer", is_nullable => 1 },
  "dhl_waybill",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "variety_description",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "mark_code",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "status",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "allocated",
  { data_type => "boolean", is_nullable => 1 },
  "season_code",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "inv_code",
  { data_type => "char", is_nullable => 1, size => 5 },
  "seq_ctn_qty",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "exit_ref",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "pod",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "packed_tm_group",
  { data_type => "char", is_nullable => 1, size => 5 },
  "ship_number",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "seal_code",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "seal_point",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "container_code",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "temp_tail",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "customer",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "consignee",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "pol",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "cargo_due",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "carrier",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "consignment_code",
  { data_type => "varchar", is_nullable => 1, size => 60 },
  "container_number",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "date_mates_sent",
  { data_type => "date", is_nullable => 1 },
  "destroyed",
  { data_type => "boolean", is_nullable => 1 },
  "eta",
  { data_type => "date", is_nullable => 1 },
  "etd",
  { data_type => "date", is_nullable => 1 },
  "arrival_date",
  { data_type => "date", is_nullable => 1 },
  "departure_date",
  { data_type => "date", is_nullable => 1 },
  "fin_tm_group",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "final_destination",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "final_receiver",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "final_set_received_date",
  { data_type => "date", is_nullable => 1 },
  "forw_ref",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "gross_total",
  { data_type => "numeric", is_nullable => 1, size => [17, 12] },
  "gross_weight",
  { data_type => "numeric", is_nullable => 1, size => [17, 12] },
  "inspection_point",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "intake_number",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "invoice_target_market",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "load_instruction_code",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "load_out_date",
  { data_type => "date", is_nullable => 1 },
  "missing_stock_data",
  { data_type => "boolean", is_nullable => 1 },
  "nett_total",
  { data_type => "numeric", is_nullable => 1, size => [17, 12] },
  "nett_weight",
  { data_type => "numeric", is_nullable => 1, size => [17, 12] },
  "notify_party",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "order_number",
  { data_type => "integer", is_nullable => 1 },
  "original_mv_booked",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "packed_farm",
  { data_type => "varchar", is_nullable => 1, size => 80 },
  "phc",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "picking_ref",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "position",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "prod_country",
  { data_type => "char", is_nullable => 1, size => 2 },
  "prod_province",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "prod_region",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "qc_failed_tm",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "rec_country",
  { data_type => "char", is_nullable => 1, size => 2 },
  "ref_no",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "season_group_code",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "season_year",
  { data_type => "char", is_nullable => 1, size => 4 },
  "sequence_nr",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "shipment_type",
  { data_type => "char", is_nullable => 1, size => 4 },
  "shipped",
  { data_type => "boolean", is_nullable => 1 },
  "shipping_line_invoice_captured_date",
  { data_type => "date", is_nullable => 1 },
  "shipping_line_invoice_no",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "shipping_tm_group",
  { data_type => "varchar", is_nullable => 1, size => 5 },
  "supplier",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "supplier_group",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "supplier_super_group",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "supplier_type",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "trans_shipment_vessel",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "transit",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "ucr_number",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "voyage_code",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "voyage_id",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "intake_date",
  { data_type => "date", is_nullable => 1 },
  "shipline",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "arrival_week",
  { data_type => "numeric", is_nullable => 1, size => [2, 0] },
  "shipping_week",
  { data_type => "numeric", is_nullable => 1, size => [2, 0] },
  "created_by",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "affected_by_program",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "updated_by",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "updated_at",
  { data_type => "timestamp", is_nullable => 1 },
  "created_at",
  { data_type => "timestamp", is_nullable => 1 },
  "preliminary_ref",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "invoice_ref",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "customer_account_sale_id",
  { data_type => "varchar", is_nullable => 1, size => 10 },
  "credit_note_ref",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "debit_note_ref",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "inco_term",
  { data_type => "char", is_nullable => 1, size => 3 },
  "deal_type",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "currency_code",
  { data_type => "char", is_nullable => 1, size => 3 },
  "estimated_exchange_rate",
  { data_type => "numeric", is_nullable => 1, size => [6, 4] },
  "exchange_rate",
  { data_type => "numeric", is_nullable => 1, size => [6, 4] },
  "invoice_price_per_unit",
  { data_type => "numeric", is_nullable => 1, size => [12, 7] },
  "revenue_per_unit",
  { data_type => "numeric", is_nullable => 1, size => [12, 7] },
  "foreign_cost_per_unit",
  { data_type => "numeric", is_nullable => 1, size => [12, 7] },
  "profit_loss_per_unit",
  { data_type => "numeric", is_nullable => 1, size => [12, 7] },
  "preliminary_approved",
  { data_type => "boolean", is_nullable => 1 },
  "invoice_approved",
  { data_type => "boolean", is_nullable => 1 },
  "customer_account_sale_approved",
  { data_type => "boolean", is_nullable => 1 },
  "customer_account_sale_final_status",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "pool_name",
  { data_type => "varchar", is_nullable => 1, size => 25 },
  "packing_week",
  { data_type => "char", is_nullable => 1, size => 2 },
  "packed_date_time",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "qc_status",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "vessel_code",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "variety_group",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "marketing_tm_group",
  { data_type => "varchar", is_nullable => 1, size => 20 },
  "pallet_size",
  { data_type => "numeric", is_nullable => 1, size => [25, 20] },
  "fruit_group_code",
  { data_type => "varchar", is_nullable => 1, size => 30 },
  "standard_cartons",
  { data_type => "numeric", is_nullable => 1, size => [5, 0] },
  "average_nett_weight",
  { data_type => "numeric", is_nullable => 1, size => [5, 2] },
  "supply_chain_status",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "commodity_group_code",
  { data_type => "varchar", is_nullable => 1, size => 15 },
  "exit_date",
  { data_type => "timestamp", is_nullable => 1 },
  "forward_agent_name",
  { data_type => "varchar", is_nullable => 1, size => 35 },
  "gate_out_date",
  { data_type => "date", is_nullable => 1 },
  "refresh_date",
  { data_type => "timestamp", is_nullable => 1, size => 0 },
  "stock_age",
  { data_type => "interval", is_nullable => 1 },
  "shipped_age",
  { data_type => "interval", is_nullable => 1 },
  "container_qty",
  { data_type => "numeric", is_nullable => 1, size => [11, 0] },
  "arrival_age",
  { data_type => "interval", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-03-09 07:47:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hY7sZn1seNkpcb5T8MtiHw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
