package K_Spreadsheet;
use strict;
use warnings;
use Carp;
use K_Cf;
use POSIX qw(strftime);
use Excel::Writer::XLSX;
use Spreadsheet::WriteExcel;
use FindBin qw($Bin $RealScript);

our ( $sqlheadformat, %bufont, %bfont, %font, $qtyformat, $headformat, 
    $linesformat, $amtformat, $bcformat);
our $LIMIT=65536;

sub heading { 
    return $headformat;
}
sub lines { 
    return $linesformat;
}
sub setup { 
    my $args = shift;
    my $wkbk = $args->{xlsx} ? 
        Excel::Writer::XLSX->new("$args->{filename}")
        : Spreadsheet::WriteExcel->new("$args->{filename}");
    if ($args->{xlsx})
    {
        $wkbk->set_optimization();
    }
    #------------------------
    # Set formats

    %bufont = (
        font      => 'Arial',
        size      => 8,
        color     => 'black',
        bold      => 1,
        underline => 1,
    );
    %bfont = (
        font      => 'Arial',
        size      => 8,
        color     => 'black',
        bold      => 1,
        border    => 1
    );
    %font = ( font => 'Arial', size => 8, color => 'black', bold => 0, border=>1);

    $headformat  = $wkbk->add_format(%bufont);
    $sqlheadformat  = $wkbk->add_format(%bfont);
    $linesformat = $wkbk->add_format(%font);

    $qtyformat = $wkbk->add_format(%font);    # Add a format
    $qtyformat->set_num_format('#,###,###,##0');
    $qtyformat->set_align('right');

    $amtformat = $wkbk->add_format(%font);    # Add a format
    $amtformat->set_num_format('#,###,###,##0.00');
    $amtformat->set_align('right');

    $bcformat = $wkbk->add_format(%font);     # Add a format
    $bcformat->set_num_format('###############0');
    $bcformat->set_align('right');
    return ($wkbk,$wkbk->add_worksheet($args->{worksheet}));
}

sub close {
    my $args = shift;
    $args->{wkbk} || croak "Must specify a workbook to close";
    my $v_ws=$args->{wkbk}->add_worksheet('version');
    my $vers=qx/svn info $Bin\/$RealScript | grep -P "URL|Revision"/;
    croak 'Cannot get svn info' unless $vers;
    $vers=~s/\n/,/gm;
    $vers=~s/URL: //;
    $v_ws->write(0, 0,"Version");
    $v_ws->write(0, 1,$vers);
    $v_ws->write(1, 0,"Rundate");
    $v_ws->write(1, 1,strftime "%a %b %e %H:%M:%S %Y", localtime);
    if ($args->{parms}) { 
        $v_ws->write(2,0,"Parameters");
        $v_ws->write(2,1,$args->{parms});
    }
}

sub run_sql { 
    my ($dbh,$sql,$wksht,$firstrow,$col)=@_;
    my $sth=$dbh->prepare($sql);
    $sth->execute;
    $firstrow||=0;
    $wksht->write_row($firstrow++,$col || 0,$sth->{NAME},$sqlheadformat);
    DBROW: while ( my @row=$sth->fetchrow_array() )
    {
        $wksht->write_row( $firstrow++, $col || 0, \@row, $linesformat);
    }
    return $firstrow;
}

sub write_xls
{
    my $args = shift;
    my ($wkbk,$wksht) = K_Spreadsheet::setup($args);

    my $dbh;

    #----------------------------------------------------
    # If a dbh was supplied, because #tables being used,
    # use that
    my $DB=$args->{db};
    if ($args->{dbh}){
        $dbh=$args->{dbh};
    }
    else {
        $dbh   = K_Db::connect($args->{db});
    }

    my $sth = $dbh->prepare( $args->{sql} );
    $sth->execute;
    $wksht->write( 0, $_, $sth->{NAME}->[$_], $headformat )
        for 0 .. $sth->{NUM_OF_FIELDS}-1;

    my $i = 1;
    while ( my @a = $sth->fetchrow_array )
    {
        for my $j ( 0 .. $sth->{NUM_OF_FIELDS}-1 )
        {
            if ( $sth->{TYPE}->[$j] == $K_Cf::CFG{DB}{$DB}{SINT_TYPE} 
            ||$sth->{TYPE}->[$j] == $K_Cf::CFG{DB}{$DB}{INT_TYPE}) 
            {
                $wksht->write( $i, $j, $a[$j], $bcformat );
                croak "Excel limit reached" if $i> $LIMIT;
                next;
            }
            if ( $sth->{TYPE}->[$j] == $K_Cf::CFG{DB}{$DB}{DEC_TYPE} )
            {
                $wksht->write( $i, $j, $a[$j], $amtformat );
                croak "Excel limit reached" if $i> $LIMIT;
                next;
            }
            
            if ( $sth->{TYPE}->[$j] == $K_Cf::CFG{DB}{$DB}{DATE_TYPE})
            {
                $a[$j] = substr( ($a[$j] || '          '), 0, 10);
            }
            $wksht->write( $i, $j, $a[$j], $linesformat );
                croak "Excel limit reached" if $i> $LIMIT;
        }
        $i++;
    }
    K_Spreadsheet::close({wkbk=>$wkbk});
}

sub write {
    my $args = shift;
    croak "No worksheet specified" unless $args->{worksheet};
    croak "No row specified" unless defined $args->{row};
    croak "No column specified" unless defined $args->{col} || $args->{row} =~m/^[A-Z]/;
    croak "No value specified" unless defined $args->{val};
    croak "Excel limit reached" if $args->{row}>$LIMIT;
    if ($args->{format}) {
        $args->{worksheet}->write($args->{row},$args->{col},
        $args->{val}, $args->{format});
    }
    else {
        $args->{worksheet}->write($args->{row},$args->{col},$args->{val});
    }
}

sub write_row {
    my $args = shift;
    croak "No worksheet specified" unless $args->{worksheet};
    croak "Excel limit reached" if $args->{row}>$LIMIT;
    croak "No row specified" unless defined $args->{row};
    croak "No column specified" unless defined $args->{col} || $args->{row}=~m/^[A-Z]/;
    croak "No value specified" unless defined $args->{val};
    croak "Row is not an array ref" unless ref $args->{val} eq "ARRAY";
    $args->{worksheet}->write($args->{row},$args->{col},$args->{val});
}
        
sub recon_rows {
    my $args=shift;
    my $excel=Spreadsheet::ParseExcel::Workbook->Parse($args->{filename}); 
    my $sheet=${$excel->{Worksheet}}[$args->{sheet_number}];
    $sheet->{MaxRow} ||= $sheet->{MinRow};
    my $xl_tot=0;
    foreach my $tmp_row ( $sheet->{MinRow}+1 .. $sheet->{MaxRow} )
    {
        my $cell = $sheet->{Cells}[$tmp_row][$args->{col_number}];
        $xl_tot+=$cell->{Val};
    }

    $xl_tot=sprintf("%.2f",$xl_tot);
    croak "Excel:$xl_tot, IQ: $args->{iq_tot}" unless $xl_tot == $args->{iq_tot};
    K_Log::msg(qq{Balances: Excel:$xl_tot, IQ:$args->{iq_tot}});
}

1;

=head1 SYNOPSIS

	use K_Csv;

	K_Csv::load_csv({
		truncate=>1,
		table=>$table,
		sep=>",",
		cr=>1,
		missing_eol_delimiter=>1,
		header=>1,
		ldfile=>"my.csv" ,
	});

=head1 DESCRIPTION

Routines for writing to Excel.

=cut

