package Util;

use v5.10;
use strict;
use warnings;
use Data::Dumper;
use YAML::XS qw(LoadFile);

my $working_dir;        # Root direcotry
my $database;
my $schema;
my $table;

my %schema_map       = ();
my @source_databases = ();

###
# Get the task description, passing in a map of parameter
#   working_dir => root directory for all yaml scripts
#   schema => schema alias
#   table => table
###
sub get_task {
    my $args = shift;

    $working_dir = $args->{working_dir};
    my $yaml = load("$working_dir/database.yml");

    # Build schema lookup map schema_alias -> { schema, database }
    foreach my $db ( keys %{ $yaml->{databases} } ) {
        foreach my $sc ( keys %{ $yaml->{databases}->{$db}->{schemas} } ) {
            my $alias = $yaml->{databases}->{$db}->{schemas}->{$sc}->{alias};
            $schema_map{$alias} = {
                database => $db,
                schema   => $sc
            };
        }
    }

    # Real schema name and database name derived from schema alias
    $schema   = $schema_map{ $args->{schema} }->{schema};
    $database = $schema_map{ $args->{schema} }->{database};
    $table    = $args->{table};
    die $args->{schema} . " is not a valid schema alias"
      unless defined($schema)
      && defined($database);


    # Get the table reference and the table_yaml
    my $table_ref =
      $yaml->{databases}->{$database}->{schemas}->{$schema}->{tables}->{$table};
    die "$database.$schema.$table did not lead to a table ref" unless defined($table_ref);
    die "Database $database, Schema $schema does not have a "
      . "table reference for table $table"
      unless defined($table_ref);
    my $table_yaml = resolve_reference($table_ref);
    die "Table reference [$table_ref] did not point to valid yaml"
      unless defined($table_yaml->{name})
      && defined($table_yaml->{type})
      && defined($table_yaml->{load}->{query});

    # Build the SQL statement and confirm that sources are all on one database
    my $table_sql = build_sql( $table_yaml->{load}->{query} );
    if ( scalar(@source_databases) > 1 ) {
        print Dumper \@source_databases;
        die "Multiple source databases";
    }

    my %result = (
        target_database => $database,
        target_table    => $table,
        target_schema   => $schema,
        source_database => $source_databases[0],
        sql             => $table_sql,
        type            => $table_yaml->{type},
    );

    # PK columns
    my $table_pk = columns_of_pk( $table_yaml->{indexes} );
    if ( defined($table_pk) ) {
        $result{target_pk_columns} = $table_pk;
    }

    # SK column
    if (defined($table_yaml->{load}->{sk_column})) {
        $result{sk_column} = $table_yaml->{load}->{sk_column}
    }

    # Merge columns
    if ( defined( $table_yaml->{load}->{merge_index} ) ) {
        my $merge_index = $table_yaml->{load}->{merge_index};
        my $merge_cols  = columns_of( $table_yaml->{indexes}->{$merge_index} );
        if ( scalar @{$merge_cols} > 0 ) {
            $result{merge_columns} = $merge_cols;
        }
    }

    # Post SQL
    my @post_sql = ();
    if (defined($table_yaml->{load}->{post_sql})) {
        push @post_sql, $table_yaml->{load}->{post_sql};
    }
    $result{post_sql} = \@post_sql;

    return %result;
}

sub resolve_reference {
    my $reference = shift;

    my ( $file, $path ) = $reference =~ m/\s*(.*?)\s*\$\$\s*(.*?)\s*$/;
    my @path_bits = split( '\.', $path );

    my $yaml = load("$working_dir/$file");
    foreach my $bit (@path_bits) {
        $yaml = $yaml->{$bit};
    }
    return $yaml;
}

sub columns_of_pk {
    my $indexes = shift;

    my @pk = grep { defined( $_->{type} ) and ( $_->{type} eq 'primary' ) }
      ( values %{$indexes} );
    my $pk_count = scalar @pk;
    return undef unless $pk_count == 1;
    my $columns = $pk[0]->{columns};
    my @columns = map { /(\w*)/ } @{$columns};
    return \@columns;
}

sub columns_of {
    my $index_yaml = shift;
    my $columns    = $index_yaml->{columns};
    my @columns    = map { /(\w*)/ } @{$columns};
    return \@columns;
}

sub database_of {
    my $table = shift;
    my ( $sc, $t ) = $table =~ m/([^.]*)\.([^.]*)/;
    my $real_schema   = $schema_map{$sc}{schema};
    my $real_database = $schema_map{$sc}{database};
    return (
        database => $real_database,
        schema   => $real_schema,
        name     => $t
    );
}

sub build_select {
    my $s = shift;
    if ( !ref($s) ) {
        return $s;
    }
    if ( defined( $s->{formula} ) and defined( $s->{alias} ) ) {
        my $formula = $s->{formula};
        $formula =~ s/[\$][\$]//g;
        return $formula . ' as ' . $s->{alias};
    }
    if ( defined( $s->{query} ) and defined( $s->{alias} ) ) {
        return '(' . build_sql( $s->{query} ) . ') as ' . $s->{alias};
    }
    return 'WTF!';
}

sub build_from {
    my $s = shift;
    my $result;
    if ( defined( $s->{table} ) ) {
        my %info = database_of( $s->{table} );
        push @source_databases, $info{database}
          unless $info{database} ~~ @source_databases;
        $result = $info{schema} . '.' . $info{name};
    }
    elsif ( defined( $s->{view} ) ) {
        my %info = database_of( $s->{view} );
        push @source_databases, $info{database}
          unless $info{database} ~~ @source_databases;
        $result = $info{schema} . '.' . $info{name};
    }
    elsif ( defined( $s->{query} ) ) {
        my $s = build_sql( $s->{query} );
        $result = "($s)";
    }
    else {
        print Dumper $s;
        die "Invalid from clause";
    }

    if ( defined( $s->{alias} ) ) {
        $result = $result . ' as ' . $s->{alias};
    }
    return $result;
}

sub build_join {
    my $s = shift;
    my $result;
    if ( defined( $s->{type} ) ) {
        $result = $s->{type} . ' join';
    }
    else {
        $result = 'join ';
    }
    $result = $result . ' ' . build_from($s);
    if ( defined( $s->{on} ) ) {
        $result = $result . ' on ' . $s->{on};
    }
    return $result;
}

###
# Pass in the contents of a query: node from a yaml document.
#   if this is a string, then interpret it as a yaml reference to elsewhere
#   if this is a map, then
#       If it contains a sql: node, then return that
#       If it contains a select: and a from: node, then build sql from the
#           child nodes.
sub build_sql {
    my $query = shift;
    die "Table yaml does not contain a load.query node" unless defined($query);

    if ( !ref($query) ) {
        my $resolved = resolve_reference($query);
        die "$query was not a valid query reference" unless defined($resolved);
        $query = $resolved;
    }

    if ( defined( $query->{sql} ) and defined( $query->{database} ) ) {
        push @source_databases, $query->{database}
          unless $query->{database} ~~ @source_databases;
        return $query->{sql};
    }
    if ( defined( $query->{select} ) and defined( $query->{from} ) ) {
        return
            "SELECT "
          . join( ",\n      ", map { build_select($_) } @{ $query->{select} } )
          . "\nfrom  "
          . join( ",\n      ", map { build_from($_) } @{ $query->{from} } )
          . prepend( "\n",
            join( ",\n      ", map { build_join($_) } @{ $query->{join} } ) )
          . prepend( "\nwhere ",
            join( "\n  and ", map { $_ } @{ $query->{where} } ) )
          . prepend( "\ngroup by ",
            join( ",\n      ", map { $_ } @{ $query->{group} } ) )
          . prepend( "\nhaving ",
            join( ",\n      ", map { $_ } @{ $query->{having} } ) );
    }
    die "Unable to build sql from " . Dumper $query;
}


sub prepend {
    my $prefix = shift;
    my $main   = shift;

    if ( defined($main) && ( length($main) > 0 ) ) {
        return $prefix . $main;
    }
    else {
        return $main;
    }
}

# Load a yaml file
sub load {
    my $filename = shift;
    open my $fh, '<', $filename or die "can't open config file: $!";
    my $yaml = LoadFile($fh);
    return $yaml;
}


1;
