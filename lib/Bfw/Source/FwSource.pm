package FwSource;

use strict;
use Moose;
use Bfw::Layout;

with qw( Source );

has 'filename'   => ( is => 'ro', isa => 'Str' );

sub init {
    my $self = shift;
}

sub pre {
}

sub open {
}

sub get_row {
}

sub close {
}

sub port {
}

1;
