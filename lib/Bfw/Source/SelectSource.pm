package SelectSource;

use strict;
use Moose;
use Bfw::Layout;
use Bfw::Utils::Db;
use Bfw::SqlSource;
use Data::Dumper;

with qw( SqlSource );

has 'select' => ( is => 'rw', isa => 'Str', required => 1 );

sub query {
    my $self = shift;
    return $self->select;
}

sub start {
    my $self = shift;
    my $dbh  = Db::connect( $self->alias );

    $self->source_layout( LayoutFactory::for_select( $dbh, $self->select ) );
    $self->dbh($dbh);
}

sub finish {
    my $self = shift;
    $self->dbh->disconnect;
}

1;
