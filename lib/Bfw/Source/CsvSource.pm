package CsvSource;

use strict;
use Moose;
use Bfw::Layout;

with qw( Source );

has 'filename'   => ( is => 'ro', isa => 'Str' );

sub init {
    my $self = shift;
}

sub start {
    #Open file
    #$self.source_layout( the layout )
}

sub pre {
    # Do nothing
}

sub open {
    # Do nothing
}

sub get_row {
    # Return row as hash
}

sub close {
    # Do nothing
}

sub post {
    # Do nothing
}

sub finish {
    # Close file
}

1;
