package TableSource;

use strict;
use Moose;
use Bfw::Layout;
use Bfw::Utils::Db;
use Bfw::SqlSource;

with qw( SqlSource );

has 'schema'    => ( is => 'ro', isa => 'Str', required => 1 );
has 'tablename' => ( is => 'ro', isa => 'Str', required => 1 );
has 'where'     => ( is => 'ro', isa => 'Str' );

sub start {
    my $self = shift;
    my $dbh  = Db::connect( $self->alias );

    $self->source_layout(
        LayoutFactory::for_table( $dbh, $self->schema, $self->tablename ) );
    $self->dbh($dbh);
}

sub where_clause {
    my $self         = shift;
    my $where_clause = $self->where;
    if (defined($where_clause) && (length $where_clause)) {
        return " where " . $where_clause;
    } else {
        return '';
    }
}

sub query {
    my $self         = shift;
    my $tablename    = $self->schema . '.' . $self->tablename;
    my $where_clause = $self->where_clause;

    my $sql = "select * from " . $tablename . $where_clause;
    return $sql;
}

sub finish {
}

1;
