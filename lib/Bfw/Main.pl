package Main;
use strict;
use Data::Dumper;
use Source::TableSource;
use Source::SelectSource;
use Target::TableWriter;
use Target::T1DimensionWriter;
use PipeManager;
use Pipeline;
use Source::TableSource;
use Target::TableWriter;
use Utils::LayoutFactory;

print "Main starting\n";

my $manager = PipeManager->new( { alias => 'UNI', name => 'Test', run_id=>14 } );

$manager->run_pipe(
    Pipeline->new(
        {   name   => 'test pipeline',
            code   => 'ex0001',
            source => TableSource->new(
                {   alias     => 'UNI',
                    schema    => 'stg',
                    tablename => 'foo'
                }
            ),
            target => TableWriter->new(
                {   alias     => 'UNI',
                    schema    => 'stg',
                    tablename => 'foo',
                    truncate  => 0
                }
            )
        }
    )
);
