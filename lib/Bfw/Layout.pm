package Layout;
use strict;
use Moose;
use Data::Dumper;
use List::Util qw(first);

has 'name'       => ( is => 'ro', isa => 'Str' );
has 'num_fields' => ( is => 'rw', isa => 'Int' );
has 'fields'     => ( is => 'rw', isa => 'ArrayRef[Field]' );

sub forget_field {
    my $self      = shift;
    my $fieldname = shift;

    my @fields = grep { $_->name ne $fieldname } @{ $self->fields };
    my $num_fields = @fields;

    $self->fields( \@fields );
    $self->num_fields($num_fields);
}

sub get_field {
    my $self      = shift;
    my $fieldname = shift;

    my $fields = $self->fields();
    return first { $_->name() eq $fieldname } @{$fields};
}

1;
