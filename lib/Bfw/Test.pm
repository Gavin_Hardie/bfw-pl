package Test;

use strict;
use Data::Dumper;
use Utils::LayoutFactory;

my $dbh = Db::connect('UNI');

my $layout1 = LayoutFactory::for_select($dbh, 'select * from stg.load_control');
my $layout2 = LayoutFactory::for_table($dbh, 'stg', 'load_control');
my @fieldnames = LayoutFactory::intersect($layout1, $layout2);

my @result = LayoutFactory::compare_fields($layout1, $layout2, \@fieldnames);

print Dumper @result;
