package Target;
use strict;
use Moose::Role;

requires 'process_layout';
requires 'start';
requires 'pre';
requires 'post';
requires 'finish';

has 'name' => ( is => 'rw', isa => 'Str' );
has 'row_count' => ( is => 'rw', isa => 'Int', default => 0 );

sub BUILD {
    my $self = shift;
}

1;
