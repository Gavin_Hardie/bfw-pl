package Source;

use strict;
use Moose::Role;

requires 'start';      # Called at startup
requires 'pre';        # Called before data transfer
                       #
                       # Open, Get_Row and Close might all be skipped totally
                       # dending on the chosen pump.
                       #
requires 'open';       # Called with list of columns before iterating
requires 'get_row';    # Called to get a row
requires 'close';      # Called once iteration completed
                       #
requires 'post';       # Called after data transfer
requires 'finish';     # Called at finish

has 'row_count' => ( is => 'rw', isa => 'Int', default => 0 );
has 'source_layout' => ( is => 'rw', isa => 'Layout' );

sub BUILD {
    my $self = shift;
}

1;
