package Pipeline;

use strict;
use Carp;
use Moose;
use MooseX::Privacy;
use Data::Dumper;
use Bfw::Pump::LocalSelectToTable;
use Bfw::Pump::LocalTableToTable;
use Bfw::Pump::LocalTableToT1dim;
use Bfw::Pump::RemotePwdTableToTable;
use Bfw::Pump::RemotePwdSelectToTable;
use Bfw::Pump::RowToRow;

has 'dataset'               => ( is => 'ro', isa  => 'Str',    required => 1 );
has 'action'                => ( is => 'ro', isa  => 'Str',    required => 1 );
has 'source'                => ( is => 'ro', does => 'Source', required => 1 );
has 'target'                => ( is => 'ro', does => 'Target', required => 1 );
has 'extraColumnsAllowed'   => ( is => 'ro', isa  => 'Bool',   default  => 1 );
has 'missingColumnsAllowed' => ( is => 'ro', isa  => 'Bool',   default  => 0 );
has 'datatypeCheck'         => ( is => 'ro', isa  => 'Bool',   default  => 0 );
has 'row_count'             => ( is => 'rw', isa  => 'Int' );

sub run {
    my $self    = shift;
    my $dataset = $self->dataset();
    my $action  = $self->action();

    print "\nPipeline [$dataset] [$action] starting ...\n";
    my $start_time = time();

    $self->source->start();
    $self->target->start();

    $self->check_layouts();

    $self->source->pre();
    $self->target->pre();

    $self->push_data;

    $self->source->post();
    $self->target->post();

    $self->source->finish();
    $self->target->finish();

    my $row_count = $self->row_count() // 0;
    my $end_time = time();
    my $duration = $end_time - $start_time;
    print "  Moved $row_count rows took $duration seconds\n";
}

private_method push_data => sub {
    my $self = shift;

    return if LocalTableToTable->new( { pipeline => $self } )->try;
    return if LocalSelectToTable->new( { pipeline => $self } )->try;
    return if LocalTableToT1dim->new( { pipeline => $self } )->try;
    return if RemotePwdTableToTable->new( { pipeline => $self } )->try;
    return if RemotePwdSelectToTable->new( { pipeline => $self } )->try;
    return if RowToRow->new( { pipeline => $self } )->try;

    print Dumper \$self;
    croak " No pump found ";
};

private_method check_layouts => sub {
    my $self = shift;

    return
      unless $self->target->does('SqlTarget')
      && $self->source->does('SqlSource');

    # Check layouts against each other
    if ( !$self->extraColumnsAllowed ) {
        my @problems = LayoutFactory::problems( $self->target->target_layout,
            $self->source->source_layout );
        if (@problems) {
            croak " Extra columns in source : "
              . join( ", ", @problems ) . " \n ";
        }
    }
    if ( !$self->missingColumnsAllowed ) {
        my @problems = LayoutFactory::problems( $self->source->source_layout,
            $self->target->target_layout );
        if (@problems) {
            croak " Missing columns in source : "
              . join( ", ", @problems ) . " \n ";
        }
    }

    my @columns = LayoutFactory::intersect( $self->source->source_layout,
        $self->target->target_layout );

    if ( $self->datatypeCheck ) {
        my @problems =
          LayoutFactory::compare_fields( $self->source->source_layout,
            $self->target->target_layout, \@columns );
        if (@problems) {
            print Dumper \@problems;
            croak " Mismatched datatypes as printed above\n";
        }
    }


};

1;
