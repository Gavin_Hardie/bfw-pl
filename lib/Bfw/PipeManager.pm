package PipeManager;

use strict;
use Moose;
use MooseX::Privacy;
use Carp;
use Bfw::Utils::Db;
use Bfw::Utils::DbUtils;
use Data::Dumper;
use Try::Tiny;

has 'alias'  => ( is => 'ro', isa => 'Str' );
has 'schema' => ( is => 'ro', isa => 'Str' );
has 'run_id' => ( is => 'rw', isa => 'Int' );
has 'dbh'    => ( is => 'rw', isa => 'Any', traits => ['Private'] );

sub BUILD {

    my $self = shift;

    # Based on alias & schema & existance of tables we set $self->dbh or not
    if ( length( $self->alias ) && length( $self->schema ) ) {
        my $dbh    = Db::connect( $self->alias() );
        my $schema = $self->schema();
        if (   DbUtils::table_exists( $dbh, $schema, 'load_control' )
            && DbUtils::table_exists( $dbh, $schema, 'run_control' ) )
        {
            $self->dbh($dbh);    # Set self->dbh
        }
        else {
            $dbh->disconnect;
        }
    }

    if ( defined( $self->dbh() ) ) {
        my $scriptname = $0;
        my $dbh        = $self->dbh();
        my $run_id     = $self->run_id();
        if ( !defined($run_id) ) {
            print "  Creating a new run id\n";
            my $sql =
                "insert into stg.run_control (name, start_time) "
              . "values (?, now())";
            my $sth = $dbh->prepare($sql);
            $sth->execute($scriptname);
            $run_id = $dbh->last_insert_id( undef, 'stg', 'run_control', 'id' );
            $self->run_id($run_id);
            $sth->finish;
        }
        else {
            print "  Checking existing run id\n";
            my $sql =
                "select count(*) from "
              . "stg.run_control where name = ? and id = ?";
            my ($count) =
              $dbh->selectrow_array( $sql, undef, $scriptname, $run_id );
            if ( $count != 1 ) {
                croak "Run id $run_id is not a valid run id for $scriptname";
            }
        }
        print "  Run id: $run_id\n";
    }
}

sub run_pipes {
    my $self      = shift;
    my @pipelines = @_;

    foreach my $pipeline (@pipelines) {
        my $result = $self->run_pipe($pipeline);
        last if $result == 0;
    }
}

sub run_pipe {
    my $self     = shift;
    my $pipeline = shift;

    my $dbh     = $self->dbh();
    my $run_id  = $self->run_id();
    my $dataset = $pipeline->dataset();
    my $action  = $pipeline->action();
    my $result  = 0;

    if ( defined($dbh) ) {
        my $sql =
            "select count(*) from stg.load_control where "
          . "run_id = ? "
          . "and dataset = ? "
          . "and action = ? "
          . "and status = 'SUCCESS'";
        my ($already_run) =
          $dbh->selectrow_array( $sql, undef, $run_id, $dataset, $action );
        return 1 if ( $already_run == 1 );
    }

    try {
        $pipeline->run();
        $self->update_load_control( $pipeline, 'SUCCESS' );
        $result = 1;
    }
    catch {
        $self->update_load_control( $pipeline, 'FAILURE' );
        print $_;
    };

    return $result;
}

private_method update_load_control => sub {
    my $self     = shift;
    my $pipeline = shift;
    my $status   = shift;

    my $dbh = $self->dbh();
    if ( defined($dbh) ) {

        my $dataset   = $pipeline->dataset();
        my $action    = $pipeline->action();
        my $row_count = $pipeline->row_count();
        my $run_id    = $self->run_id();

        my $update_sql =
            "update stg.load_control set status = ?, "
          . "row_count = ?, load_time = now() "
          . "where run_id = ? and dataset = ? and action = ?";
        my $affected =
          $dbh->do( $update_sql, undef, $status, $row_count, $run_id, $dataset,
            $action );
        if ( $affected == 0 ) {
            my $insert_sql =
                "insert into stg.load_control ( "
              . "run_id, dataset, action, status, row_count, load_time "
              . ") values ( "
              . "?, ?, ?, ?, ?, now() )";
            $dbh->do(
                $insert_sql, undef,   $run_id, $dataset,
                $action,     $status, $row_count
            );
        }
    }
};

sub finish {
    my $self = shift;
    my $dbh  = self->dbh();
    if ( defined($dbh) ) {
        $dbh->disconnect;
    }
}

1;
