package Field;
use strict;
use Moose;

has 'name'       => ( is => 'ro', isa => 'Str' );
has 'datatype'   => ( is => 'ro', isa => 'Str' );
has 'typename'   => ( is => 'ro', isa => 'Str' );
has 'columnsize' => ( is => 'ro', isa => 'Str' );
has 'isnullable' => ( is => 'ro', isa => 'Str' );

1;

sub equals {
    my $self  = shift;
    my $other = shift;

    return
         ( $self->name eq $other->name )
      && ( $self->datatype eq $other->datatype )
      && ( $self->typename eq $other->typename )
      && ( $self->columnsize eq $other->columnsize )
      && ( $self->isnullable eq $other->isnullable );
}
