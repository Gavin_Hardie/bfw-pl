package SqlTarget;

use strict;
use Moose::Role;
use Bfw::Target;

with('Target');

has 'alias'                 => ( is => 'ro', isa => 'Str' );
has 'pre_sql'               => ( is => 'rw', isa => 'ArrayRef[Str]' );
has 'post_sql'              => ( is => 'rw', isa => 'ArrayRef[Str]' );
has 'dbh'                   => ( is => 'rw', isa => 'Any' );

sub pre {
    my $self = shift;
    if ( defined( $self->pre_sql ) ) {
        my $dbh = $self->dbh;
        foreach my $sql ( @{ $self->pre_sql } ) {
            $dbh->do($sql);
        }
    }
}

sub post {
    my $self = shift;
    if ( defined( $self->post_sql ) ) {
        my $dbh = $self->dbh;
        foreach my $sql ( @{ $self->post_sql } ) {
            $dbh->do($sql);
        }
    }
}

1;
