package TableWriter;

use strict;
use Moose;
use MooseX::Privacy;
use Data::Dumper;
use Bfw::Utils::LayoutFactory;
use Bfw::Utils::ParameterisedStatement;
use Bfw::SqlTarget;

with qw(SqlTarget);

has 'schema'           => ( is => 'ro', isa => 'Str' );
has 'tablename'        => ( is => 'ro', isa => 'Str' );
has 'truncate'         => ( is => 'rw', isa => 'Bool', default => 0 );
has 'delete'           => ( is => 'rw', isa => 'Bool', default => 0 );
has 'force_row_by_row' => ( is => 'rw', isa => 'Bool', default => 0 );
has 'target_layout'    => ( is => 'rw', isa => 'Layout' );
has 'source_layout'    => ( is => 'rw', isa => 'Layout' );
has 'param_stmt'       => ( is => 'rw', isa => 'ParameterisedStatement' );
has 'sth'              => ( is => 'rw', isa => 'Any' );

sub start {
    my $self = shift;
    my $dbh  = Db::connect( $self->alias );

    $self->target_layout(
        LayoutFactory::for_table( $dbh, $self->schema, $self->tablename ) );
    $self->dbh($dbh);
}

after 'pre' => sub {
    my $self      = shift;
    my $dbh       = $self->dbh;
    my $tablename = $self->schema . '.' . $self->tablename;

    if ( $self->delete ) {
        my $sql = "delete from $tablename";
        $dbh->do($sql);
    }
    if ( $self->truncate ) {
        my $sql = "truncate $tablename";
        $dbh->do($sql);
    }
};

sub process_layout {
    my $self          = shift;
    my $source_layout = shift;

    $self->source_layout($source_layout);
}

sub finish {
    my $self = shift;
    $self->dbh->disconnect if defined( $self->dbh );
}

1;
