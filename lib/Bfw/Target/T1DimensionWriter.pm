package T1DimensionWriter;

use strict;
use Moose;
use MooseX::Privacy;

with qw(SqlTarget);

has 'schema'      => ( is => 'ro', isa => 'Str',           required => 1 );
has 'tablename'   => ( is => 'ro', isa => 'Str',           required => 1 );
has 'sk_col'      => ( is => 'ro', isa => 'Str',           required => 1 );
has 'unique_cols' => ( is => 'ro', isa => 'ArrayRef[Str]', required => 1 );
has 'target_layout' => ( is => 'rw', isa => 'Layout' );
has 'source_layout' => ( is => 'rw', isa => 'Layout' );
has 'dbh'           => ( is => 'rw', isa => 'Any' );

sub start {
    my $self = shift;
    my $dbh  = Db::connect( $self->alias );

    my $target_layout
        = LayoutFactory::for_table( $dbh, $self->schema, $self->tablename );
    $target_layout->forget_field( $self->sk_col );
    $self->target_layout($target_layout);

    $self->dbh($dbh);
}

sub process_layout {
    my $self          = shift;
    my $source_layout = shift;

    $self->source_layout($source_layout);
}

sub finish {
    my $self = shift;
    $self->dbh->disconnect;
}

1;
