package CsvWriter;
use strict;
use Moose;
use MooseX::Privacy;
use Data::Dumper;
use Utils::LayoutFactory;

with qw(Target);

has 'target_layout' => ( is => 'rw', isa => 'Layout' );

sub process_layout {
    my $self = shift;
    my $layout = shift;

    $self->layout($layout);
}

sub start {
}

sub pre {
}

sub post {
}

sub finish {
}

1;
