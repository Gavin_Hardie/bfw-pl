package Transform;

use Moose::Role;

requires 'process_layout';
requires 'process_row';
requires 'close';

has 'name' => ( is => 'rw', isa => 'Str' );

# An 'init' methoid
#sub BUILD {
#      my $self = shift;
#}

1;
