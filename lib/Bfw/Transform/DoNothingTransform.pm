package SqlSource;
use strict;
use Moose;
use Layout;
use Row;
with qw( Transform );

sub process_layout() {
    my $self = shift;
    my $layout = shift;
    return $layout;
}

sub process_row() {
    my $self = shift;
    my $row = shift;
    return $row;
}

sub close() {
    my $self = shift;
}
