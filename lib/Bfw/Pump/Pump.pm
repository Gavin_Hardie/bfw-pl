package Pump;
use strict;
use Carp;
use Data::Dumper;
use POSIX qw(WIFEXITED);
use Moose::Role;

has 'pipeline' => ( is => 'rw', isa => 'Pipeline' );

requires 'is_appropriate';
requires 'pump';

sub try {

    my $self = shift;

    if ( $self->is_appropriate ) {
        $self->pump();
        return 1;
    }
    return 0;
}

sub match {

    # 0 - no optimisation possible
    # 1 - same database " insert into select from " possible
    # 2 - seperate postgres database, same password, " psql | psql " possible
    # 3 - seperate postgres database, " psql > file, psql < file " possile

    my $self     = shift;
    my $pipeline = $self->pipeline;

    return 0
      unless $pipeline->source->does('SqlSource')
      && $pipeline->target->does('SqlTarget');

    my $alias1 = $pipeline->source->alias;
    my $alias2 = $pipeline->target->alias;

    return 1 if $alias1 eq $alias2;

    if (   K_Db::dsn($alias1) =~ m/^dbi[:]Pg/
        && K_Db::dsn($alias2) =~ m/^dbi[:]Pg/ )
    {
        return 2 if K_Db::pwd($alias1) eq K_Db::pwd($alias2);
        return 3;
    }
    return 0;
}

sub run_cmd {
    my $self = shift;
    my $cmd  = shift;
    $cmd =~ s/\n/ /msg;
    WIFEXITED( system $cmd);
    croak " Failed running $cmd: $? " if $?;
};

1;
