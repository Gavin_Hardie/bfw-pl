package RowToRow;
use strict;
use Bfw::Pump::Pump;
use Data::Dumper;
use Moose;

with qw( Pump );

sub is_appropriate {

    my $self   = shift;
    my $source = $self->pipeline->source;
    my $target = $self->pipeline->target;

    return $source->does('SqlSource')
      && $target->meta->name eq 'TableWriter';
}

sub pump {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    my $target_table = $target->schema . '.' . $target->tablename;
    my @columns      = LayoutFactory::intersect( $source->source_layout,
        $target->target_layout );
    my $column_list = join( ', ', @columns );
    my $source_query      = $source->query;

    my $source_sql = "select $column_list from ( $source_query ) a";
    my $target_sql = "insert into $target_table ($column_list) values ("
      . join( ", ", map { ':' . $_ } @columns ) . ")";

    print "  $source_sql\n";
    print "  $target_sql\n";

    # Prepare the insert statement
    my $param_stmt = ParameterisedStatement->new( { orig_sql => $target_sql } );
    my $ins_sth = $target->dbh->prepare( $param_stmt->sql );

    # Prepare and execute the select statement
    my $sth = $source->dbh->prepare($source_sql);
    $sth->execute;

    $target->dbh->begin_work();

    my $row = $sth->fetchrow_hashref;
    my $row_count = 0;
    while (defined($row)) {
        my @data = $param_stmt->make_params($row);
        $ins_sth->execute(@data);
        $row_count += 1;
        $row = $sth->fetchrow_hashref;
    }

    $target->dbh->commit();
    
    $ins_sth->finish;
    $sth->finish;

    $pipeline->row_count($row_count);
}
