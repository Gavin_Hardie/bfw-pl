package RemotePwdTableToTable;

use strict;
use Bfw::Pump::Pump;
use Data::Dumper;
use Moose;
use Bfw::Utils::Db;
with qw( Pump );

sub is_appropriate {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    return
         $source->meta->name eq 'TableSource'
      && $target->meta->name eq 'TableWriter'
      && $target->force_row_by_row == 0
      && Db::server( $source->alias ) eq 'Pg'
      && Db::server( $target->alias ) eq 'Pg'
      && Db::pwd( $source->alias ) eq Db::pwd( $target->alias );
}

sub pump {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    my $source_table = $source->schema . '.' . $source->tablename;
    my $source_where = $source->where_clause;
    my $target_table = $target->schema . '.' . $target->tablename;
    my $column_list  = join(
        ', ',
        LayoutFactory::intersect(
            $source->source_layout, $target->target_layout
        )
    );

    my $source_alias = $source->alias;
    my $password     = Db::pwd($source_alias);
    my $source_user  = Db::user($source_alias);
    my $source_host  = Db::host($source_alias);
    my $source_port  = Db::port($source_alias);
    my $source_db    = Db::dbname($source_alias);

    my $target_alias = $target->alias;
    my $target_user  = Db::user($target_alias);
    my $target_host  = Db::host($target_alias);
    my $target_port  = Db::port($target_alias);
    my $target_db    = Db::dbname($target_alias);

    my $cmd =
        " psql -U $source_user -h $source_host -p $source_port "
      . " -d $source_db -c "
      . " '\\copy ( select $column_list from $source_table $source_where ) to "
      . " STDOUT with (format csv)' " . " | "
      . " psql -U $target_user -h $target_host -p $target_port "
      . " -d $target_db -c "
      . " '\\copy $target_table ( $column_list ) from "
      . " STDIN with (format csv)'";

    $self->run_cmd( "export PGPASSWORD=$password; " . $cmd );
}

1;
