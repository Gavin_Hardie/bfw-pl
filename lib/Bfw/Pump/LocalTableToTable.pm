package LocalTableToTable;

use strict;
use Bfw::Pump::Pump;
use Data::Dumper;
use Moose;

with qw( Pump );

sub is_appropriate {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    return
         $source->meta->name eq 'TableSource'
      && $target->meta->name eq 'TableWriter'
      && $target->force_row_by_row == 0
      && $source->alias eq $target->alias;
}

sub pump {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    my $source_query = $source->query;
    my $target_table = $target->schema . '.' . $target->tablename;
    my @field_list   = LayoutFactory::intersect( $source->source_layout,
        $target->target_layout );
    my $columns = join( ', ', @field_list );
    my $sql =
        "insert into $target_table ( $columns ) "
      . "select $columns from ($source_query) a";

    my $row_count = $target->dbh->do($sql);

    $row_count = 0 if $row_count eq '0E0';

    $pipeline->row_count($row_count);
}

1;
