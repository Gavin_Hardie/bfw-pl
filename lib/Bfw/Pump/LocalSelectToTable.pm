package LocalSelectToTable;

use strict;
use Data::Dumper;
use Moose;
use Bfw::Pump::Pump;

with qw( Pump );

sub is_appropriate {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    return
         $source->meta->name eq 'SelectSource'
      && $target->meta->name eq 'TableWriter'
      && $target->force_row_by_row == 0
      && $source->alias eq $target->alias;
}

sub pump {

    my $self     = shift;
    my $pipeline = $self->pipeline;

    my $source_select = $pipeline->source->select;
    my $target_table =
      $pipeline->target->schema . '.' . $pipeline->target->tablename;
    my $column_list = join(
        ', ',
        LayoutFactory::intersect(
            $pipeline->source->source_layout,
            $pipeline->target->target_layout
        )
    );

    my $sql =
        "insert into $target_table ( $column_list ) "
      . "select $column_list from ( $source_select ) a";

    my $row_count = $pipeline->target->dbh->do($sql);
    $row_count = 0 if $row_count eq '0E0';
    $pipeline->row_count($row_count);
}
