package LocalTableToT1dim;
use strict;

use Bfw::Pump::Pump;
use Data::Dumper;
use Moose;

with qw( Pump );

sub is_appropriate {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    return ( ( $source->meta->name eq 'TableSource' )
          || ( $source->meta->name eq 'SelectSource' ) )
      && $target->meta->name eq 'T1DimensionWriter'
      && $source->alias eq $target->alias;
}

sub pump {

    my $self     = shift;
    my $pipeline = $self->pipeline;
    my $source   = $pipeline->source;
    my $target   = $pipeline->target;

    my $source_query = $source->query;
    my $target_table = $target->schema . '.' . $target->tablename;
    my @columns      = LayoutFactory::intersect( $source->source_layout,
        $target->target_layout );

    my %h;
    @h{ @{ $target->unique_cols } } = undef;
    my @attribute_columns = grep { not exists $h{$_} } @columns;
    my @unique_columns = @{ $target->unique_cols };

    my $insert_clause = join( ', ', @columns );
    my $set_clause = join( ', ', map { $_ . '=s.' . $_ } @attribute_columns );
    my $join_clause =
      join( ' and ', map { 't.' . $_ . '=s.' . $_ } @unique_columns );

    my $is_distinct_clause = join( ' or ',
        map { 't.' . $_ . ' is distinct from s.' . $_ }
        grep { $_ ne 'load_date' } @attribute_columns );

    my $sql_update = "update $target_table as t set $set_clause from"
      . " ($source_query) as s where $join_clause and ( $is_distinct_clause )";

    my $conflict_columns = join( ', ', @unique_columns );

    my $rows_updated = $target->dbh->do($sql_update);

    #    my $sql_insert =
    #        "insert into $target_table($insert_clause) "
    #      . "select $insert_clause from ($source_query) s "
    #      . "where not exists "
    #      . "( select 1 from $target_table t where $join_clause )";

    my $sql_insert =
        "insert into $target_table($insert_clause) "
      . "select $insert_clause from ($source_query) s "
      . "on conflict ( $conflict_columns ) do nothing";

    my $rows_inserted = $target->dbh->do($sql_insert);

    $pipeline->row_count( $rows_updated + $rows_inserted );
}

1;
