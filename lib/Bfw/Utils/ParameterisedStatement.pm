package ParameterisedStatement;
use strict;
use Moose;
use Data::Dumper;
use MooseX::Privacy;

has 'sql' => ( is => 'rw', isa => 'Str' );
has 'orig_sql' => ( is => 'ro', isa => 'Str' );
has 'params' => ( is => 'rw', isa => 'ArrayRef[Str]');

sub BUILD {
    my $self = shift;

    my @params = $self->orig_sql =~ m/[^:][:](\w+)/g;
    $self->params( \@params );

    my $sql = $self->orig_sql =~ s/([^:])[:]\w+/$1\?/gr;
    $self->sql($sql);
}

sub make_params {
    my $self = shift;
    my $dataref = shift;

    my @result = ();
    foreach my $param (@{$self->params}) {
        push @result, $dataref->{$param};
    }

    return @result;
}

#
# my $test = "insert into foo (a, b, c) values (:col1, :col2, :col3::numeric(13,2))";
# while ($test =~ m/[^:][:](\w+)/g) {
#     print "$1\n";
# }
# $test =~ s/([^:])[:]\w+/$1\?/g;
# print "$test\n";
#

1;
