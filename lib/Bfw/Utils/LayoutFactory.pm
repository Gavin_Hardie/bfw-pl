package LayoutFactory;

use strict;
use Switch;
use DBIx::Class::Schema::Loader;
use Bfw::Utils::Db;
use Data::Dumper;
use Bfw::Layout;
use Bfw::Field;

# stmt meta data reports precision as different from the defined column size.
# These are the adjustments per datatype that need to be applied when using
# sth->PRECISION rather than dbh->column_info
my %select_width_adjustments = (
    1  => -4,    # bpchar
    12 => -4,    # text
);

sub for_table {
    my $dbh    = shift;
    my $schema = shift;
    my $table  = shift;

    my $sth = $dbh->column_info( undef, $schema, $table, undef );

    my @fields = ();
    while ( my $ref = $sth->fetchrow_hashref ) {
        my $name = $ref->{COLUMN_NAME};
        $name =~ s/"//g;
        my $datatype = $ref->{DATA_TYPE};
        my $typename = (
            $datatype == -1
            ? "UNKNOWN"
            : $dbh->type_info($datatype)->{TYPE_NAME}
        );
        my $columnsize = $ref->{COLUMN_SIZE};
        my $field      = Field->new(
            {
                name       => $name,
                datatype   => $datatype,
                typename   => $typename,
                columnsize => $columnsize // 'undefined',
                isnullable => $ref->{IS_NULLABLE},
            }
        );
        push @fields, $field;
    }

    my $result = Layout->new(
        {
            name   => $schema . '.' . $table,
            fields => \@fields,
        }
    );

    $sth->finish;
    return $result;
}

sub for_select {
    my $dbh = shift;
    my $sql = shift;

    #my $wrapped_sql = "select * from ( $sql ) a where 1 = 2 ";
    my $wrapped_sql = $sql . " fetch first 0 rows only";
    my $sth         = $dbh->prepare($sql);
    $sth->execute;
    my @fields   = ();
    my $num_cols = @{ $sth->{NAME} };
    for ( my $i = 0 ; $i < $num_cols ; $i++ ) {
        my $name = $sth->{NAME}[$i];
        $name =~ s/"//g;
        my $datatype = $sth->{TYPE}[$i];
        my $typename = (
            $datatype == -1
            ? "UNKNOWN"
            : $dbh->type_info($datatype)->{TYPE_NAME}
        );
        my $scale     = $sth->{SCALE}[$i];
        my $precision = $sth->{PRECISION}[$i];
        my $columnsize =
            $scale eq ''
          ? $precision + get_width_adjustment($datatype)
          : $scale . ',' . $precision;
        my $isnullable;
        switch ( $sth->{NULLABLE}[$i] ) {
            case 0 { $isnullable = 'NO' }
            case 1 { $isnullable = 'YES' }
            case 2 { $isnullable = 'UNKNOWN' }
            else   { $isnullable = 'UNEXPECTED VALUE' }
        }
        my $field = Field->new(
            {
                name       => $name,
                datatype   => $datatype,
                typename   => $typename,
                columnsize => $columnsize,
                isnullable => $isnullable,
            }
        );
        push @fields, $field;
    }

    my $result = Layout->new(
        {
            name   => 'select statement',
            fields => \@fields,
        }
    );

    $sth->finish;

    return $result;
}

sub get_width_adjustment {
    my $datatype   = shift;
    my $adjustment = $select_width_adjustments{$datatype};
    if ( defined($adjustment) ) {
        return $adjustment;
    }
    return 0;
}

sub problems {    # check that left contains right, return problems
    my $left  = shift;
    my $right = shift;

    my %left_fields = ();
    foreach my $field ( @{ $left->fields } ) {
        $left_fields{ $field->name } = 1;
    }
    my @problems = ();
    foreach my $field ( @{ $right->fields } ) {
        if ( $left_fields{ $field->name } != 1 ) {
            push @problems, $field->name;
        }
    }
    if (@problems) {
        print "LEFT ";
        foreach my $field ( @{ $left->fields } ) {
            print $field->name . " ";
        }
        print "\nRIGHT ";
        foreach my $field ( @{ $right->fields } ) {
            print $field->name . " ";
        }
    }
    return @problems;
}

# Get the list of fieldnames that are in both layouts
sub intersect {
    my $left        = shift;
    my $right       = shift;
    my %left_fields = ();
    foreach my $field ( @{ $left->fields } ) {
        $left_fields{ $field->name } = 1;
    }
    my @result = ();
    foreach my $field ( @{ $right->fields } ) {
        if ( $left_fields{ $field->name } == 1 ) {
            push @result, $field->name;
        }
    }
    return @result;
}

sub compare_fields {
    my $layout1        = shift;
    my $layout2        = shift;
    my $ref_fieldnames = shift;

    my @fields = @{$ref_fieldnames};

    my @result = ();
    foreach my $field (@fields) {
        my $field1 = $layout1->get_field($field);
        my $field2 = $layout2->get_field($field);

        if ( !$field1->equals($field2) ) {
            my %problem = (
                name   => $field,
                field1 => $field1,
                field2 => $field2,
            );
            push @result, \%problem;
        }
    }

    return @result;
}

1;
