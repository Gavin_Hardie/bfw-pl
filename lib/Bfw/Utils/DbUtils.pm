package DbUtils;

use strict;
use Data::Dumper;
use Bfw::Utils::Db;
use Bfw::Utils::LayoutFactory;

sub table_exists {
    my $dbh       = shift;
    my $schema    = shift;
    my $tablename = shift;

    my $result = 0;
    my $sth = $dbh->table_info( undef, $schema, $tablename, undef );
    while ( my $ref = $sth->fetchrow_hashref ) {
        $result = 1;
        last;
    }

    $sth->finish;

    return $result;
}

1;
