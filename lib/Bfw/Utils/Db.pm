package Db;

use strict;
use Carp;
use Data::Dumper;
use DBI qw(:sql_types);
use FindBin qw($Bin);
use Fatal qw(open close);
use Hash::Util qw(lock_keys unlock_keys);
use Config::General;

$SIG{__WARN__} = sub { die $_[0] };    # Turn warnings into die

my $config_file = "$ENV{MIS}/mis.conf";

my %CFG = Config::General::ParseConfig($config_file);

sub connect {
    my $alias = shift;
    if ( !exists $CFG{DB}{$alias}{DSN} ) {
        croak "Cannot find entry for $alias in $config_file";
    }
    my $dbh = DBI->connect(
        $CFG{DB}{$alias}{DSN},
        $CFG{DB}{$alias}{USER},
        $CFG{DB}{$alias}{PWD},
        {
            PrintError => 1,
            AutoCommit => 1,
            ChopBlanks => 1,

            #RaiseError       => 1,
        }
    ) or croak "$DBI::errstr\n";
    return $dbh;
}

sub dsn {
    my $alias = shift;
    return property( $alias, 'DSN' );
}

sub host {
    my $alias = shift;
    return property( $alias, 'HOST' );
}

sub port {
    my $alias = shift;
    return property( $alias, 'PORT' );
}

sub pwd {
    my $alias = shift;
    return property( $alias, 'PWD' );
}

sub user {
    my $alias = shift;
    return property( $alias, 'USER' );
}

sub dbname {
    my $alias = shift;
    return property( $alias, 'DBNAME' );
}

sub server {
    my $alias = shift;
    return property( $alias, 'SERVER' );
}

sub version {
    my $alias = shift;
    return property( $alias, 'VERSION' );
}

sub property {
    my $alias = shift;
    my $prop  = shift;
    if ( !exists $CFG{DB}{$alias} ) {
        croak "Cannot find entry for $alias in config file";
    }
    return $CFG{DB}{$alias}{$prop};
}

1;
