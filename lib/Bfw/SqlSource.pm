package SqlSource;

use strict;

use Moose::Role;
use Bfw::Source;

with('Source');

has 'alias' => ( is => 'ro', isa => 'Str', required => 1 );
has 'pre_sql'  => ( is => 'rw', isa => 'ArrayRef[Str]' );
has 'post_sql' => ( is => 'rw', isa => 'ArrayRef[Str]' );
has 'dbh'      => ( is => 'rw', isa => 'Any' );
has 'sth'      => ( is => 'rw', isa => 'Any' );

requires 'query';

sub query;

sub pre {
    my $self = shift;
    if ( defined( $self->pre_sql ) ) {
        my $dbh = $self->dbh;
        foreach my $sql ( @{ $self->pre_sql } ) {
            print "  $sql\n";
            $dbh->do($sql);
        }
    }
}

sub post {
    my $self = shift;
    if ( defined( $self->post_sql ) ) {
        my $dbh = $self->dbh;
        foreach my $sql ( @{ $self->post_sql } ) {
            print "  $sql\n";
            $dbh->do($sql);
        }
    }
}

sub open {
    my $self = shift;

    my $dbh = $self->dbh();
    my $sth = $dbh->prepare( $self->query() );
    $self->sth($sth);
}

sub get_row {

    # Get row from sth
}

sub close {
    my $self = shift;
    my $sth  = $self->sth();
    $sth->finish;
}

1;
