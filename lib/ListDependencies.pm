#!/usr/bin/perl -w
package ListDependencies;
use strict;
use ExtUtils::MakeMaker;
unshift @INC, sub {
        local $_ = $_[1];
        return unless /[[:upper:]]/;
        s/\.pm$//i;
        s/[\/:]/::/g;
        # print STDERR $_, $/;
        my $filename=`perldoc -l $_`;
        chomp $filename;
        print $filename,":ver:";
        print MM->parse_version($filename),"\n";
};
1;
__END__

=head1 NAME

ListDependencies.pm - list any Perl code's module dependencies

=head1 SYNOPSIS

perl -c -MListDependencies scriptname
perl -MListDependencies -MModule::Name -e1
perl -MListDependencies -e'use Module::Name'

=head1 DESCRIPTION

This module prints the names of any modules any piece
of Perl code compiled after it tries to load to C<STDERR>,
but ignores C<use pragma;> directives. You should load
this module prior to all others. The C<-M> command line
switch to C<perl> is a good place to do so.

Note that it won't prevent execution of the code you're
trying to examine. If all you want to do is list
dependencies for a piece of standalone code, you may
want to use the C<-c> command line switch to C<perl> to
check compile time dependencies. Be aware that this will
leave you blind to any attempts to load modules
dynamically at runtime.

=head1 CAVEATS

If your own custom modules use all-lowercase names, this
module won't list them. This is intentional:
all-lowercase names are reserved for Perl pragmas.

Note that not all pragmas are available everywhere, so
they may constitute a depency as well. L<warnings> is an
obvious example.

=head1 BUGS

There is no way to specify a different output file handle
than C<STDERR>. A proper version should include an
C<import()> routine to take care of this.

=head1 RESTRICTIONS

The module only honours Unix, DOSish and MacOS directory
separators. A more portable solution would have to rely
on L<File::Spec>, but unfortunately that would require
this module to import modules itself.
Heisenberg's Dispersion Relation applies. :-)

=head1 AUTHOR

Aristotle Pagaltzis <pagaltzis at gmx dot de>

=head1 COPYRIGHT

Copyright 2002. This may be modified and distributed on
the same terms as Perl.

1;
__END__

=head1 SYNOPSIS



=head1 INPUT



=head1 OUTPUT



=head1 NOTES

=cut

