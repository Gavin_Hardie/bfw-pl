=head1 Kromco MIS Documentation

This is the Kromco MIS documentation.   

This documentation does not provide help on generic Perl or Linux
issues. See the L<"Help on help"|"Help on help"> section.

=head1 HWE tables

These tables are in the C<hwe> schema. They are loaded using the
C<hwe_layout.TXT> file as metadata. The copy of this file that is used 
to create and load the tables is in C<$MIS/imp> directory. The tables are
created and loaded using the C<$MIS/util/hwe> utility eg :

    $ hwe -tTab -d

To create the table in the database, and

    $ hwe -tTab -l 

to load it, where C<Tab> is the heading in the C<hwe_layout.TXT> file 

=head1 Naming Standards

Naming conventions are important, as the
system will be probably maintained by numerous people over a long period of time. Requirements for 
the naming conventions are that they are:

=over

=item terse, for the sake of developer productivity, and because shorter pathnames are
easier to use in the ControlM context.

=item specific, to avoid confusion in locating and remembering names, and to accomodate
many additions over time.

=back

All naming standards and conventions as listed in 'Perl Best Practices' (PBP) apply, unless covered here.

=head2 Scripts

Scripts are named C<AA9999Z.XXX>, where 

=head4 C<AA> is one of:

=over 

=item C<ex> - for scripts that extract from source systems

=item C<xf> - for scripts that perform transformations ie refresh logic of dimensions and fact tables

=item C<rp> - for scripts that extract data from the MIS system, or perform reporting functionality

=item C<rc> - for data reconciliation scripts

=item C<ri> - for referential integrity checks

=item C<xc> - for Excel and CSV reports

=back

=head4 C<9999> 

is a consecutive number applied to scripts that work together,
commonly to refresh a single table in the presentation area.

The procedure for allocating a number to new scripts is:

=over

=item

Check out the C<README> file from C<$MIS>.

=item 

Add a line such as :

    0034        Programs to perform refresh of MIS.TD_ACCOUNT_TDA.

using the next consecutive available number

=item

Commit the C<README> file.

=back 


=head4 C<Z> - letter

Is letter C<a-z>, to provide for several scripts participating in
the same data flow.

=head4 C<XXX> - extension

=over

=item C<.csv>   - For delimited files, variable length

=item C<.txt>   - For fixed width files

=item C<.sql>   - For SQL scripts

=item C<.pl>   - For Perl scripts

=item C<.pm>   - For Perl modules

=item C<.t>   - For test scripts

=item C<.ddl>   - For files containing table definitions in C<$MIS/ddl>

=item Utilities in C<$MIS/util> have no filename extensions.

=back

If two developers try to commit conflicting changes the second will
get an error message saying their version is out of step with the
repository. That developer will then have to check the file out again,
apply their changes and commit.

=head4 Example

An example would be C<ex0001.pl> and C<xf0001.pl> which could together
implement the ETL for a table in the MIS presentation schema. In
cases where more than one script is needed to do extracts, additional
extract scripts can be named (staying with this example) C<ex0001a.pl>,
C<xf0001b.pl>, etc.

=head2 Perl modules

Top level MIS local modules (in C<$MIS/lib>) are named C<K_*>. Below
that modules are named according to the PBP naming convention.

=head2 Perl Identifiers

Generally the guidelines in PBP should be followed. In summary:

=over

=item Private variables - C<lower_case_with_underscore>.

=item Constants - Upper case

=item Loop variables - C<i,j,k,...>

=item Hashes - Singular noun, optionally followed by preposition eg C<%row>, C<%count_for>,...

=item Arrays - Plural noun

=item Subroutines - Verb followed by noun eg C<get_record>, C<eat_cookie>.

=item Reference variables - end in C<_ref>.

=item Package and Class names - Capitalized 

=back 

=head2 Database

TBD...

In cases where extract staging tables bear no resemblance to target tables 
they can be named according to the source system file or table name 
with a standard prefix mnemonic referring to the source system eg C<STG.HOS_CUST_ID>.

Column names for staging tables should be named according to which they resemble more
closely, source or target.

=head1 Setup and Getting Started

=over

=item 

Get the hang of  the GNU screen utility and use it

=item

Create the directory under your home directory where the MIS directory structure will be
placed. This is commonly in C<~/MIS>:

        $ mkdir ~/MIS

=item

Check that the MIS environment variables are setup correctly.

        $ echo $MIS             (should get /home/fyournumber/MIS)
        $ echo $PERL5LIB        (should get /home/fyournumber/MIS/lib)

If not set, add the following lines to your C<~/.bashrc> file:


    export MIS=/home/your_os_user/MIS/
    export PATH=/opt/perl/bin:$MIS/lib/bin:$MIS/util:$PATH
    export PERL5LIB=$MIS/lib

and source the file:

    $ . ~/.bashrc


=item 

Setup SVN. See the section on L<Subversion|"Subversion (SVN)"> for help on this, 
the L<Getting started with Subversion for MIS|"Getting started with Subversion for MIS">
subsection 

=item 

Create the C<$MIS/stg> and C<$MIS/log> directories.

=item 

Edit the C<$MIS/mis.conf.sample> file, entering your details. 
Refer to the L<mis.conf|"Configuration file: mis.conf"> section
for an explanation of the entries in that file.
You have to enter your userid and password for database connections and
supply your home directory in places. In the development environment, the C<MIS>
entry refers to the IQ database on C<dwsdev01>. Save the file as C<$MIS/mis.conf>.

Please note that the initial checkout from svn is the only time your L<mis.conf|"Configuration file: mis.conf"> file should
have anything to do with svn. Hereafter it is manual editing only. L<mis.conf|"Configuration file: mis.conf"> files are
the ONLY environment-specific files in the MIS system, hence need to be 
maintained in each environment, and not committed/updated in svn, other than an initial
checkout to get you up and running.

=item

To run C<SELECT> SQL, the C<rsql> utility is provided, so you can go

        $ rsql MIS 'select getdate()'

to run SQL against the staging database. This is also an easy way to check that everything
is setup correctly. See the L<Utilities|"Command-line utilities"> section for help on rsql.
The first argument uses entries from the C<DB> block in the C<$MIS/mis.conf> module. 

=item 

To run a script you are editing use the C<Ctrl-p> keyboard shortcut in vim. There are several
local keyboard shortcuts in the /etc/vimrc file. See L<local vim keyboard shortcuts|"VIM">.

=item 

To run C<LOAD TABLE> SQL, use the L<rsql|"rsql"> utility.

=back

=head1 Database object creation and maintenance

The objectives are to:

=over 

=item 

Encourage use of the same datatypes for the same columns
occurring in different tables, such as C<*_SK>and amount columns, by
means of user-defined data types (UDTs).

=item 

Provide a uniform way of creating and maintaining DB objects and the DDL code used 

=back

To this end, C<*.ddl> files in the C<$MIS/ddl> directory are used, one
for each table in the MIS. A C<TYPES> file in this directory is used
to map UDTs to IQ data types. Data types in the individual C<.ddl>
files are UDTs in this file. The C<.ddl> files are all checked into
svn, allowing changes to be tracked.

Searching across the C<.ddl> files and the C<TYPES> file shows the use of a data type
across the database, providing impact analysis of potential changes.

=head1 Subversion (SVN)

SVN is the version control tool that controls changes made to all scripts.  MIS scripts
must be obtained and committed only via SVN, not copied between environments/developers.

=head2 Learning Subversion

A comprehensive guide to Subversion is available L<here|http://svnbook.red-bean.com>

=head2 Getting started with Subversion for MIS

To check out the trunk branch of the MIS repo, assuming you have C<$MIS>
setup:

        $ cd $MIS/.. && svn co \
            svn+ssh://myuser@172.16.16.43/home/subversion/depot/MIS/trunk $MIS

where C<myuser> is your OS username

You can see a full listing of the trunk branch on the repository with:

        $ svn list -R svn+ssh://myuser@172.16.16.43/home/subversion/depot/MIS/trunk

To add a script to the repository:

        $ svn add somescript.pl
        $ svn commit -m"some comment" somescript.pl

To commit a change, 

        $ svn commit -m"Changed something" somescript.pl

from the directory that contains your script. To get a history of changes made to anything:

        $ svn log somescript.pl

To see local changes to your script vs the repository:

        $ svn diff somescript.pl

To remove local changes that have been made to a script and revert to the
latest version in the repo:

    $ svn revert somescript.pl

To restore a script to a previous version, find it using the C<log>
subcommand, then:

    $ svn cat -rthatversion somescript.pl > somescript.pl

To see the difference between development and live versions of a script in the
repo:

    $ svn diff \
    svn+ssh://myuser@172.16.16.43/home/subversion/MIS/trunk/path/to/script.pl \
    svn+ssh://myuser@172.16.16.43/home/subversion/MIS/prod/path/to/script.pl
    
To see the difference between your working copy and the working copy on live:

    $ diff somescript.pl <(ssh myuser@172.16.16.25 cat /path/to/somescript.pl)

For side by side diff, add C<-y> flag

=head1 Modules

Documentation related to each module is found in the module. For example
if your C<PERL5LIB> environment variable is setup as above,
 you can find documentation about C<K_Module> like this:

        $ perldoc K_Module

Or use the web server. All documentation for modules under the C<$MIS/lib>
and C<$MIS/web/lib> directories are in the C</perldoc> path eg

=for html <a href="/perldoc/K_HWE">K_HWE</a>

=over

=item

=for html <a href="/perldoc/K_Db">Routines for DB access</a>

=item

=for html <a href="/perldoc/K_Googledocs">Routines to upload to Google docs</a>

=back

To see further details of how the module can be used, look at the test script for it. Test scripts 
are in the C<$MIS/lib/t> directory.

=head1 Report utility

Create a markup script like the following:

    SQL   <<EOF
    select * from d_country
    EOF
    FORMAT DELIMITED
    HEADER_ROW 1
    <DEST>
        OUTFILE = country.csv
    </DEST>

And name the script C<rpt9999.cf> where C<9999> is the number from 
updating the C<README> file. The C<rpt> utility will run the SQL and output
the result as a delimited file to C<$MIS/stg/country.csv>.


    $ rpt 9999


=head1 Script Standards

This section lists the standards for MIS scripts. Also see L<Naming Standards|"Naming Standards">

=over

=item 

Remove unused variables and modules

=item 

Remove commented out code that is no longer useful

=item

Tabstops should be 4 characters, and be saved by the editor as 
spaces, not the physical C<TAB> character (hex 09).

=item

All code, strings and comments should not be wider than 80 chars. 

=item 

Run perltidy

=item 

Add the documentation at the end of the script, according to the 
template:

    =head1 SYNOPSIS

    xf9999.pl arguments

    =head1 DESCRIPTION

    Functionality of the script

    =head1 INPUT

    Tables, files, that the script reads from

    =head1 OUTPUT

    What the script outputs to

    =head1 SCHEDULING

    1st business day, ...

=item 

Check that the script - as far as possible - follows the same
way of doing things as other scripts, ito sequence of variable
declaration, etc, naming conventions, etc. Remember one of the
virtues of a good programmer: laziness - the reluctance to re-invent
the wheel, rather using a way something has been done before. But then 
also remember to avoid cargo cultism.

=item

Laziness, as in brevity, is also valued. In Perl,
the creation of short-lived variables can usually be avoided. 
Less code (usually) means less to maintain and easier to understand. 

=item 

The book by Damian Conway, 'Perl Best Practices' (PBP), except for suggestions 
about brace placing, should be followed. In all matters of standards, unless
contradicted by something in this documentation, the advice in that book applies.

=item

svn commit

=item 

Email/Tell someone else on the team - the script now needs a code inspection/review, 
that will evaluate the code against this standard.

=back

=head1 Deployment Standards

=over

=item

Ensure the script is executable. If not, change to executable
and change the svn repository:

    $ chmod +x script.pl
    $ svn propset svn:executable ON srcipt.pl

=item

Ensure the script is committed to svn on the trunk branch, and the steps
in the L<"Script Standards"|"Script Standards"> section are completed.

=item

If modules changes are needed, or you are not sure which module changes 
maybe involved in the deployment follow these steps:

=over

=item

Use the C<ListDependencies> module to check which modules are used, directly
and indirectly by a script:

    $ perl -c -MListDependencies scriptname

=back

=item 

New Database Objects

=over

=item 

Check that DDL for any new tables on IQ are  checked into svn on
C<$MIS/ddl/SCHEMA.NEWTABLE.ddl>

=item

Specify necessary permissions for the tables in the change order

=back

=item 

Prepare and submit change order 

=back 

=head1 Coding Layout

Scripts must be run through C<perltidy> before being committed. There is 
a site-wide configuration for perltidy, which ensures that all scripts
get the same layout. The procedure for running C<perltidy> is 
described for vim above. In Emacs, this can be done by the key 
sequence: C< C-x h Esc 1 Esc | perltidy >, or you can bind this to a 
key for ease of use.

=head1 Tags

In the $MIS/util directory is a script called C<pltags.pl>. Run this script 
from your $MIS directory:

        $ $MIS/util/pltags.pl $MIS/lib/*.pm

This will create a file called C<tags>. Then edit your C<~/.vimrc> file, adding
the line:

        se tags=/home/yourfnumber/MIS/tags

Now, in vim, you can jump to module subroutines with the C<Ctrl-]> key and back
with the C<Ctrl-t> key. If eg your cursor is over the C<K_Db::Connect> subroutine
in a script and you want to check the parameters to that subroutine, 
go C<Ctrl-]> to jump to the subroutine definition.

=head1 Command-line utilities

These are all in C<$MIS/util>, so that directory needs to be in the current shell
path.

=over

=item Syntax convention

In arguments, C<{}> indicates mandatory arguments, C<[]> optional arguments. C<|>
indicates alternative options.

=back

=head1 Configuration file: mis.conf

Divided into markup sections similar to the syntax of the apache C<.conf>
files. Unfortunately it cannot be commented because of the implementation
of password encryption, which rewrites the file when the L<crpt|"Password encryption"> utility
is run.

The file is divided into sections corresponding to different application
uses. Any entry called PWD is meant to be encrypted using the L<crpt|"Password encryption"> utility.
Documentation for each section follows:

=head2 DB

This section contains a tag pair for each database connection that is used in
scripts. Each connection typically has a DSN value, which is used by
Perl DBI, and  PWD and USER values. The DBD::Sybase driver does not
make provision for symbolic data type values, so each IQ connection
has a set of values describing data types, which is used for certain
formatting functionality, mainly in the rsql utility, which is not used
in production.

=over

=item DSN - the Perl DBI data source string used to connect to the database

=item USER - database userid

=item PWD  - encrypted password, entered with the C<crpt> utility.

=back

Currently, the database connections are:

=over

=item MES - connection to the MES production database

=item MIS - connection to the MIS database

=back

=head2 FTP

Contains subsections, with IP-address, userid and password entries for each ftp connection

=head2 GEN

Miscelaneous options, currently:

=over

=item DEBUG - Can be used by scripts to print debug information

=back

=head1 Changing this documentation

Edit the pod source in C<$MIS/lib/Doc/MIS.pm>. Remember to commit changes to svn.

That is literally it! The documentation is picked up and translated to html by the web
server using the URL: http://ipaddress:port/perldoc/Doc::MIS. You just have to 
start your web server with:

    morbo $MIS/web/script/web -l http://*:port

=cut

=for vim:tw=90:sw=4:ts=4:ft=help:norl:
