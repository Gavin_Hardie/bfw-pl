=head1 Komco MIS Documentation

This is the Kromco MIS documentation.   

asdlfkjadlsfk jalskjf dafslkj 
This documentation does not provide help on generic Perl or Linux
issues. See the L<"Help on help"|"Help on help"> section.

=head1 Naming Standards

Naming conventions are important, as the
system will be probably maintained by numerous people over a long period of time. Requirements for 
the naming conventions are that they are:

=over

=item terse, for the sake of developer productivity, and because shorter pathnames are
easier to use in the ControlM context.

=item specific, to avoid confusion in locating and remembering names, and to accomodate
many additions over time.

=back

All naming standards and conventions as listed in 'Perl Best Practices' (PBP) apply, unless covered here.

=head2 Scripts

Scripts are named C<AA9999Z.XXX>, where 

=head4 C<AA> is one of:

=over 

=item C<ex> - for scripts that extract from source systems

=item C<xf> - for scripts that perform transformations ie refresh logic of dimensions and fact tables

=item C<rp> - for scripts that extract data from the MIS system, or perform reporting functionality

=item C<rc> - for data reconciliation scripts

=item C<ri> - for referential integrity checks

=item C<xc> - for Excel and CSV reports

=back

=head4 C<9999> 

is a consecutive number applied to scripts that work together,
commonly to refresh a single table in the presentation area.

The procedure for allocating a number to new scripts is:

=over

=item

Check out the C<README> file from C<$MIS>.

=item 

Add a line such as :

    0034        Programs to perform refresh of MIS.TD_ACCOUNT_TDA.

using the next consecutive available number

=item

Commit the C<README> file.

=back 


=head4 C<Z> - letter

Is letter C<a-z>, to provide for several scripts participating in
the same data flow.

=head4 C<XXX> - extension

=over

=item C<.csv>   - For delimited files, variable length

=item C<.txt>   - For fixed width files

=item C<.sql>   - For SQL scripts

=item C<.pl>   - For Perl scripts

=item C<.pm>   - For Perl modules

=item C<.t>   - For test scripts

=item C<.ddl>   - For files containing table definitions in C<$MIS/ddl>

=item Utilities in C<$MIS/util> have no filename extensions.

=back

If two developers try to commit conflicting changes the second will
get an error message saying their version is out of step with the
repository. That developer will then have to check the file out again,
apply their changes and commit.

=head4 Example

An example would be C<ex0001.pl> and C<xf0001.pl> which could together
implement the ETL for a table in the MIS presentation schema. In
cases where more than one script is needed to do extracts, additional
extract scripts can be named (staying with this example) C<ex0001a.pl>,
C<xf0001b.pl>, etc.

=head2 Perl modules

Top level MIS local modules (in C<$MIS/lib>) are named C<K_*>. Below
that modules are named according to the PBP naming convention.

=head2 Perl Identifiers

Generally the guidelines in PBP should be followed. In summary:

=over

=item Private variables - C<lower_case_with_underscore>.

=item Constants - Upper case

=item Loop variables - C<i,j,k,...>

=item Hashes - Singular noun, optionally followed by preposition eg C<%row>, C<%count_for>,...

=item Arrays - Plural noun

=item Subroutines - Verb followed by noun eg C<get_record>, C<eat_cookie>.

=item Reference variables - end in C<_ref>.

=item Package and Class names - Capitalized 

=back 

=head2 Staging Filenames

Names of load files should correspond to the table to be loaded eg
C<MIS.TD_CUSTOMER.txt>. Temporary files used should be named according
to the script producing them eg C<ex0034.txt>. When more than one 
temporary text file is produced by the same program, a letter can be 
added after the 4-digit program number eg C<ex0034a.txt>, C<ex0034b.txt>...

=head2 Database

TBD...

In cases where extract staging tables bear no resemblance to target tables 
they can be named according to the source system file or table name 
with a standard prefix mnemonic referring to the source system eg C<STG.HOS_CUST_ID>.

Column names for staging tables should be named according to which they resemble more
closely, source or target.

=head1 Setup and Getting Started

=over

=item 

Get the hang of L<Screen|/perldoc/Doc/X>

=item

(From within Screen) Create the directory under your home directory where the MIS 
directory structure will be
placed. This is commonly in C<~/MIS>:

        $ mkdir ~/MIS

=item

Check that the MIS environment variables are setup correctly.

        $ echo $MIS             (should get /home/fyournumber/MIS)
        $ echo $PERL5LIB        (should get /home/fyournumber/MIS/lib)

=item 

Setup SVN. See the section on L<Subversion|"Subversion (SVN)"> for help on this, 
the L<Getting started with Subversion for MIS|"Getting started with Subversion for MIS">
subsection 


=item 

Create the C<$MIS/stg> and C<$MIS/log> directories.

=item 

Edit the C<$MIS/mis.conf.sample> file, entering your details. 
Refer to the L<mis.conf|"Configuration file: mis.conf"> section
for an explanation of the entries in that file.
You have to enter your userid and password for database connections and
supply your home directory in places. In the development environment, the C<MIS>
entry refers to the IQ database on C<dwsdev01>. Save the file as C<$MIS/mis.conf>.

Please note that the initial checkout from svn is the only time your L<mis.conf|"Configuration file: mis.conf"> file should
have anything to do with svn. Hereafter it is manual editing only. L<mis.conf|"Configuration file: mis.conf"> files are
the ONLY environment-specific files in the MIS system, hence need to be 
maintained in each environment, and not committed/updated in svn, other than an initial
checkout to get you up and running.

=item 

Setup the password encrypt/decrypt facility (see below).

=item

To run C<SELECT> SQL, the C<rsql> utility is provided, so you can go

        $ rsql MIS 'select getdate()'

to run SQL against the staging database. This is also an easy way to check that everything
is setup correctly. See the L<Utilities|"Command-line utilities"> section for help on rsql.
The first argument uses entries from the C<DB> block in the C<$MIS/mis.conf> module. 

=item 

To run a script you are editing use the C<Ctrl-p> keyboard shortcut in vim. There are several
local keyboard shortcuts in the /etc/vimrc file. See L<local vim keyboard shortcuts|"VIM">.

=item 

To run C<LOAD TABLE> SQL, use the L<rsql|"rsql"> utility.

=back

=head1 Database object creation and maintenance

The objectives are to:

=over 

=item 

Encourage use of the same datatypes for the same columns
occurring in different tables, such as C<*_SK>and amount columns, by
means of user-defined data types (UDTs).

=item 

Provide a uniform way of creating and maintaining DB objects and the DDL code used 

=back

To this end, C<*.ddl> files in the C<$MIS/ddl> directory are used, one
for each table in the MIS. A C<TYPES> file in this directory is used
to map UDTs to IQ data types. Data types in the individual C<.ddl>
files are UDTs in this file. The C<.ddl> files are all checked into
svn, allowing changes to be tracked.

Searching across the C<.ddl> files and the C<TYPES> file shows the use of a data type
across the database, providing impact analysis of potential changes.

=head1 Subversion (SVN)

SVN is the version control tool that controls changes made to all scripts.  MIS scripts
must be obtained and committed only via SVN, not copied between environments/developers.

=head2 Learning Subversion

A comprehensive guide to Subversion is available L<here|http://svnbook.red-bean.com>

=head2 Getting started with Subversion for MIS

TSD have setup svn permissions using f numbers same as the OS userids. To check
that your svn setup is working, try this:

        $ svn info https://10.33.6.112/svn/MIS

The first time you do this you will be prompted to accept the security certificate. Type
in C<p> to accept it permanently. Login to SVN when prompted with your dwsdev01 password.

You can see a full listing of the trunk branch on the repository with:

        $ svn list -R https://10.33.6.112/svn/MIS/trunk

To checkout the repository into eg C<~/MIS>: 

        $ svn co https://10.33.6.112/svn/MIS/trunk ~/MIS/

Do this to get the current working version of the MIS ETL software. 

To add a script to the repository:

        $ svn add somescript.pl
        $ svn commit -m"some comment" somescript.pl

To commit a change, 

        $ svn commit -m"Changed something" somescript.pl

from the directory that contains your script. To get a history of changes made to anything:

        $ svn log somescript.pl

To see local changes to your script vs the repository:

        $ svn diff somescript.pl

=head2 Branches

The pre-production and production environments of MIS are hosted on C<dwspreprod01> and
C<dwsprod01>. Each environment has its own Subversion branch in the repository, to enable
branch/environment-specific change tracking. For example, the production branch is 
at C<https://10.33.6.112/svn/MIS/branches/prod>. Scripts and modules are migrated between 
the environments by exporting from trunk and committing to the pprod/prod branch.

=head2 Guidelines

=over

=item

Please be careful in running C<svn commit> on all scripts in a directory at the same time. 
It is always a good idea to run C<svn status> beforehand to see what
is different from the repository.

    $ svn status -uv

For the meaning of the status symbols:
    
    $ svn help status

=item

Run C<svn update> from C<$MIS/lib> frequently to ensure you stay in step with module updates.

=item

Commit comments are especially useful if they refer to the version of a spec, or
source-to-target mapping. Do not refer to the name of the script in the comment.

=back

=head2 Adding a subsystem to SVN

Example scenario: new project which will add a distinct subsystem/application (new_app) to MIS.
Separate team of developers will be working on this set of scripts, therefore they may only
want to checkout this subsystem.  Steps to follow:

=over

=item

Create $MIS/new_app and $MIS/new_app/bin directories.

=item 

Add the directory to SVN:

        $ cd $MIS
        $ svn add new_app

=item

Setup the C<stg> and C<log> directories. Its easier to do this after adding the
C<new_app/bin> directory, otherwise they get added to SVN as well.

        $ cd $MIS/new_app
        $ mkdir /data/hosstg/tmp/fxxxxxx/new_app/stg
        $ mkdir /data/hosstg/tmp/fxxxxxx/new_app/log
        $ ln -s /data/hosstg/tmp/fxxxxxx/new_app/log log
        $ ln -s /data/hosstg/tmp/fxxxxxx/new_app/stg stg

=back

=head1 Modules

Documentation related to each module is found in the module. For example
if your C<PERL5LIB> environment variable is setup as above,
 you can find documentation about C<K_Module> like this:

        $ perldoc K_Module

Or use the web server. All documentation for modules under the C<$MIS/lib>
and C<$MIS/web/lib> directories are in the C</perldoc> path eg
C<http://172.16.16.25:3001/perldoc/K_HWE>

=over

=item

L<K_Db|http://172.16.16.25:3001/perldoc/K_Db>      - Routines for DB access

=item

L<K_Rec|http://dws/MIS/ETL/lib/K_Rec.htm>    - How to read and write fixed and variable width files and generate load scripts

=item

L<K_Cf|http://dws/MIS/ETL/lib/K_Cf.htm>     - Contains the environment-specific configuration data setup 

=item

L<K_Dates|http://dws/MIS/ETL/lib/K_Dates.htm>   - Routines for returning standard dates eg max and min DB dates, current date etc.

=item

L<K_Log|http://dws/MIS/ETL/lib/K_Log.htm>   - Write log messages

=back

To see further details of how the module can be used, look at the test script for it. Test scripts 
are in the C<$MIS/lib/t> directory.

=head1 Error checking

You don't have to check for errors when:

=over 

=item  calling C<K_*> subroutines. These are set to explode on error.

=item  calling any C<DBI> subroutines, such as C<fetch>. 

=back

However, you DO have to check for errors on builtins other than C<open> and C<close>.
This means saying C< or die "$!" > when calling eg C<print> and C<printf>.
C<open> and C<close>  are catered for by saying

        use Fatal qw(open close);

The line:

        $SIG{__WARN__} = sub { die $_[0] };

in combination with the C<-w> switch, causes a script to treat Perl warnings as fatal. 

When any of the above does not apply, refer to PBP Chapter 13.

=head1 Help on help

For general "HOWTO" type assistance on Linux, try running:

        $ info coreutils

=head1 Bash help

In addition to the above documentation, each bash command has a manual page, so
for example to get help on the C<ls> command,

    $ man ls

=head1 Perl help

Before asking for help on deciphering a Perl script or generic Perl questions eg 
about modules or functions, try the following:

=over 

=item 

Run 

    $ man perl  

and try to find a manpage that may answer your question. 
Then run the C<man> command on that page. 

=item 

If it is a variable eg C<< $>,$.,$' >>, try the C<perlvar> manpage.

=item 

If you are trying to read a Perl statement and dont know where to start, or
 have difficulty with an operator, try the C<perlop> manpage. It has a table listing 
precedence of the various operators, and describes each operator, including the search and
replace operators.

=item 

If it is a function eg C<eof, unpack, pack, join>, try running C<$ perldoc -f function>.

=item 

If it is a Perl module, try 

    $ perldoc Module

=item 

Otherwise run 

    $ perldoc -q somestring

where C<somestring> is a keyword to search through the Perl FAQ.

=back

=head1 Editors

vim, emacs, ne and joe are all available. Guidelines, whatever editor is used:

=over

=item 

Setup the editor to translate C<TAB> characters to spaces

=item 

Tabstops should be 4 characters

=item 

A skeleton for new scripts is in C</home/f2977834/.vim/skeleton.pl>.

=item 

Use C<perltidy>. See elsewhere in this document for details.

=back 

=head2 VIM

Local keyboard shortcuts in global vimrc file:

=over 

=item 

C<Ctrl-x>: run your script through perltidy

=item 

C<Ctrl-p>: run the script

=item 

C<K>     : switch to alternate buffer

=item 

C<Ctrl-h>: delete current buffer

=item 

C<Ctrl-k>: list of all buffers. Just type in a buffer number and press C<Enter> to jump

=back

A cheat sheet can be found L<here|http://dws/MIS/ETL/Vi_Cheat_Sheet.pdf>. A tutorial
on vim is L<here|http://dws/MIS/ETL/vimbook-OPL.pdf>. A great list of vim tips is 
L<here|http://dws/MIS/ETL/vimtips.html>

=head2 Running SQL in vi

In another Screen window, type 

   $ less -S ~/tmpx.out

To run SQL, mark it visually by line (C<V>) and hit C<\p> to run on production,
C<\d> to run on development. Results will be in the Screen window
where the C<less> command above was run.

=head2 EMACS

The site-wide initialization file is C</usr/share/emacs/site-lisp/site-start.d/dws.el>.

=over 

=item 

C<Ctrl-h> and following instructions for help eg 

=over 

=item 

C<Ctrl-h t> for the EMACS tutorial - just follow the instructions 
on screen

=item 

C<Ctrl-h a> for keyword help

=item 

C<Ctrl-h b> to show list of current keybindings

=item 

C<Ctrl-h k> to show binding of a specific key

=back 

=item 

C<Ctrl-g> to abort something

=item 

C<Ctrl-x Ctrl-c> to quit

=back

=head2 EMACS DWS Customization

       F1 to get a list of buffers - press return on any line to visit that buffer
       F2 to move to previously visited buffer
       F3 list tables for a schema. Put cursor at beginning of a word eg IDMI04
       F4 svn
       F5 to delete other windows
       F6 delete this window
       F7 put point and mark around the current smallest enclosing C< { ( [ > brace.

=head2 Running SQL in EMACS

Press Ctrl space to set the mark at one end of the SQL. Move to the other end. Press F11. 

If the cursor is at the end of a line you should see
the result set in another buffer. Press F2 to return to the buffer with the SQL.

If the cursor is at the beginning of a line the results will be pivoted, easier
for running SQL with few rows and many columns.

If you press C<Ctrl-u 2> before F11 the SQL will be run on C<dwsdev>.
If you press C<Ctrl-u 1> before F11 the SQL will be run on C<dwspreprod>.

=head1 Documenting scripts

The template script C</home/f2977834/.vim/skeleton.pl> is to be used
for all new scripts.  This script contains a documentation section at
the end which is to be completed. When editing a non-existent file in 
vim, this file will automatically be presented for editing.

=head1 Report utility

Create a markup script like the following:

    SQL   <<EOF
    select * from d_country
    EOF
    FORMAT DELIMITED
    HEADER_ROW 1
    <DEST>
        OUTFILE = country.csv
    </DEST>

And name the script C<rpt9999.cf> where C<9999> is the number from 
updating the C<README> file. The C<rpt> utility will run the SQL and output
the result as a delimited file to C<$MIS/stg/country.csv>.


    $ rpt 9999


=head1 Script Standards

This section lists the standards for MIS scripts. Also see L<Naming Standards|"Naming Standards">

=over

=item 

Remove unused variables and modules

=item 

Remove commented out code that is no longer useful

=item

Tabstops should be 4 characters, and be saved by the editor as 
spaces, not the physical C<TAB> character (hex 09).

=item

All code and comments should not be wider than 80 chars. 

=item 

Run perltidy

=item 

Add the documentation at the end of the script, according to the 
template:

    =head1 SYNOPSIS

    xf9999.pl arguments

    =head1 DESCRIPTION

    Functionality of the script

    =head1 INPUT

    Tables, files, that the script reads from

    =head1 OUTPUT

    What the script outputs to

    =head1 SCHEDULING

    1st business day, ...

=item 

Check that the script - as far as possible - follows the same
way of doing things as other scripts, ito sequence of variable
declaration, etc, naming conventions, etc. Remember one of the
virtues of a good programmer: laziness - the reluctance to re-invent
the wheel, rather using a way something has been done before. But then 
also remember to avoid cargo cultism.

=item

If a dimension surrogate key lookup has changed, remember to change the RI logic in
C<ri0017.pl>

=item 

Laziness, as in brevity, is also valued. In Perl,
the creation of short-lived variables can usually be avoided. 
Less code (usually) means less to maintain and easier to understand. 

=item 

The book by Damian Conway, 'Perl Best Practices' (PBP), except for suggestions 
about brace placing, should be followed. In all matters of standards, unless
contradicted by something in this documentation, the advice in that book applies.

=item

svn commit

=item 

Email/Tell someone else on the team - the script now needs a code inspection/review, 
that will evaluate the code against this standard.

=back

=head1 Deployment Standards

=over

=item

Ensure the script is executable. If not, change to executable
and change the svn repository:

    $ chmod +x script.pl
    $ svn propset svn:executable ON srcipt.pl

=item

Ensure the script is committed to svn on the trunk branch, and the steps
in the L<"Script Standards"|"Script Standards"> section are completed.

=item

If modules changes are needed, or you are not sure which module changes 
maybe involved in the deployment follow these steps:

=over

=item

Use the C<ListDependencies> module to check which modules are used, directly
and indirectly by a script:

    $ perl -c -MListDependencies scriptname

To check which differences there are between your DWS modules and modules in production
in the same step, do something like this:

    $ perl -c -MListDependencies $MIS/..../script.pl 2>&1 | grep "K_" | \
        sed 's|::|/|;s|.*|svn diff https://10.33.6.112/svn/MIS/trunk/lib/&.pm \
        https://10.33.6.112/svn/MIS/branches/prod/lib/&.pm|' |bash|less

=item
    
To do a comparison test, checkout the production environment into another 
subdirectory of your home directory:

    $ mkdir ~/EDWP
    $ cd 
    $ svn co https://10.33.6.112/svn/MIS/branches/prod EDWP 
    
Now copy your script to the subdirectory off of C<EDWP>.

Then change your environment to use the new subdirectory:

    $ export MIS=/home/yourfnumber/EDWP
    $ export PERL5LIB=$MIS/lib

(You may want to save these 2 commands into a scriptfile to execute 
every time you want to do deployment testing.)

You also have to copy C<mis.conf> from your C<$MIS> directory to C<~/EDWP>.

Now run the script and check.

=back

=item 

New Database Objects

=over

=item 

Check that DDL for any new tables on IQ are  checked into svn on
C<$MIS/ddl/SCHEMA.NEWTABLE.ddl>

=item

Specify necessary permissions for the tables in the change order

=back

=item 

Prepare and submit change order 

=back 

=head1 Referential Integrity (RI) processing

=head2 Background

The general rule is that data that fails RI checks should cause
a row with the failed extracted data to be inserted into the parent table.

The characteristics of this issue in MIS include:

=over 

=item

There are many combinations of child-parent data that need to be checked.

=item

Where more than one child table needs to be checked against the same parent table,
and checks are not done sequentially, 
there is a risk that if the same data is present in both children and it fails
the RI check, the row may be inserted twice into the parent table. This means
that the RI checks for a given table all need to happen at one point in time, 
with the child tables being checked sequentially against the parent table.

=item 

Over time, developers need to be able to add RI checks as new tables are added, with minimum
effort, minimum impact on existing code and maximum use of existing processing. For example,
if there is a standard lookup and RI insert for a dimension, new tables that can use 
this should be added without repeating the SQL.

=back

=head2 Solution

Refer to C<ri0017.pl>:

The format for each parent table is:
    
    <MIS.PARENT_TABLE>
        SQL = 
        COLS =
        SK = 
        SOURCE_SYSTEM = 
        CHILDREN = 
    </MIS.PARENT_TABLE>

As can be seen from the C<Config::General> documentation, multi-line strings, eg 
for the SQL, need the backslash line-continuation character.  

=over

=item SQL

This is the SQL that will be used to add failed RI check rows to the parent table.
Usually this will be a left outer join between the child and the parent, where the 
values for the parent table are C<NULL>. See existing examples in the config file.

=item COLS 

A list of the columns with the key columns and default values for the other columns
that will be added to the  parent table. 'Special' values are indicated using 
field names enclosed by square brackets. These values are substituted in the script
and include C<MAX_SK>, that will be substituted by the current maximum values of the 
surrogate key column of the parent table, and C<ROW_...> names, that will be 
substituted by the fields in the DBI fetch loop.

Values hardcoded in the list are trimmed of leading whitespace before being used
in the C<INSERT> statement, ie C<, N > in the list will be inserted as C<N>, no
leading space.

=item SK

The name of the surrogate key column of the parent   table.

=item SOURCE_SYSTEM

The value that will be used for the source system operational key column.

=item CHILDREN

The child tables that will be checked against the parent.

=back

C<ri0017.pl> uses these tags to run the SQL provided and load the RI rows (if 
needed) into the parent table. In each case a C<.txt> and C<.sql> pair of files
are created, as with normal loads into tables. The names of these files are 
formed by combining the parent-child combination eg 
C<MIS.TD_PRODUCT_RI_STG.TF_ACCT_MNTHLY_SS_DDA.txt>. 

In many cases, child tables will have specific values for one or more of these 
fields. They may override the default values eg 

    <MIS.PARENT_TABLE>
        SQL = somesql
        COLS = somecol1,....
        CHILDREN = other_tab1, other_tab2,... 
        <CHILD>
            TABLE_NAME = 
            SK = SK_COL
            SOURCE_SYSTEM = 
        </CHILD>
    </MIS.PARENT_TABLE>

In these cases, the C<< <CHILD> >> value is taken if it exists, otherwise
the value is taken from the main field. As can be seen from 
the above example, C<< <CHILDREN> >>
and specific cases can co-exist.

The procedure for adding a new parent and/or child is firstly to 
add the necessary lines
to the L<mis.conf|"Configuration file: mis.conf"> file in the developer's own environment and test, 
and then to put the request for adding the lines in production to 
a change order.

=head1 Coding Layout

Scripts must be run through C<perltidy> before being committed. There is 
a site-wide configuration for perltidy, which ensures that all scripts
get the same layout. The procedure for running C<perltidy> is 
described for vim above. In Emacs, this can be done by the key 
sequence: C< C-x h Esc 1 Esc | perltidy >, or you can bind this to a 
key for ease of use.

=head1 Tags

In the $MIS/util directory is a script called C<pltags.pl>. Run this script 
from your $MIS directory:

        $ $MIS/util/pltags.pl $MIS/lib/*.pm

This will create a file called C<tags>. Then edit your C<~/.vimrc> file, adding
the line:

        se tags=/home/yourfnumber/MIS/tags

Now, in vim, you can jump to module subroutines with the C<Ctrl-]> key and back
with the C<Ctrl-t> key. If eg your cursor is over the C<K_Db::Connect> subroutine
in a script and you want to check the parameters to that subroutine, 
go C<Ctrl-]> to jump to the subroutine definition.

=head1 Command-line utilities

These are all in C<$MIS/util>, so that directory needs to be in the current shell
path.

=over

=item Syntax convention

In arguments, C<{}> indicates mandatory arguments, C<[]> optional arguments. C<|>
indicates alternative options.

=item rsql

Runs C<SELECT> statements against whatever databases are setup in the C<$MIS/mis.conf>
file, either from SQL specified on the command line, or in a SQL script. Usage:

    $ rsql {DB connection in mis.conf} [-t tab delimited] [-x print row of '-----' at end]
    {'select statement'|-f file.sql}

    eg

    $ rsql HOS 'select 1'

    $ rsql MIS -f myscript.sql

If running a script, SQL statements will successively be run separated by ';' 
characters.  A line consisting only of the string C<__END__> will cause C<rsql> to 
ignore everything in the scriptfile after that line. Lines after a line consisting of C< <<< >
and ending with a line consisting of C< E<gt>E<gt>E<gt> > will cause C<rsql> to only read SQL 
between those lines.

=item iqld

This utility will load a table with several options. Usage:

     $ iqld {-t Table to load} {-f path to the file to load from} 

Further options:

=over 

=item -a   will cause the table to be appended to, otherwise it will 
be truncated before loading

=item -v   variable width input file

=item -s   indicates the separation character, eg
      
      $ iqld -t sometable -f /path/to/dir/loadfile.txt -s,

will use comma as the separation character

=item -e  indicates that there is a header row in the load file that is to be 
ignored

=item -c   run the load into IQ table straight away. If this option is not given,
the utility will produce a C<.sql> file that can be used as the load script eg
C<IDMI01.MI_TABLE_T.sql>. 

=item -m  This option indicates that the load file is fixed width and that the layout
of the file is given by a C<.pm> file in the C<$MIS/lib/K_Rec> directory eg

     $ iqld -t sometable -f /path/to/dir/loadfile.txt -f Nca

will use the layout in the C<$MIS/lib/K_Rec/Nca.pm> module. If more than one layout
the C<det> layout will be used.

=back

=item disprec

Displays a text file created with the L<K_Rec|http://dws/MIS/ETL/lib/K_Rec.htm> module. Usage:

        $ cat textfile.txt | disprec {-t Table|-f Format [det|hdr|trlr]}

An example using the format option:

        $ head -2 ncafile.txt | disprec -f Nca det

This uses C<$MIS/lib/K_Rec/Nca.pm> and refers to the  detail record layout
in that module.

An example using the table option:

        $ head -2 TK_DIM.txt | disprec -t TK_DIM

=item crtab

Creates a table or view in C<MIS> database using a C<.ddl> file in C<$MIS/ddl>. eg

        $ crtab STG.HOS_CUSTOMER
        
Just show the ddl as represented by the C<*.ddl> files:
    
        $ crtab -n STG.HOS_CUSTOMER

Prompt before running the SQL:

        $ crtab -p STG.HOS_CUSTOMER

Verbose:

        $ crtab  -v STG.HOS_CUSTOMER

=item pvt

Pivots standard input, by default C<|> delimited. It is used with C<rsql>
to display a limited number of long rows vertically, eg 

        $ rsql MIS 'SELECT * FROM IDMI01.MI_CUSTOMER_T WHERE 
                CUST_NO=1234' | pvt

Can also pivot output  delimited by other characters by supplying delimiter as argument eg:

        $ command that emits , as delimiter  | pvt ,

or 

        $ command that emits ; as delimiter  | pvt \;


=item cmb 

Filter that combines columns from one row each of  two tables, displaying results side-by-side.
Assumes standard input is from rows produced by rsql

        $ (rsql MIS 'select top 1 *  from a';rsql MIS 'select top 1 * from b')| cmb

=item refromprod

Refreshes tables in dwsdev from production IQ:

        $ refromprod -c IDMI01.MI_CUSTOMER_T

Uses the following entries in L<mis.conf|"Configuration file: mis.conf">:

        DB HOS - must point at production IQ database
        EXT_IP, EXT_USER, EXT_PWD - entries under C<HOS> for ftp purposes

The C<-c> argument copies the table according to the Sybase catalog, otherwise copies 
the HOS load file from C</data/hosstg> in production to dwsdev and loads.

=item archv

Archives and gzips a file into the C<$MIS/archv> directory,  one gzip file per
business month. Successive calls will create additional files in the gzip file
for the business month:

    $ archv -f /full/path/to/extract/file

=item archld

Archives load scripts and files together, creating successive generations 
of tar gzipped files. For example the most recent load file and script
for C<MIS.TK_ACCOUNT_DDA> in C<$MIS/stg>:

    $MIS/stg/MIS.TD_ACCOUNT_DDA.txt
    $MIS/stg/MIS.TD_ACCOUNT_DDA.sql

    $ archld -t MIS.TD_ACCOUNT_DDA
    
will create C<$MIS/stg/MIS.TD_ACCOUNT_DDA.tgz.1>. After the next load, running 
the same command will:

=over

=item move C<$MIS/stg/MIS.TD_ACCOUNT_DDA.tgz.1> to C<$MIS/stg/MIS.TD_ACCOUNT_DDA.tgz.2>

=item create C<$MIS/stg/MIS.TD_ACCOUNT_DDA.tgz.1>  from the existing load file and script

=back


=item ldchk

For HOS backward compatability, this script checks a load file as it would be read
by a SQL load script and displays the output as IQ would see it:

    $ cat loadfile | ldchk -s /path/to/sqlscript 

Example:

    $ cat /data/hosstg/IDMI04.MI_D_PAYPAL_HISTORY_T.TXT | ldchk -s /data/hosstg/sql/IDMI04.MI_D_PAYPAL_HISTORY_T.sql |less

It is useful for testing HOS load scripts, not used with MIS environment as C<disprec> is
all that is needed.

=item makedwsdocs

        $ makedwsdocs

This is how to maintain this documentation: Edit the source in C<$MIS/doc/src>.
Then run C<makedwsdocs>.  It takes the C<.pod> source in C<$MIS/doc/src>, compiles it into C<*.htm> files
and copies to the C<dws> web server. Enter your fnbjnb01 username and password when
prompted.

=back

=head1 Configuration file: mis.conf

Divided into markup sections similar to the syntax of the apache C<.conf>
files. Unfortunately it cannot be commented because of the implementation
of password encryption, which rewrites the file when the L<crpt|"Password encryption"> utility
is run.

The file is divided into sections corresponding to different application
uses. Any entry called PWD is meant to be encrypted using the L<crpt|"Password encryption"> utility.
Documentation for each section follows:

=head2 DB

This section contains a tag pair for each database connection that is used in
scripts. Each connection typically has a DSN value, which is used by
Perl DBI, and  PWD and USER values. The DBD::Sybase driver does not
make provision for symbolic data type values, so each IQ connection
has a set of values describing data types, which is used for certain
formatting functionality, mainly in the rsql utility, which is not used
in production.

=over

=item DSN - the Perl DBI data source string used to connect to the database

=item USER - database userid

=item PWD  - encrypted password, entered with the C<crpt> utility.

=item EXT_* - settings for IQ batch extract purposes. Not currently used in
production.

=back

Currently, the database connections are:

=over

=item HOS - connection to the HOS database

=item MIS - connection to the MIS database

=back

=head2 FTP

Contains subsections, with IP-address, userid and password entries for each ftp connection

=head2 GEN

Miscelaneous options, currently:

=over

=item DEBUG - Can be used by scripts to print debug information

=item ENV  - Set to C<dev> if in the development environment. This is used by the
C<K_Ext::iq_select_into_table> routine to batch extract, ftp and load from production to
development. Otherwise just inserts from one table to another. 

=back

=head1 Changing this documentation

Edit the pod source in C<$MIS/lib/Doc/MIS.pm>. Remember to commit changes to svn.

That is all! The documentation is picked up and translated to html by the web
server using the URL: http://ipaddress:port/perldoc/Doc/MIS.

=cut

=for vim:tw=90:sw=4:ts=4:ft=help:norl:
