package K_Rec;
use strict;
use warnings;
use DBI;
use Carp;
use K_Db;
use K_Cf;

sub new
{
    my $class = shift;
    my $args  = shift;
    my $self  = {};

    my ( $rec_len, @cols, @ofmts, @ifmts, @vlds, @lds, %row, @tfmts );
    if ( $args->{fmt} )
    {
        @cols  = $args->{fmt} =~ /^\s*(\w+)/gms;
        @ofmts = $args->{fmt} =~ /:(.*?)$/gms;
        @lds   = map {
            /%/ ? "ASCII(" . (/%[\D]*(\d*)/)[0] . ")" : "ASCII(" . length(
                do { $_ =~ s/"//g; $_ }
                ) . ")"
        } @ofmts;

        #---------------------------
        # FILLER does not have ASCII
        for my $i ( 0 .. $#cols )
        {
            $rec_len += ( $lds[$i] =~ m/\((\d+)\)/ )[0];
            if ( $cols[$i] eq 'FILLER' )
            {
                $lds[$i] =~ s/ASCII//;
            }
            if ( $cols[$i] && $args->{cust} )
            {
                if ( $args->{cust}->{$cols[$i]} )
                {
                    $lds[$i] = $args->{cust}->{$cols[$i]};
                }
            }
        }
        @ifmts = map {
            /%/ ? "A" . (/%[\D]*(\d*)/)[0] : "A" . length(
                do { $_ =~ s/"//g; $_ }
                )
        } @ofmts;
    }
    else
    {    # No format, so using a table DDL
        croak "db not specified" unless $args->{db} || $args->{dbh};
        my $dbh = K_Db::connect( $args->{db} );
        my ( $user, $table );
        if ( $args->{table} =~ /\./ )
        {
            ( $user, $table ) = split( /\./, $args->{table} );
        }
        else
        {
            $user = $args->{user} || $K_Cf::CFG{DB}{$args->{db}}{USER};
            $table = $args->{table};
        }

        my $sth = K_Db::run(
            $dbh, \%row, qq{
		SELECT c.user_name,b.table_name,a.column_name,d.domain_name,a.width,a.scale,a.nulls 
		FROM sys.syscolumn a, sys.systable b, sysuserperms c,sysdomain d 
		WHERE a.domain_id=d.domain_id 
		AND a.table_id=b.table_id 
		AND b.creator=c.user_id 
		AND b.table_name='} . $table . qq{' 
		AND c.user_name='} . $user . qq{' ORDER BY a.column_id }
        );

        my $time_ar;
        if ( $K_Cf::CFG{GEN}{ENV} eq 'dev' )
        {
            $time_ar = [
                "%12.12s", "ASCII(12) NULL('            ','NULL        ')",
                "A12", 12
            ];
        }
        else
        {
            $time_ar = [
                "%15.15s", "ASCII(15) NULL('            ','NULL        ')",
                "A15", 15
            ];
        }
        my %type_fmt = (
            "date" => [
                "%10.10s",
"ASCII(10) NULL('          ','NULL      ','      NULL','0000-00-00')",
                "A10",
                10
            ],
            "time"      => $time_ar,
            "timestamp" => ["%26.26s", "ASCII(26)", "A26", 26],
            "smallint" =>
                ["% 6d", "ASCII(6) NULL('      ','NULL  ')", "A6", 6],
            "integer" => [
                "% 11d", "ASCII(11) NULL('           ','NULL       ')",
                "A11", 11
            ],
            "unsigned integer" => [
                "% 11d", "ASCII(11) NULL('           ','NULL       ')",
                "A11", 11
            ],
            "bigint" => [
                "%  20d", "ASCII(20) NULL('           ','NULL       ')",
                "A20", 20
            ],
            "unsigned bigint" => [
                "% 21d", "ASCII(21) NULL('           ','NULL       ')",
                "A21", 21
            ],
            "bit" => ["% 4d", "ASCII(4) NULL('    ','NULL       ')", "A4", 4]
        );

        while ( $sth->fetch )
        {
            $row{domain_name} =~ s/\bint\b/integer/;
            push @cols, $row{column_name};
            if (   ( $row{domain_name} eq "decimal" )
                || ( $row{domain_name} eq "numeric" ) )
            {
                my $width;
                if ( $K_Cf::CFG{GEN}{ENV} eq 'dev' )
                {
                    $width = $row{width} >= 2 ? $row{width} : 2;
                }
                else
                {
                    $width = $row{width} >= 2 ? $row{width} : 4;
                }
                push @ofmts, "% " . ( $width + 2 ) . "." . $row{scale} . "f";
                push @ifmts, "A" .  ( $width + 2 );
                $rec_len += $width + 2;
                my $sps = ' ' x ( $width - 2 );
                push @lds,
                    "ASCII(" . ( $width + 2 ) . ") NULL('NULL" . $sps . "')";
            }
            elsif ( grep ( /char|binary/, $row{domain_name} ) )
            {
                my $width = $row{width} >= 4 ? $row{width} : 4;
                push @ofmts, "%-" . $width . "." . $width . "s";
                push @ifmts, "A" . $width;
                $rec_len += $width;
                push @lds, "ASCII(" . $width . ")";
            }
            else
            {
                if ( !$type_fmt{$row{domain_name}} )
                {
                    croak "Cannot find IQ type!:"
                        . $row{domain_name}
                        . " for $user.$table";
                }
                push @ofmts, $type_fmt{$row{domain_name}}[0];
                push @lds,   $type_fmt{$row{domain_name}}[1];
                push @ifmts, $type_fmt{$row{domain_name}}[2];
                $rec_len += $type_fmt{$row{domain_name}}[3];
            }
            push @tfmts, $row{domain_name};
        }
        croak "Cannot find DB catalog info for $user,$table" unless $rec_len;
    }

    $self->{cols}    = \@cols;
    $self->{ofmts}   = \@ofmts;
    $self->{ifmts}   = \@ifmts;
    $self->{tfmts}   = \@tfmts;
    $self->{ofmt}    = join( '', @ofmts, "\n" );
    $self->{ifmt}    = join( '', @ifmts, "\n" );
    $self->{rec_len} = $rec_len + 1;
    if ( $args->{var} )
    {
        $self->{var}                   = 1;
        $self->{missing_eol_delimiter} = $args->{missing_eol_delimiter} || 0;
        $self->{header}                = $args->{header} || 0;
        $self->{sep}                   = $args->{sep}
            or croak "Variable length input specified but no field separator";
    }
    for my $i ( 0 .. $#cols )
    {
        my $var_sep = "";
        if ( $args->{var} )
        {
            if ( $args->{missing_eol_delimiter} )
            {
                if ( $i < $#cols )
                {
                    $var_sep =
                          "\t\x27"
                        . sprintf( "\x5cx%s", unpack( "H*", $args->{sep} ) )
                        . "\x27";
                }
                else
                {
                    if ( $args->{cr} )
                    {
                        $var_sep = "\t\x27\\x0d\x27";
                    }
                    else
                    {
                        $var_sep = "\t\x27\\x0a\x27";
                    }
                }
            }
        }
        $self->{ldsql} .=
            $cols[$i] . " " . ( $args->{var} ? $var_sep : $lds[$i] ) . ",\n";
    }
    bless $self, $class;
    return $self;
}

sub cols
{
    my $args = shift;
    my $dbh  = K_Db::connect( $args->{db} );
    my ( $user, $table );
    ( $user, $table ) = split( /\./, $args->{table} );
    return map { $_->[0] } @{
        $dbh->selectall_arrayref(
            qq{
        SELECT a.column_name
        FROM sys.syscolumn a, sys.systable b, sysuserperms c
        WHERE a.table_id=b.table_id 
        AND b.creator=c.user_id 
        AND b.table_name='} . $table . qq{' 
        AND c.user_name='} . $user . qq{' ORDER BY a.column_id }
        )
    };

}

sub gen_ddl
{
    my $self = shift;
    my $args = shift;

    my ( @cols, %row, @tfmts, $ddl );

    croak "db not specified" unless $args->{db} || $args->{dbh};
    my $dbh = K_Db::connect( $args->{db} );
    my ( $user, $table );
    if ( $args->{table} =~ /\./ )
    {
        ( $user, $table ) = split( /\./, $args->{table} );
    }
    else
    {
        $user = $args->{user} || $K_Cf::CFG{DB}{$args->{db}}{USER};
        $table = $args->{table};
    }

    my $sth = K_Db::run(
        $dbh, \%row, qq{
    SELECT c.user_name,b.table_name,a.column_name,upper(d.domain_name) domain_name,
        a.width,a.scale,a.nulls 
    FROM sys.syscolumn a, sys.systable b, sysuserperms c,sysdomain d 
    WHERE a.domain_id=d.domain_id 
    AND a.table_id=b.table_id 
    AND b.creator=c.user_id 
    AND b.table_name='} . $table . qq{' 
    AND c.user_name='} . $user . qq{' ORDER BY a.column_id }
    );

    $ddl = "CREATE TABLE $user.$table (";
    my $notfirstcol;
    while ( $sth->fetch )
    {
        if ($notfirstcol) { $ddl .= "," }
        else              { $notfirstcol = 1 }
        $ddl .=
            sprintf( "\n%-40.40s%s", $row{column_name}, $row{domain_name} );
        if ( $row{width}
            && !
            grep( /(DATE|TIMESTAMP|SMALLINT|INTEGER)/, $row{domain_name} ) )
        {
            $ddl .= "(" . $row{width};
            if ( $row{scale} )
            {
                $ddl .= "," . $row{scale};
            }
            $ddl .= ")";
        }
        if ( $row{nulls} eq 'Y' )
        {
            $ddl .= " NULL";
        }
    }
    $ddl .= ")\n";
    $self->{ddl} = $ddl;
}

sub disp
{
    my ($self) = @_;
    my $inrec;
    my $fh = \*STDIN;

    while (1)
    {
        last unless read $fh, $inrec, $self->{rec_len};
        my @inflds = unpack $self->{ifmt}, $inrec;
        for my $i ( 0 .. scalar @{$self->{cols}} - 1 )
        {
            printf "%-30.30s:", $self->{cols}[$i] or croak $!;
            printf $self->{ofmts}[$i], $inflds[$i] or croak $!;
            print ":\n" or croak $!;
        }
        print "-------------\n" or croak $!;
    }
}

1;
__END__

=head1 NAME

K_Rec - Class for specifying record layout of text files and load scripts
based on IQ table or manually specified layout spec.

=head1 SYNOPSIS

=head2 SELECT into fixed width flat file and load table

	use K_Rec;

	my $rec = K_Rec->new(
		{
			db   => 'DB_ENTRY',
			table => "SOME_TABLE",
		}
	);

	my $ofmt = $rec->{ofmt};

Get data into @fields (see K_Db)

	print $some_fh $ofmt,@fields;

Display the record length of the file:

	print $rec->{rec_len};

Get a list of the columns:

	print @{$rec->{cols}};

Later...

	K_Db::write_load(
		{
			table    => "IQ_TABLE",
			rec    => $rec
		}
	);

Or read the file back in again:

	while (read $fh, $inrec, $rec->{rec_len}){
		my @inflds = unpack $rec->{ifmt}, $inrec;
	}

Look at the file using the specified format:

	$ cat somefile | $MIS/util/disprec.pl -t IQ_TABLE

See doc/samples/ext_dim.pl or $MIS/util/disprec.pl for an example.

=head2 Writing to a fixed width flat file

Create $MIS/lib/K_Rec/SomeApp.pm with subroutine that returns a format. For 
an example see $MIS/lib/K_Rec/Nca.pm. Then using it:

	my $some_app=K_Rec->new(fmt=>K_Rec::SomeApp::get_layout());

	print $some_fh $rec->{ofmt},@fields;

The resulting file can also be displayed using the C<$MIS/util/disprec.pl> utility:

	$ cat somefile | $MIS/util/disprec.pl -f SomeApp

To read the file same as the example using unpack above.

=head2 Format strings for fixed width files

Lines in the format string are of the form

	<Field_mnemonic>		:<Format_String>

where the field mnemonic is an easy-to-remember placeholder for the
field in the output, and the format string is either a L<sprintf> format string, 
or which can be passed on verbatim to be included in a L<sprintf> format string. 
Verbatim strings are optionally enclosed by double quotes, especially where
they are spaces.

=head1 NOTES

To load directly from a delimited file into a table see C<D_Csv>.

=cut
