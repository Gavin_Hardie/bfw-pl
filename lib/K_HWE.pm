package K_HWE;
use v5.010;
use strict;
use warnings;

use K_Cf;
use Data::Dumper;

our %allhr;

# List PG keywords  to eliminate later on as column names
#
my @pgkeywords = (
    'abort',             'absolute',
    'access',            'action',
    'add',               'admin',
    'after',             'aggregate',
    'all',               'also',
    'alter',             'always',
    'analyse',           'analyze',
    'and',               'any',
    'array',             'as',
    'asc',               'assertion',
    'assignment',        'asymmetric',
    'at',                'authorization',
    'backward',          'before',
    'begin',             'between',
    'bigint',            'binary',
    'bit',               'boolean',
    'both',              'by',
    'cache',             'called',
    'cascade',           'cascaded',
    'case',              'cast',
    'catalog',           'chain',
    'char',              'character',
    'characteristics',   'check',
    'checkpoint',        'class',
    'close',             'cluster',
    'coalesce',          'collate',
    'column',            'comment',
    'commit',            'committed',
    'concurrently',      'configuration',
    'connection',        'constraint',
    'constraints',       'content',
    'continue',          'conversion',
    'copy',              'cost',
    'create',            'createdb',
    'createrole',        'createuser',
    'cross',             'csv',
    'current',           'current_catalog',
    'current_date',      'current_role',
    'current_schema',    'current_time',
    'current_timestamp', 'current_user',
    'cursor',            'cycle',
    'data',              'database',
    'day',               'deallocate',
    'dec',               'decimal',
    'declare',           'default',
    'defaults',          'deferrable',
    'deferred',          'definer',
    'delete',            'delimiter',
    'delimiters',        'desc',
    'dictionary',        'disable',
    'discard',           'distinct',
    'do',                'document',
    'domain',            'double',
    'drop',              'each',
    'else',              'enable',
    'encoding',          'encrypted',
    'end',               'enum',
    'escape',            'except',
    'excluding',         'exclusive',
    'execute',           'exists',
    'explain',           'external',
    'extract',           'false',
    'family',            'fetch',
    'first',             'float',
    'following',         'for',
    'force',             'foreign',
    'forward',           'freeze',
    'from',              'full',
    'function',          'global',
    'grant',             'granted',
    'greatest',          'group',
    'handler',           'having',
    'header',            'hold',
    'hour',              'identity',
    'if',                'ilike',
    'immediate',         'immutable',
    'implicit',          'in',
    'including',         'increment',
    'index',             'indexes',
    'inherit',           'inherits',
    'initially',         'inner',
    'inout',             'input',
    'insensitive',       'insert',
    'instead',           'int',
    'integer',           'intersect',
    'interval',          'into',
    'invoker',           'is',
    'isnull',            'isolation',
    'join',              'key',
    'lancompiler',       'language',
    'large',             'last',
    'lc_collate',        'lc_ctype',
    'leading',           'least',
    'left',              'level',
    'like',              'limit',
    'listen',            'load',
    'local',             'localtime',
    'localtimestamp',    'location',
    'lock',              'login',
    'mapping',           'match',
    'maxvalue',          'minute',
    'minvalue',          'mode',
    'month',             'move',
    'name',              'names',
    'national',          'natural',
    'nchar',             'new',
    'next',              'no',
    'nocreatedb',        'nocreaterole',
    'nocreateuser',      'noinherit',
    'nologin',           'none',
    'nosuperuser',       'not',
    'nothing',           'notify',
    'notnull',           'nowait',
    'null',              'nullif',
    'nulls',             'numeric',
    'object',            'of',
    'off',               'offset',
    'oids',              'old',
    'on',                'only',
    'operator',          'option',
    'options',           'or',
    'order',             'out',
    'outer',             'over',
    'overlaps',          'overlay',
    'owned',             'owner',
    'parser',            'partial',
    'partition',         'password',
    'placing',           'plans',
    'position',          'preceding',
    'precision',         'prepare',
    'prepared',          'preserve',
    'primary',           'prior',
    'privileges',        'procedural',
    'procedure',         'quote',
    'range',             'read',
    'real',              'reassign',
    'recheck',           'recursive',
    'references',        'reindex',
    'relative',          'release',
    'rename',            'repeatable',
    'replace',           'replica',
    'reset',             'restart',
    'restrict',          'returning',
    'returns',           'revoke',
    'right',             'role',
    'rollback',          'row',
    'rows',              'rule',
    'savepoint',         'schema',
    'scroll',            'search',
    'second',            'security',
    'select',            'sequence',
    'serializable',      'server',
    'session',           'session_user',
    'set',               'setof',
    'share',             'show',
    'similar',           'simple',
    'smallint',          'some',
    'stable',            'standalone',
    'start',             'statement',
    'statistics',        'stdin',
    'stdout',            'storage',
    'strict',            'strip',
    'substring',         'superuser',
    'symmetric',         'sysid',
    'system',            'table',
    'tablespace',        'temp',
    'template',          'temporary',
    'text',              'then',
    'time',              'timestamp',
    'to',                'trailing',
    'transaction',       'treat',
    'trigger',           'trim',
    'true',              'truncate',
    'trusted',           'type',
    'unbounded',         'uncommitted',
    'unencrypted',       'union',
    'unique',            'unknown',
    'unlisten',          'until',
    'update',            'user',
    'using',             'vacuum',
    'valid',             'validator',
    'value',             'values',
    'varchar',           'variadic',
    'varying',           'verbose',
    'version',           'view',
    'volatile',          'when',
    'where',             'whitespace',
    'window',            'with',
    'without',           'work',
    'wrapper',           'write',
    'xml',               'xmlattributes',
    'xmlconcat',         'xmlelement',
    'xmlforest',         'xmlparse',
    'xmlpi',             'xmlroot',
    'xmlserialize',      'year',
    'yes',               'zone'
);

# Connect table in hwe_layout.txt file, filename in 
#  /home/osshare/Exports and key column
#
our %hwe_nm = (
    AGCommissionVc    => { fn => 'Comm',         key => 'SerNr' },
    ERVc              => { fn => 'EXRate',       key => 'SerNr' },
    IVVc              => { fn => 'SInv',         key => 'SerNr' },
    INVc              => { fn => 'Items',        key => 'UUID' },
    IPVc              => { fn => 'Receipts',     key => 'SerNr' },
    AGStdCostVPCBlock => { fn => 'StdCostCart',  key => 'SerNr' },
    RecVc             => { fn => 'Recipes',      key => 'Code' },
    VIVc              => { fn => 'PInv',         key => 'SerNr' },
    ORVc              => { fn => 'Orders',       key => 'SerNr' },
    'CUVc'            => { fn => 'Contacts',     key => 'UUID' },
    'HWE Ports'       => { fn => 'PofD',         key => 'SerNr' },
    'ShipDealVc'      => { fn => 'DelTerms',     key => 'Code' },
    'PDVc'            => { fn => 'PayTerms',     key => 'UUID' },
    'AGValPerCart1Vc' => { fn => 'VPC',          key => 'SerNr' },
    'StockTakeVc'     => { fn => 'StockTakeVc',  key => 'SerNr' },
    'IPrsVc'          => { fn => 'IPrsVc',       key => 'TransNr' },
    'AGStdWCartVc'    => { fn => 'AGStdWCartVc', key => 'ArtCode' },
    'TRVc'	          => { fn => 'TRVc', 	     key => [ qw(Number IntYc) ]},
    'ObjVc'           => { fn => 'ObjVc',        key => 'Code' },
    'ObjBalVc'        => { fn => 'ObjBalVc',     key => 'AccNumber' },
    'AccVc'           => { fn => 'AccVc',        key => 'AccNumber' },
    'Bud1Vc'           => { fn => 'Bud1Vc',        key => [ qw(AccNumber Objects) ] },
);

# Translate HWE types to PG
#
our %pg_type_of = (
    'M4Int'          => 'bigint',
    'M4Val'          => 'decimal(14,4)',
    'M423Val'        => 'decimal(14,4)',
    'M4UVal'         => 'decimal(14,4)',
    'M45Val'         => 'decimal(16,4)',
    'ExtraProdQty'   => 'decimal(16,4)',
    'UUID'           => 'character varying',
    'M4List'         => 'character varying',
    'RetainPrc'      => 'character varying',
    'M41Val'         => 'decimal(14,3)',
    'M4Qty'          => 'decimal(14,4)',
    'M4NegVal'       => 'decimal(14,4)',
    'M4Rate'         => 'decimal(14,6)',
    'M4Long'         => 'bigint',
    'M4UStr'         => 'character varying',
    'M4Dummy'        => 'character varying',
    'M4PackedMatrix' => 'character varying',
    'M4Set'          => 'character varying',
    'M4Mark'         => 'smallint',
    'M4Str'          => 'character varying',
    'DefMarkup'          => 'character varying',
    'M4Code'         => 'character varying',
    'M4Date'         => 'character varying',
    'M4Time'         => 'time without time zone'
);

sub get_ddl
{
    my ($self)     = @_;
    my $hdr_tab_nm = $self->{hdr_table};
    my $str        = "DROP TABLE IF EXISTS $hdr_tab_nm ;";
    $str .= "CREATE TABLE  $hdr_tab_nm  ( ";
    for my $hdr ( @{ $self->{hdr} } )
    {
        $str .= $hdr->[0] . " " . $hdr->[1] . ",\n";
    }
    $str =~ s/,\s*\z/);/ms;
    $str .= qq{GRANT SELECT ON } . $self->{hdr_table} . qq{ TO public;};
    if ( $self->{mat_fld_cnt} )
    {
        my $mat_tab_nm = $self->{mat_table};
        $str .= ";DROP TABLE IF EXISTS $mat_tab_nm ;";
        $str .= "CREATE TABLE  $mat_tab_nm ( ";

        for my $fk (@{$self->{mat_fks}})
        {
            $str .=
                'm_' . $$fk[0] . " " . $$fk[1] . ",\n";
        }
        for my $fld ( @{ $self->{matrix} } )
        {
            $str .= $fld->[0] . " " . $fld->[1] . ",\n";
        }
        $str =~ s/,\s*\z/);/ms;
        $str .= qq{GRANT SELECT ON } . $self->{mat_table} . qq{ TO public;};
    }
    return $str;
}

sub write_loadfile
{
    my ( $self, $exp_fl ) = @_;

    my $hwefn;
    if ( $self->{hdr_table} eq 'hwe.IPrsVc' )
    {
        $hwefn = $ENV{MIS} . '/imp/IPrsVc.TXT';
    }
    else
    {
        my $ls =
              qq{ls -t $K_Cf::CFG{FILE}{HWE_EXPORTS}/}
            . $self->{filename}
            . '*.txt | head -1';
        chomp $ls;
        $ENV{HWE_DEBUG} && print $ls, "\n";
        $hwefn = $exp_fl || qx/$ls/;
    }

    $self->{hwefn}=$hwefn;
    chomp $self->{hwefn};
    $self->{hwe_file_size}= -s $self->{hwefn};
    my $inv_recs = 0;
    my $chk_inv  = sub {
        my ( $row_cnt, $tabs, $fld_cnt, $row ) = @_;
        if ( $inv_recs )
        {
            #print "At row $row_cnt:Tabs found:$tabs, Fields in header:"
            #    . $fld_cnt
            #    . " - skipping row:\n$row<--END OF ROW\n";
        }
        $inv_recs++;
        die "Too many invalid records, quiting..."
            if $inv_recs > 10;
    };
    $ENV{HWE_DEBUG} && print $hwefn;
    chomp $hwefn;
    open my $inv_fh, " < ", $hwefn or die "$! ";
    open my $hdr_fh, " > ", "$ENV{MIS}/stg/" . $self->{hdr_table} . ".txt"
        or die "$! ";

    my $row_cnt;

    if ( $self->{mat_fld_cnt} )
    {
        open my $mat_fh, " > ", "$ENV{MIS}/stg/" . $self->{mat_table} . ".txt"
            or die "$! ";
        local $/ = "\x0d\x0a\x0d\x0a";
        no warnings "uninitialized";
        
        while ( my $hdr_trlr = <$inv_fh> )
        {
            my (@ht);
            
            if ($hdr_trlr =~m/\$%\^!:/) {
                # say 'B4:',$hdr_trlr;
                $hdr_trlr=~s/\t\x0d\x0a\$%\^!:\t[^\t]*//msg;
                # say 'Af:',$hdr_trlr;
            }
            if ($self->{table} eq 'INVc')
            {
                @ht = split /\x0d\x0a\x0d\x0a/, $hdr_trlr;
            }
            else
            {
                @ht = split /\x0d\x0a/, $hdr_trlr;
            }
            $row_cnt++;

            $ENV{HWE_DEBUG}
                && print substr( $ht[0], 0, 10 ) . ":"
                . ( $ht[0] =~ tr/\t// ) . "\n";
            my $tabs = () = $ht[0] =~ m/(\t)/g;

            if ( $tabs + 1 < $self->{hdr_fld_cnt} )
            {
                &$chk_inv( $row_cnt, $tabs, $self->{hdr_fld_cnt}, $hdr_trlr );
                next;
            }

            # Some tabs have odd tabs (does not 
            # match up with metadata
#            if ( $self->{table} eq 'IVVc' )
#            {
#                $ht[0] =~ s/((.*?\t){163})\t/$1/;
#            }
            # if ( $self->{table} eq 'VIVc' )
            # {
            #     $ht[0] =~ s/((.*?\t){77})\t/$1/;
            # }

            # printf STDERR "Too few tabs:$ht[0]\n" and next
            my @key_flds;
            #----------------------------------------------
            # Grab foreign key column values from the header
            # Using the column number saved in mat_fks[n][2]
            for my $ky ( @{$self->{mat_fks}})
            {
                my $fldno=$$ky[2];
                if ($fldno ==0)
                {
                    push @key_flds, ($ht[0] =~ m/^([^\t]*)\t/);
                } else
                {
                    # Have to count that number of tabs 
                    # into the row
                    my $re=qr/^(?:[^\t]*\t){$fldno}([^\t]*)/;
                    push @key_flds, ($ht[0]) =~ $re;
                }
            }
            
            $ht[0] =~ s/\r\n//g;
            $ht[0] =~ s/\t$//;
            $ht[0] =~ s/(?<=\d),(?=(?:\d\d\d)\b)//g;

            print $hdr_fh $ht[0] . "\n";
            
            for my $det ( @ht[ 1 .. $#ht ] )
            {
                $det =~ s/\r\n//g;
                $det =~ s/\t$//;
                $det =~ s/(?<=\d),(?=(?:\d\d\d)\b)//g;

                $ENV{HWE_DEBUG}
                    && print substr( $det, 0, 30 ) . ":"
                    . ( $det =~ tr/\t// ) . "\n";
                $tabs = () = $det =~ m/(\t)/g;

                #printf STDERR "Too few tabs:$det\n" and next
                if ( $tabs + 1 < $self->{mat_fld_cnt} )
                {
                    &$chk_inv( $row_cnt, $tabs, $self->{mat_fld_cnt}, $det );
                    next;
                }
                for my $ky ( @key_flds)
                {
                    print $mat_fh $ky . "\t";
                }
                print $mat_fh $det . "\n";
            }
        }
    }
    else
    {
        local $/ = "\x0d\x0a\x0d\x0a" if  $self->{table} eq 'CUVc';
        local $/ = "\x0d\x0a" if  $self->{table} ne 'CUVc';
        no warnings "uninitialized";
        while ( my $hdr_trlr = <$inv_fh> )
        {
            my $i = 0;
            $row_cnt++;
            $ENV{HWE_DEBUG}
                && print substr( $hdr_trlr, 0, 10 ) . ":"
                . ( $hdr_trlr =~ tr/\t// ) . "\n";
            if ($hdr_trlr =~m/\$%\^!:/) {
                $hdr_trlr=~s/\x0d\x0a\$%\^!:\t[^\t]*\t//ms;
            }
            chomp $hdr_trlr;
            my $tabs = () = $hdr_trlr =~ m/(\t)/g;

            if ( $self->{table} eq 'ObjBalVc' )
            {
                $hdr_trlr =~ s/(.*?\t).*?\t/$1/;
            }
            if ( $self->{table} eq 'ORVc' )
            {
                $hdr_trlr =~ s/((.*?\t){111})\t/$1/;
            }
            if ( $self->{table} eq 'IVVc' )
            {
                $hdr_trlr =~ s/((.*?\t){163})\t/$1/;
            }

            if ( $tabs + 1 < $self->{hdr_fld_cnt} )
            {
                &$chk_inv( $row_cnt, $tabs, $self->{hdr_fld_cnt}, $hdr_trlr );
                next;
            }
            my ($key_fld) = $hdr_trlr =~ m/^([^\t]+?)\t/;
            $hdr_trlr =~ s/\t$//;
            $hdr_trlr =~ s/(?<=\d),(?=(?:\d\d\d)\b)//g;
            print $hdr_fh $hdr_trlr . "\n";
        }
    }
}

sub new
{
    my ( $self, $tab ) = @_;
    my $FL;
    local $SIG{__WARN__} = sub { die $_[0] };
    {
        local $/ = "\x0d\x0a\x0d\x0a";
        open my $dt_fh, "<", "$ENV{MIS}/imp/hwe_layout.TXT"
            or die "$! ";
        ($FL) = grep { /^$tab/m } <$dt_fh>;
        die "Cannot find table metadata for $tab in the "
            . "$ENV{MIS}/imp/hwe_layout.TXT file" unless $FL;
    }
    $FL=~s/\A.*$tab/$tab/ms;
    my @flds;
    my $cnt = 0;
    my ($oddlayout,$igtax,$crcnt);
    for my $rw ( splice( @{ [ split( /\x0a/, $FL ) ] }, 1 ) )
    {
        if ($rw =~ /This Register is/) {
            $igtax=1;
            $crcnt=0;
            # push @flds, [ ( 'txsernr','M4Long') ];
            # push @flds, [ ( 'txrdval','M4Val') ];
        }
        if ($igtax) {
            $crcnt++ if $rw=~/^\s*<CR>\s*$/;
            $cnt++;
            if ($crcnt==2) {
                undef $igtax;
            }
            next;
        }
        last if $rw =~ /Matrix Rows/ && $cnt > 1;
        if ( $rw =~ /Matrix Rows/ && $cnt == 1 )
        {
            $oddlayout = 1;
            next;
        }
        last if $rw =~ /<CR>/;
        $cnt++;
        $oddlayout
            ? push @flds, [ ( $rw =~ /(\w+)\s+(\w+)/ ) ]
            : push @flds, [ ( $rw =~ /\w+\s+(\w+)\s+(\w+)/ ) ];

        # $key_no = $cnt - 2 if $rw =~ /\b$key\b/;
    }
    my $hdr_fld_cnt;
    for my $fld (@flds)
    {
        $fld->[0] = '"' . $fld->[0] . '"'
            if grep( /$fld->[0]/i, @pgkeywords );
        $hdr_fld_cnt++;
        $fld->[1] = $pg_type_of{ $fld->[1] } || 'character varying';
    }
    #--------------------------------------------------
    # Populate matrix fields 
    my @matrix;
    undef $igtax;
    undef $crcnt;
    for my $rw ( splice( @{ [ split( /\x0a/, $FL ) ] }, $cnt + 2 ) )
    {
        if ($rw =~ /This Register is/) {
            $igtax=1;
            $crcnt=0;
            # push @flds, [ ( 'txsernr','M4Long') ];
            # push @flds, [ ( 'txrdval','M4Val') ];
        }
        if ($igtax) {
            $crcnt++ if $rw=~/^\s*<CR>\s*$/;
            $cnt++;
            if ($crcnt==2) {
                undef $igtax;
            }
            next;
        }
        last if $rw =~ /<CR>/;
        push @matrix, [ ( $rw =~ /(\w+)\s+(\w+)/ ) ];
    }
    my $mat_fld_cnt = 0;
    if ( scalar $#matrix )
    {
        for my $fld (@matrix)
        {
            # Avoid collision with pg keywords
            $fld->[0] = 'x_' . $fld->[0]
                if grep( /$fld->[0]/i, @pgkeywords );
            $mat_fld_cnt++;

            # $fld->[0]='m_' . $fld->[0];
            $fld->[1] = $pg_type_of{ $fld->[1] } || 'character varying';
        }
    }

    #-------------------------------------------------
    #  Work out matrix foreign key fields and data types
    #------------------------------------------------
    #  Each element of mat_fks will be array ref
    #  [0] name, [1] data type, [2] number in header row
    my @mat_fks=();
    my $fk_ind=0;
    my $fld_ind=0;
    for my $fld ( @flds )
    {
        if (ref $hwe_nm{$tab}->{key})
        {
            if ($$fld[0] ~~ @{$hwe_nm{$tab}{key}})
            {
                $mat_fks[$fk_ind]=[ @{$fld},$fld_ind ] ;
                $fk_ind++;
            }
        }
        elsif ( $$fld[0] eq $hwe_nm{$tab}{key} )
        {
            $mat_fks[0]=[ $$fld[0], $$fld[1], $fld_ind ];
            last;
        }
        $fld_ind++;
    }

    my $objref = {
        table     => $tab,
        hdr_table => "hwe." . $tab . ( $mat_fld_cnt ? "_h" : '' ),
        mat_table => $mat_fld_cnt
        ? "hwe." . $tab . "_m"
        : '',
        hdr         => \@flds,
        hdr_fld_cnt => $hdr_fld_cnt,
        mat_fld_cnt => $mat_fld_cnt,
        filename    => $hwe_nm{$tab}->{fn} || 'Unknown',
        matrix      => \@matrix,
        mat_fks     => \@mat_fks,
    };

    bless $objref, $self;
    return $objref;
}

1;

=head1 Introduction

This module provides the functionality to create and load
PostgreSQL tables from the HWE exports in the C</home/osshare/Exports>
directory. To do this it uses the C<$MIS/imp/hwe_Layout.txt> file in the 
directory to provide the data definitions of the exports.

=head1 Synopsis


    use K_HWE;

    my $hwe    = K_HWE->new( HWE_table_name_as_listedin_Layout );

    my $ddl = $hwe->get_ddl;

    $hwe->write_loadfile($OPT{f} || '');
    for my $tab ($hwe->{hdr_table}, $hwe->{mat_table})
    {
        $dbh->do(qq{ TRUNCATE TABLE } . $tab);
        K_Db::copy($dbh,$tab);
    }

=head1 Methods

=head2 get_ddl

Produce the ddl to create the header and matrix tables, including
setting up permissions

=head2 write_loadfile

Produces C<.txt> file in C<$MIS/stg> directory that can be loaded
using the C<K_Db::copy> routine.

=head1 Adding a table

=over 

=item Add the table to the C<hwe_layout.txt> file

=item Add the details to the  C<hwe_nm> hash in this file

=back

=cut
