package K_OS;
use strict;
use warnings;
use Carp;
use POSIX qw(WIFEXITED);

sub run_cmd {
    my $cmd=shift;
    $cmd=~s/\n/ /msg;
    WIFEXITED(system $cmd);
    croak "Failed running $cmd:$?" if $?;
}

1;
__END__

=head1 NAME

D_OS - Module to run system commands

=head1 SYNOPSIS

use D_OS;

...
D_OS::run_cmd(qq{...});
....

=cut

